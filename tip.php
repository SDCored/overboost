<?php

	$page = "";
	ob_start();
	$sRoot = $_SERVER['DOCUMENT_ROOT'];
	require($sRoot . '/app/start.php');
	require($sRoot . "/app/connect.php");

	if(isset($userSession)) {
		// Nada
	}else{
		header("Location: /?notLoggedIn");
	}

	use PayPal\Api\Payment;
	use PayPal\Api\PaymentExecution;

	if(!isset($_GET['success'], $_GET['paymentId'], $_GET['PayerID'])) {
		die(); // Make redirect message
	}

	if((bool)$_GET['success'] === false) {
		die(); // Make redirect message
	}

	$paymentId = $_GET['paymentId'];
	$payerId = $_GET['PayerID'];
	$boosterID = $_GET['userID'];
	$oID = $_GET['oID'];

	$payment = Payment::get($paymentId, $paypal);

	$execute = new PaymentExecution();
	$execute->setPayerId($payerId);

	try {
		$result = $payment->execute($execute, $paypal);
		$getEmail = $result->getPayer()->getPayerInfo()->getEmail();
		$getFirstName = $result->getPayer()->getPayerInfo()->getFirstName();
		$getLastName = $result->getPayer()->getPayerInfo()->getLastName();
		$fullName = $getFirstName . ' ' . $getLastName;

		$findEmail = "SELECT * FROM ob_users WHERE `email` = '$getEmail'";
		$findEmailResult = $con->query($findEmail);

		$getJSONResult = json_decode($result);
		$totalPrice = $getJSONResult->transactions[0]->amount->total;

		$emailSubject = "Overwatch Boost - Tip Receipt Confirmation";
		$emailMessage = "Hello, " . $fullName . "!\nThank you for the tip of ".$totalPrice.". Your booster has recieved the tip.";

		$getBoosterBalance = mysqli_query($con, "SELECT * FROM ob_users WHERE `uID` = '$boosterID'");
		while($row = mysqli_fetch_assoc($getBoosterBalance)) {
			$boosterBalance = $row['unpaidBalance'];
		}

		$balanceAfterTip = $boosterBalance + $totalPrice;
		mysqli_query($con, "UPDATE ob_users SET `unpaidBalance` = $balanceAfterTip WHERE `uID` = '$boosterID'") or die(mysqli_error($con));

		mail("sdcored@gmail.com", $emailSubject, $emailMessage);
		header("Location: /dashboard.php?oID=".$oID);
	}catch (Exception $e){
		$data = json_decode($e->getData());
		var_dump($data);
		die();
	}

?>

.