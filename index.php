<?php 
    $page = "index";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];

    if(isset($_GET['notLoggedIn'])) {
        $notLoggedIn = "<div class='alert alert-danger'>Not Logged In. To do that, please make sure you are logged in.</div>";
    }else{
        echo "";
    }

    require($sRoot."/include/navbar.php");
?>

        <!-- =======================
      WELCOME MAT
      ======================== -->

        <div id="welcome-mat">

            <!-- Background Video -->
            <!-- <video poster="" id="cover-video" playsinline autoplay muted loop>
                <source src="static/media/cover-video.mp4" type="video/mp4">
            </video> -->

            <!-- Background Net Texture -->
            <div id="cover-net"></div>

            <!-- Background Dark Overlay -->
            <div id="cover-overlay"></div>

            <!-- Background Particles -->
            <div id="cover-particles"></div>

            <!-- Floating Content -->
            <div class="floating-content align-middle">

                <div class="row">

                    <div class="col">

                        <!-- <div class="logo-wrap">
                            <img src="static/img/main-logo.png" alt="White Logo">
                        </div> -->

                        <div class="content-wrap">

                            <?php
                                if(isset($notLoggedIn)) {
                                    echo $notLoggedIn;
                                }
                            ?>

                            <p>Next Gen Elo Boosting</p>

                            <div class="cta-wrap">
                                <a href="/demo.php" class="btn btn-hero demo" id="demo-btn">Demo</a>
                                <a href="/pricing.php" class="btn btn-hero pricing" id="prices-btn-home">Prices</a>
                            </div>

                            <div class="coupon-holder">
                                <p class="coupon">HIGHAF13
                                    <span class="tagline">Claim Before Offer Ends</span>
                                </p>

                            </div>

                            <!-- == TIMER == -->
                            <div class="hero-counter-new" data-end-date="30 June 2017 09:00:00">
                                <div class="row">
                                    <div class="col timer-component">
                                        <div class="fyre-days">
                                            <p class="num">00</p>
                                            <p class="tt day-slug">days</p>
                                        </div>
                                    </div>
                                    <div class="col timer-component">
                                        <div class="fyre-hours">
                                            <p class="num">00</p>
                                            <p class="tt hour-slug">hours</p>
                                        </div>
                                    </div>
                                    <div class="col timer-component">
                                        <div class="fyre-minutes">
                                            <p class="num">00</p>
                                            <p class="tt minute-slug">minutes</p>
                                        </div>
                                    </div>
                                    <div class="col timer-component">
                                        <div class="fyre-seconds">
                                            <p class="num">00</p>
                                            <p class="tt second-slug">seconds</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- == TIMER ENDS == -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- =======================
      WELCOME MAT - END
      ======================== -->

        <!-- =======================
      PERKS - END
      ======================== -->

        <div id="perks">

          
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6 text-center">
                        <img src="static/img/home/reinhart.webp" class="section-img" alt="Section Image">
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="padding-top padding-bottom">
                            <h1 class="section-head">The Easiest, Fastest Way
                            to Get your Rank!</h1>

                            <h3 class="section-tag">Trusted by thousands of players Worldwide.
                            </h3>

                            <p class="desc">With an experienced team of over 300 Master/Challenger Boosters, we are able to provide the best boosting experience to our customers.</p>

                            <ul class="feature-list">
                                <li><i class="fa fa-support"></i> 24/7 Live Customer Support</li>
                                <li><i class="fa fa-trophy"></i> 1212 Completed Orders</li>
                                <li><i class="fa fa-area-chart"></i> 90% Overall Win Ratio</li>
                                <li><i class="fa fa-check-circle-o"></i> 30 Boosters Ready To Start</li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- =======================
      PERKS - END
      ======================== -->

        <!-- ==============
          ORDER TUTORIAL
         =============== -->

        <div id="order-tutorial" class="padding-top padding-bottom">


            <div class="container">
                <p class="process-title text-center">Easy to Follow Steps with a Video Tutorial!</p>
                <div class="row process-row">

                    <div class="col-md-6">

                        <div class="vertical-process">

                            <!-- @process-step1 -->
                            <div class="step">
                                <div class="icon">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="desc">
                                    <p class="head">1. Select Your Package</p>
                                    <p class="text">Decide which of our packages best suits your needs.</p>
                                </div>
                            </div>

                            <!-- @process-step2 -->
                            <div class="step">
                                <div class="icon">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                                <div class="desc">
                                    <p class="head">2. CHECKOUT</p>
                                    <p class="text">Proceed by checking out using your credit card, your debit card or Paypal. If you want to use Bitcoin, please use the contact us form at the bottom of the page.</p>
                                </div>
                            </div>

                            <!-- @process-step3 -->
                            <div class="step">
                                <div class="icon">
                                    <i class="fa fa-send"></i>
                                </div>
                                <div class="desc">
                                    <p class="head">3. INSTANT DELIVERY</p>
                                    <p class="text">As soon as you’ve sent your payment, an email containing the user name and the password of the account your purchased will be dispatched to your payment email.</p>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-6 include-flex">
                        <!-- @process-title -->
                        <div class="process-left">

                            <div class="video-wrap">
                                <img src="static/img/browser.png" class="browser-mockup" alt="browser Mockup">
                                <!-- @process-video -->
                                <div class="video-holder embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/U5_WLD8ZkNg"></iframe>
                                    <!-- Just change the "src" attribute to the YouTube embed link -->
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <!-- ==============
          ORDER TUTORIAL - END
         =============== -->

        <!-- ==============
          FEATURES
         =============== -->

        <div id="features">

            <div class="features-container">

                <!-- <div id="features-particles"></div> -->

                <div class="swiper-wrapper">

                    <!-- Watch your booster live -->
                    <div class="feature swiper-slide" data-swiper-autoplay="4000">
                        <div id="booster-live">

                            <img class="feature-bg" src="static/img/home/feature1.jpg" alt="booster live">

                            <div class="container padding-top padding-bottom">
                                <div class="row content-row">
                                    
                                    <div class="col-12 col-lg-6 align-center">

                                        <div class="screen-overlap align-center">
                                            <img src="static/img/cover-screen.png" class="img-responsive" alt="Responsive Screen">

                                            <div class="video-wrap">
                                                <video class="video" autoplay="autoplay" muted loop>
                                                    <source src="static/media/broadcast-video.mp4" type="video/mp4">
                                                </video>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col-12 col-lg-6 align-center">
                                        <div class="content-info">
                                            <h1>Watch Your Booster Live!</h1>

                                            <p class="tagline">You deserve to be a part of the victory.</p>

                                            <p class="desc">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut nibh turpis. Suspendisse quis metus at erat sodales varius. Aliquam mauris turpis, blandit in congue non, cursus vitae sapien. Maecenas in cursus ipsum. Donec sit amet auctor enim, vel blandit tortor. Morbi in semper mauris.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--===============-->

                    <!-- Watch your booster live -->
                    <div class="feature swiper-slide" data-swiper-autoplay="4000">
                        <div id="live-chat">

                           <img class="feature-bg" src="static/img/home/feature2.jpg" alt="booster live">

                            <div class="container padding-top padding-bottom">
                                <div class="row content-row">
                                    <div class="col-12 col-lg-6 align-center">

                                        <div class="screen-overlap align-center">

                                            <img src="static/img/responsive-ipad.png" alt="Responisve iPad" class="responsive-ipad">

                                            <div class="chat-wrap">
                                                <div class="chat-inside">

                                                    <div class="message">
                                                        <p class="name">Customer</p>
                                                        <div class="msg-box">Hey!</div>
                                                    </div>

                                                    <div class="message inverse">
                                                        <p class="name">Booster</p>
                                                        <div class="msg-box">Hello! I'm starting right now!</div>
                                                    </div>

                                                    <div class="message">
                                                        <p class="name">Customer</p>
                                                        <div class="msg-box">That's cool!</div>
                                                    </div>

                                                    <div class="message">
                                                        <p class="name">Customer</p>
                                                        <div class="msg-box">Hey, you were great.</div>
                                                    </div>

                                                    <div class="message inverse">
                                                        <p class="name">Booster</p>
                                                        <div class="msg-box">Haha, fanks.</div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-12 col-lg-6 align-center">
                                        <div class="content-info">
                                            <h1>Live Chat With Your Booster!</h1>
                                            <p class="tagline">Congratulate the booster for every win.</p>
                                            <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut nibh turpis. Suspendisse quis metus at erat sodales varius. Aliquam mauris turpis, blandit in congue non, cursus vitae sapien. Maecenas in cursus ipsum. Donec sit amet auctor enim, vel blandit tortor. Morbi in semper mauris.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--===============-->

                    <!-- Watch your booster live -->
                    <div class="feature swiper-slide" data-swiper-autoplay="4000">
                        <div id="pick-stuff">

                            <img class="feature-bg" src="static/img/home/champions.jpg" alt="booster live">

                            <div class="container padding-top padding-bottom">
                                <div class="row content-row">

                                    <div class="col align-center">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <img src="static/img/home/roadhog.png" class="pick-stuff-img">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <h1>Pick Your Champions</h1>
                                                <p class="tagline">Your choices matter.</p>
                                                <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut nibh turpis. Suspendisse quis metus at erat sodales varius. Aliquam mauris turpis, blandit in congue non, cursus vitae sapien. Maecenas in cursus ipsum. Donec sit amet auctor enim, vel blandit tortor. Morbi in semper mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut nibh turpis. Suspendisse quis metus at erat sodales varius. Aliquam mauris turpis, blandit in congue non, cursus vitae sapien. Maecenas in cursus ipsum. Donec sit amet auctor enim, vel blandit tortor. Morbi in semper mauris.
                                                </p>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--===============-->

                    <!-- Watch your booster live -->
                    <div class="feature swiper-slide" data-swiper-autoplay="4000">
                        <div id="security">

                            <img class="feature-bg" src="static/img/home/feature3.jpg" alt="booster live">

                            <div class="container padding-top padding-bottom">
                                <div class="row content-row">
                                    <div class="col align-center">
                                    <img src="static/img/home/Mercy.webp" class="password-img">
                                    </div>

                                    <div class="col-12 col-lg-6 align-center">
                                        <div class="content-info">
                                            <h1>Password Encryption, Blocked Shop &amp; VPN usage</h1>
                                            <p class="tagline">We value your security.</p>
                                            <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut nibh turpis. Suspendisse quis metus at erat sodales varius. Aliquam mauris turpis, blandit in congue non, cursus vitae sapien. Maecenas in cursus ipsum. Donec sit amet auctor enim, vel blandit tortor. Morbi in semper mauris.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--===============-->

                    <!-- Watch your booster live -->
                    <!-- <div class="feature swiper-slide" data-swiper-autoplay="4000">
                        <div id="track-order">
                            <img class="feature-bg" src="static/img/home/feature4.jpg" alt="booster live">
                            <div class="container padding-top padding-bottom">
                                <div class="row content-row">

                                    <div class="col align-center">
                                        <div class="content-info">
                                            <h1>Track Your Order</h1>
                                            <p class="tagline">Keep check on every step.</p>
                                            <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <div id='demo'>
                                            <div class="block">
                                                <div class="block-head">
                                                    Skill Rating Boost (In Progress)
                                                </div>
                                                <div class="block-body no-pad">
                                                    <div class="rank-list">
                                                        <div class="rank">
                                                            <p class="head">Start</p>
                                                            <img src="static/img/demo/ranks/1.png" class="img-responsive">  
                                                            <p class="value">2474</p>
                                                        </div>
                                                        <div class="rank">
                                                            <p class="head">Current</p>
                                                            <img src="static/img/demo/ranks/2.png" class="img-responsive">  
                                                            <p class="value">2537</p>
                                                        </div>
                                                        <div class="rank">
                                                            <p class="head">Desired</p>
                                                            <img src="static/img/demo/ranks/3.png" class="img-responsive">  
                                                            <p class="value">3000</p>
                                                        </div>
                                                    </div>

                                                    <div class="order-progress">
                                                        <p class="head">Progress</p>
                                                        <div class="order-progress-slider" data-value="40">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--===============-->

                </div>
                <div class="features-pagination"></div>
            </div>

        </div>

        <!-- ==============
          FEATURES - END
         =============== -->

        



        <!-- ==============
          REVIEWS
         =============== -->

        <div id="reviews">

            <div class="row">

                <!-- <div class="col-md-6 review-col-left">
                    <div class="stuff-holder">
                        
                        <div class="text container">

                            <p class="count">OverwatchBoost is rated <b>5.00</b> stars by Review.io based on <b>800</b> merchant reviews.</p>

                            <p class="confidence">
                                All our customer reviews are from genuine OverwatchBoost customers and are independently
                                <br>collected &amp; verified by Reviews.co.uk. To view our full review page <a href="#">click here</a>.
                            </p>

                        </div>
                    </div>
                </div> -->

                <div class="col-md-12 review-col-right">

                    <img src="static/img/reviewio.png" alt="Reviews.io Logo" class="img-responsive hidden-sm">

                    <div class="reviews-container">

                        <div class="swiper-wrapper">

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="3000">

                                <p class="review-text">
                                    "Best LoL account I've ever purchased"
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="3000">

                                <p class="review-text">
                                    "I'm never buying my LoL accounts from another site."
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                        </div>

                    </div>

                </div>

                <div class="col-md-12">
                    <div class="reviews-text">
                        <p>OverwatchBoost is rated 4.7 stars based on 333 total reviews.</p>

                        <a href="#" class="btn btn-primary">See All Reviews</a>
                    </div>
                </div>
                

            </div>

            

        </div>

        <!-- =============
          REVIEWS - END
         ============== -->

         <!-- =============
          FAQ
         ============== -->
         <div id="faq-home" class="padding-top">

            <div id="faq-particles"></div>

             <div class="container">
                <div class="row">

                    <div class="col-12 col-sm-6 padding-bottom">
                        <h1>Have any questions?</h1>
                        <p>Our Live Customer Support is up 24/7! Talk to us now!</p>

                        <a href="faq" class="btn btn-primary btn-inverse">Frequently Asked Questions</a>

                    </div>

                    <div class="col-12 col-sm-6">
                        <img src="static/img/home/genji.webp" class="faq-img">
                    </div>

                </div>
            </div>
         </div>
         <!-- =============
          FAQ - END
         ============== -->

        <?php require($sRoot."/include/footer.php"); ?>

                 <script type="text/javascript">
            function navbarScroll() {
                var primaryNav = $('#primary-nav');
                var topNav = $('#top-nav');
                var switchLinks = $('.nav-scroll');
                var backToTop = $('#back-to-top');
                backToTop.hide();

                var scrollTop = $(window).scrollTop();

                if(scrollTop !== 0) {
                    primaryNav.addClass('scrolled');
                    topNav.addClass('scrolled');
                    switchLinks.addClass('scrolled');
                    backToTop.show('slow');
                } else {
                    primaryNav.removeClass('scrolled');
                    topNav.removeClass('scrolled');
                    switchLinks.removeClass('scrolled');
                    backToTop.hide('slow');
                }

                $(window).on('scroll', function() {

                    var scrollTop = $(window).scrollTop();

                    if(scrollTop !== 0) {
                        primaryNav.addClass('scrolled');
                        topNav.addClass('scrolled');
                        switchLinks.addClass('scrolled');
                        backToTop.show('slow');
                    } else {
                        primaryNav.removeClass('scrolled');
                        topNav.removeClass('scrolled');
                        switchLinks.removeClass('scrolled');
                        backToTop.hide('slow');
                    }

                });

            }
         </script>