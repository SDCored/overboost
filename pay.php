<?php

	$page = "";
	$sRoot = $_SERVER['DOCUMENT_ROOT'];
	require($sRoot . '/app/start.php');
	require($sRoot . "/app/connect.php");

	use PayPal\Api\Payment;
	use PayPal\Api\PaymentExecution;

	if(!isset($_GET['success'], $_GET['paymentId'], $_GET['PayerID'])) {
		header("Location: /");
	}

	if((bool)$_GET['success'] === false) {
		header("Location: /");
	}

	$paymentId = $_GET['paymentId'];
	$payerId = $_GET['PayerID'];
	$gameType = $_GET['gameType'];
	$lastRank = $_GET['rankType'];

	$payment = Payment::get($paymentId, $paypal);

	$execute = new PaymentExecution();
	$execute->setPayerId($payerId);

	try {
		$result = $payment->execute($execute, $paypal);
		$getEmail = $result->getPayer()->getPayerInfo()->getEmail();
		$getFirstName = $result->getPayer()->getPayerInfo()->getFirstName();
		$getLastName = $result->getPayer()->getPayerInfo()->getLastName();
		$fullName = $getFirstName . ' ' . $getLastName;

		// echo 'Payment made. Thanks dawg! ' . $getEmail . ' Name: ' . $getFirstName . ' ' . $getLastName;

		$findEmail = "SELECT * FROM ob_users WHERE `email` = '$getEmail'";
		$findEmailResult = $con->query($findEmail);

		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+/.!@#$%^&*()";
		$randomPassword = substr(str_shuffle($chars), 0, 10);
		$randomHashPass = md5($randomPassword);

		$getJSONResult = json_decode($result);

		$totalPrice = $getJSONResult->transactions[0]->amount->total;

		$orderUID = uniqid();

		$getUserEmail = $_GET['userEmail'];

		if($getUserEmail == "none") {
			$ppEmail = $result->getPayer()->getPayerInfo()->getEmail();
		}else{
			$ppEmail = $getUserEmail;
		}

		$emailSubject = "Overwatch Boost - Account Information & Payment Receipt";
		$emailMessage = "Hello, " . $fullName . "!\nThank you for your order!\n\nYou paid $".$totalPrice." for this order.\n\nYou can view the order at: https://".$_SERVER['SERVER_NAME']."/dashboard.php?oID=".$orderUID."\n\nUsername: ".$ppEmail."\n\nPassword: ".$randomPassword;

		$emailSubject2 = "Overwatch Boost - Payment Receipt";
		$emailMessage2 = "Hello, " . $fullName . "!\nThank you for your order!\n\nYou paid $".$totalPrice." for this order.\n\nYou can view the order at: https://".$_SERVER['SERVER_NAME']."/dashboard.php?oID=".$orderUID;

		function get_client_ip() {
		    $ipaddress = '';
		    if (getenv('HTTP_CLIENT_IP'))
		        $ipaddress = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipaddress = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		       $ipaddress = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipaddress = getenv('REMOTE_ADDR');
		    else
		        $ipaddress = 'UNKNOWN';
		    return $ipaddress;
		}

		$userIPAddress = get_client_ip();

		$timeCreated = date("h:i:sA");
		$dateMade = date("m/d/Y");

		$currentRank = $_GET['currentRank'];
		$desiredRank = $_GET['desiredRank'];

		if(isset($userSession)) {
			mysqli_query($con, "INSERT INTO ob_orders (`uniqueID`, `orderType`, `firstName`, `lastName`, `userEmail`, `currentRank`, `desiredRank`, `amountPaid`, `bidAmount`, `bidUsername`, `timerCreated`, `bidCreated`, `userRegion`, `userPlatform`, `percentComplete`, `dateCreated`, `timeCreated`, `orderNotes`, `gameEmail`, `gameBattleTag`, `gamePassword`, `hasPlayed`) VALUES ('".$orderUID."', '$gameType', '".$getFirstName."', '".$getLastName."', '".$ppEmail."', '".$currentRank."', '".$desiredRank."', '".$totalPrice."', '0', 'none', '0', '0', '0', '0', '0', '".$dateMade."', '".$timeCreated."', 'No notes.', 'email', 'username', 'password', '0')") or die(mysqli_error($con));
			mail($ppEmail, $emailSubject2, $emailMessage2);
			header("Location: /dashboard.php?oID=".$orderUID."&newOrder");
		}else{
			$findEmail = "SELECT * FROM ob_users WHERE `email` = '$ppEmail'";
			$findEmailResult = $con->query($findEmail);
			if($findEmailResult->num_rows > 0) {
				mysqli_query($con, "INSERT INTO ob_orders (`uniqueID`, `orderType`, `firstName`, `lastName`, `userEmail`, `currentRank`, `desiredRank`, `amountPaid`, `bidAmount`, `bidUsername`, `timerCreated`, `bidCreated`, `userRegion`, `userPlatform`, `percentComplete`, `dateCreated`, `timeCreated`, `orderNotes`, `gameEmail`, `gameBattleTag`, `gamePassword`, `hasPlayed`) VALUES ('".$orderUID."', '$gameType', '".$getFirstName."', '".$getLastName."', '".$ppEmail."', '".$currentRank."', '".$desiredRank."', '".$totalPrice."', '0', 'none', '0', '0', '0', '0', '0', '".$dateMade."', '".$timeCreated."', 'No notes.', 'email', 'username', 'password', '0')") or die(mysqli_error($con));
				mail($ppEmail, $emailSubject2, $emailMessage2);
				$_SESSION['userEmail'] = $ppEmail;
            	$userSession = $_SESSION['userEmail'];
				header("Location: /dashboard.php?oID=".$orderUID."&newOrder");
			}else{
				mysqli_query($con, "INSERT INTO ob_orders (`uniqueID`, `orderType`, `firstName`, `lastName`, `userEmail`, `currentRank`, `desiredRank`, `amountPaid`, `bidAmount`, `bidUsername`, `timerCreated`, `bidCreated`, `userRegion`, `userPlatform`, `percentComplete`, `dateCreated`, `timeCreated`, `orderNotes`, `gameEmail`, `gameBattleTag`, `gamePassword`, `hasPlayed`) VALUES ('".$orderUID."', '$gameType', '".$getFirstName."', '".$getLastName."', '".$ppEmail."', '".$currentRank."', '".$desiredRank."', '".$totalPrice."', '0', 'none', '0', '0', '0', '0', '0', '".$dateMade."', '".$timeCreated."', 'No notes.', 'email', 'username', 'password', '0')") or die(mysqli_error($con));
				mysqli_query($con, "INSERT INTO ob_users (`firstName`, `lastName`, `email`, `skype`, `password`, `ppEmail`, `ipAddress`, `platform`, `region`, `skillRating`, `unpaidBalance`, `paidBalance`) VALUES ('$getFirstName', '$getLastName', '$ppEmail', 'none', '$randomHashPass', '$ppEmail', '$userIPAddress', '0', 'none', '0', '0.00', '0.00')") or die(mysqli_error($con));
				mail($ppEmail, $emailSubject, $emailMessage);
				$_SESSION['userEmail'] = $ppEmail;
            	$userSession = $_SESSION['userEmail'];
				header("Location: /dashboard.php?oID=".$orderUID."&newOrder");
			}
			// if $ppEmail_numrows > 0
			//  log user in
			// else
			//  create user account with $ppEmail
		}
	}catch (Exception $e){
		$data = json_decode($e->getData());
		var_dump($data);
		die();
	}

?>

.