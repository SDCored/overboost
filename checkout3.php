<?php

    $page = "";
	$sRoot = $_SERVER['DOCUMENT_ROOT'];
	require_once($sRoot . "/app/connect.php");

	use PayPal\Api\Payer;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Details;
	use PayPal\Api\Amount;
	use PayPal\Api\Transaction;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Payment;

	require_once($sRoot . "/app/start.php");

	$product = "Overwatch Boosting - Placement";
	$price = filter_var($_POST['hiddenAmount'], FILTER_SANITIZE_EMAIL);
	$shipping = 0.00;

	$lastRanking = $_POST['rankType'];
	$currentRating = $_POST['hiddenGameAmount'];
	$amountOfGames = $_POST['hiddenAmount'];

	if(isset($userSession)) {
		$userEmail = $globalUserEmail;
	}else{
		$userEmail = "none";
	}

	$total = $price;

	$payer = new Payer();
	$payer->setPaymentMethod('paypal');

	$item = new Item();
	$item->setName($product)
		->setCurrency('USD')
		->setQuantity(1)
		->setPrice($price);

	$itemList = new ItemList();
	$itemList->setItems([$item]);

	$details = new Details();
	$details->setShipping($shipping)
		->setSubtotal($total);

	$amount = new Amount();
	$amount->setCurrency('USD')
		->setTotal($total)
		->setDetails($details);

	$transaction = new Transaction();
	$transaction->setAmount($amount)
		->setItemList($itemList)
		->setDescription("Overwatch Boost Payment")
		->setInvoiceNumber(uniqid());

	$redirectUrls = new RedirectUrls();
	$redirectUrls->setReturnUrl(SITE_URL . '/pay.php?success=true&currentRank='.$lastRanking.'&desiredRank='.$currentRating.'&userEmail='.$userEmail.'&gameType=2')
		->setCancelUrl(SITE_URL . '/pricing.php?success=false');

	$payment = new Payment();
	$payment->setIntent('sale')
		->setPayer($payer)
		->setRedirectUrls($redirectUrls)
		->setTransactions([$transaction]);

	try {
		$payment->create($paypal);
	}catch (Exception $e){
		die($e);
	}

	$approvalUrl = $payment->getApprovalLink();

	header("Location: {$approvalUrl}");