<?php

	$page = "";
	$sRoot = $_SERVER['DOCUMENT_ROOT'];
	require_once($sRoot . "/app/connect.php");

	if(isset($userSession)) {
		// Nada
	}else{
		header("Location: /?notLoggedIn");
	}

	use PayPal\Api\Payer;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Details;
	use PayPal\Api\Amount;
	use PayPal\Api\Transaction;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Payment;

	require_once($sRoot . "/app/start.php");

	$product = "Overwatch Boosting - Tip";
	$price = filter_var($_POST['tipAmount'], FILTER_SANITIZE_EMAIL);
	$shipping = 0.00;

	$orderID =  $_GET['oID'];
	$boosterUID = $_GET['bID'];

	$total = $price; // Just temporary, can be changed later

	$payer = new Payer();
	$payer->setPaymentMethod('paypal');

	$item = new Item();
	$item->setName($product)
		->setCurrency('USD')
		->setQuantity(1)
		->setPrice($price);

	$itemList = new ItemList();
	$itemList->setItems([$item]);

	$details = new Details();
	$details->setShipping($shipping)
		->setSubtotal($total);

	$amount = new Amount();
	$amount->setCurrency('USD')
		->setTotal($total)
		->setDetails($details);

	$transaction = new Transaction();
	$transaction->setAmount($amount)
		->setItemList($itemList)
		->setDescription("Overwatch Boost Tip")
		->setInvoiceNumber(uniqid());

	$redirectUrls = new RedirectUrls();
	$redirectUrls->setReturnUrl(SITE_URL . '/tip.php?success=true&oID='.$orderID.'&userID='.$boosterUID)
		->setCancelUrl(SITE_URL . '/dashboard.php?oID='.$orderID.'&success=false');

	$payment = new Payment();
	$payment->setIntent('sale')
		->setPayer($payer)
		->setRedirectUrls($redirectUrls)
		->setTransactions([$transaction]);

	try {
		$payment->create($paypal);
	}catch (Exception $e){
		die($e);
	}

	$approvalUrl = $payment->getApprovalLink();

	header("Location: {$approvalUrl}");

?>