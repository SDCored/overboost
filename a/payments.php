<?php
    $sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot."/a/include/navbar.php");

    $getActiveOrders = "SELECT * FROM ob_orders ORDER BY `oID` DESC";
    $getActive = mysqli_query($con, $getActiveOrders);
?>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Payments</div>
                            </div>
                            <div class="panel-body">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr style="border-bottom: 1px solid #CCC;">
                                            <th style="font-weight: 300; padding: 5px;">ID</th>
                                            <th style="font-weight: 300; padding: 5px;">Customer Name</th>
                                            <th style="font-weight: 300; padding: 5px;">Customer Email</th>
                                            <th style="font-weight: 300; padding: 5px;">Customer BattleTag</th>
                                            <th style="font-weight: 300; padding: 5px;">Type of Boost</th>
                                            <th style="font-weight: 300; padding: 5px;">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($row = $getActive->fetch_assoc()) {
                                                $oID = $row['uniqueID'];
                                                $orderType = $row['orderType'];
                                                $orderAmount = $row['amountPaid'];

                                                $userFirstName = $row['firstName'];
                                                $userLastName = $row['lastName'];
                                                $userEmail = $row['userEmail'];
                                                $userBattleTag = $row['gameBattleTag'];

                                                if($orderType == "0") {
                                                    $orderT = "Skill Rating";
                                                }elseif($orderType == "1") {
                                                    $orderT = "Solo/Duo";
                                                }elseif($orderType == "2") {
                                                    $orderT = "Placement";
                                                }

                                                if($userBattleTag == "username") {
                                                	$userBattleTag = "None";
                                                }

                                                echo "
                                                    <tr>
                                                        <td style='padding: 5px;'>".$oID."</td>
                                                        <td style='padding: 5px;'>".$userFirstName." ".$userLastName."</td>
                                                        <td style='padding: 5px;'>".$userEmail."</td>
                                                        <td style='padding: 5px;'>".$userBattleTag."</td>
                                                        <td style='padding: 5px;'>".$orderT."</td>
                                                        <td style='padding: 5px;'>$".number_format($orderAmount,2)."</td>
                                                    </tr>
                                                ";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php require($sRoot."/a/include/footer.php"); ?>