<?php
    $page = "";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/a/include/navbar.php');

    if($globalUserAccountType === "2") { // Admin
        // All good
    }else{
        header("location: /?notLoggedIn");
    }

    $getRequestedPayouts = "SELECT * FROM ob_payouts WHERE `payoutStatus` = '0' ORDER BY `pID` DESC";
    $requestedPayoutQuery = $con->query($getRequestedPayouts);

    $getCompletedPayouts = "SELECT * FROM ob_payouts WHERE `payoutStatus` = '1' ORDER BY `pID` DESC";
    $completedPayoutQuery = $con->query($getCompletedPayouts);

?>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Requested Payouts</div>
                    </div>
                    <div class="panel-body">
                        <table style="width: 100%;">
                            <thead>
                                <tr style="border-bottom: 1px solid #CCC;">
                                    <th style="font-weight: 300; text-align: right; width: 17%; padding: 5px;">Date</th>
                                    <th style="font-weight: 300; text-align: left; width: 23%; padding: 5px;">Email</th>
                                    <th style="font-weight: 300; text-align: right; width: 20%; padding: 5px;">Amount</th>
                                    <th style="font-weight: 300; text-align: left; width: 20%; padding: 5px;">Status</th>
                                    <th style="font-weight: 300; text-align: left; width: 20%; padding: 5px; text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(mysqli_num_rows($requestedPayoutQuery) > 0) {
                                        while($row = $requestedPayoutQuery->fetch_assoc()) {
                                            $getStatus = $row['payoutStatus'];

                                            $pStatus = "<b><span style='color: #FF0000;'>Requested</span></b>";
                                            $ppButton = '<input type="image" src=https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png name="submit" alt="Make payments with payPal - it\'s fast, free and secure!">';

                                            echo '
                                                <tr style="height: 40px;">
                                                    <td style="text-align: right; padding: 5px;">'.$row['date'].'</td>
                                                    <td style="text-align: left; padding: 5px;">'.$row['payoutEmail'].'</td>
                                                    <td style="text-align: right; padding: 5px;">$'.$row['payoutAmount'].'</td>
                                                    <td style="text-align: left; padding: 5px;">'.$pStatus.'</td>
                                                    <td style="padding: 5px; text-align: center;">
                                                        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                                            <input type="hidden" name="cmd" value="_xclick">
                                                            <input type="hidden" name="business" value="'.$row['payoutEmail'].'">
                                                            <input type="hidden" name="item_name" value="Overboost Payout">
                                                            <input type="hidden" name="item_number" value="'.$row['pID'].'">
                                                            <input type="hidden" name="amount" value="'.$row['payoutAmount'].'">
                                                            <input type="hidden" name="no_shipping" value="2">
                                                            <input type="hidden" name="no_note" value="1">
                                                            <input type="hidden" name="currency_code" value="USD">
                                                            <input type="hidden" name="bn" value="IC_Sample">
                                                            <input type="hidden" name="cancel_return" value="'.$sRoot.'/a/" />
                                                            <input type="hidden" name="return" value="http://'.$_SERVER['HTTP_HOST'].'/a/updatePayouts.php?pID='.$row['pID'].'" />

                                                            '.$ppButton.'
                                                        </form>
                                                    </td>
                                                </tr>
                                            ';
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Completed Payouts</div>
                    </div>
                    <div class="panel-body">
                        <table style="width: 100%;">
                            <thead>
                                <tr style="border-bottom: 1px solid #CCC;">
                                    <th style="font-weight: 300; text-align: right; width: 17%; padding: 5px;">Date</th>
                                    <th style="font-weight: 300; text-align: left; width: 43%; padding: 5px;">Email</th>
                                    <th style="font-weight: 300; text-align: right; width: 20%; padding: 5px;">Amount</th>
                                    <th style="font-weight: 300; text-align: left; width: 20%; padding: 5px;">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(mysqli_num_rows($completedPayoutQuery) > 0) {
                                        while($row = $completedPayoutQuery->fetch_assoc()) {
                                            $getStatus = $row['payoutStatus'];

                                            $pStatus = "<b><span style='color: #00CC00;'>Completed</span></b>";

                                            echo '
                                                <tr style="height: 40px;">
                                                    <td style="text-align: right; padding: 5px;">'.$row['date'].'</td>
                                                    <td style="text-align: left; padding: 5px;">'.$row['payoutEmail'].'</td>
                                                    <td style="text-align: right; padding: 5px;">$'.$row['payoutAmount'].'</td>
                                                    <td style="text-align: left; padding: 5px;">'.$pStatus.'</td>
                                                </tr>
                                            ';
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require($sRoot."/a/include/footer.php"); ?>