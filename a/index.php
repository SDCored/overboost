<?php
    $sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot."/a/include/navbar.php");

    $getActiveOrders = "SELECT * FROM ob_orders WHERE `playComplete` = '0' ORDER BY `oID` DESC LIMIT 5";
    $getActive = mysqli_query($con, $getActiveOrders);

    $getCompletedOrders = "SELECT * FROM ob_orders WHERE `playComplete` = '1' ORDER BY `oID` DESC LIMIT 5";
    $getCompleted = mysqli_query($con, $getCompletedOrders);
?>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">5 Recent Active Orders</div>
                            </div>
                            <div class="panel-body">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr style="border-bottom: 1px solid #CCC;">
                                            <th style="font-weight: 300; padding: 5px;">ID</th>
                                            <th style="font-weight: 300; padding: 5px;">Booster</th>
                                            <th style="font-weight: 300; padding: 5px;">Type of Boost</th>
                                            <th style="font-weight: 300; padding: 5px;">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($row = $getActive->fetch_assoc()) {
                                                $oID = $row['uniqueID'];
                                                $boosterID = $row['boosterUID'];
                                                $orderType = $row['orderType'];
                                                $orderAmount = $row['amountPaid'];

                                                if($boosterID == "0") {
                                                    $boosterFirstName = "None";
                                                }else{
                                                    $boosterInfoQuery = "SELECT * FROM ob_users WHERE `uID` = '".$boosterID."'";
                                                    $boosterInfo = $con->query($boosterInfoQuery);
                                                    $getBoosterRow = mysqli_fetch_array($boosterInfo);

                                                    $boosterFirstName = $getBoosterRow['firstName'];
                                                }

                                                if($orderType == "0") {
                                                    $orderT = "Skill Rating";
                                                }elseif($orderType == "1") {
                                                    $orderT = "Solo/Duo";
                                                }elseif($orderType == "2") {
                                                    $orderT = "Placement";
                                                }

                                                echo "
                                                    <tr>
                                                        <td style='padding: 5px;'>".$oID."</td>
                                                        <td style='padding: 5px;'>".$boosterFirstName."</td>
                                                        <td style='padding: 5px;'>".$orderT."</td>
                                                        <td style='padding: 5px;'>$".number_format($orderAmount,2)."</td>
                                                    </tr>
                                                ";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">5 Recent Completed Orders</div>
                            </div>
                            <div class="panel-body">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr style="border-bottom: 1px solid #CCC;">
                                            <th style="font-weight: 300; padding: 5px;">ID</th>
                                            <th style="font-weight: 300; padding: 5px;">Type of Boost</th>
                                            <th style="font-weight: 300; padding: 5px;">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($row = $getCompleted->fetch_assoc()) {
                                                $oID = $row['uniqueID'];
                                                $boosterID = $row['boosterUID'];
                                                $orderType = $row['orderType'];
                                                $orderAmount = $row['amountPaid'];

                                                if($boosterID == "0") {
                                                    $boosterFirstName = "None";
                                                }else{
                                                    $boosterInfoQuery = "SELECT * FROM ob_users WHERE `uID` = '".$boosterID."'";
                                                    $boosterInfo = $con->query($boosterInfoQuery);
                                                    $getBoosterRow = mysqli_fetch_array($boosterInfo);

                                                    $boosterFirstName = $getBoosterRow['firstName'];
                                                }

                                                if($orderType == "0") {
                                                    $orderT = "Skill Rating";
                                                }elseif($orderType == "1") {
                                                    $orderT = "Solo/Duo";
                                                }elseif($orderType == "2") {
                                                    $orderT = "Placement";
                                                }

                                                echo "
                                                    <tr>
                                                        <td style='padding: 5px;'>".$oID."</td>
                                                        <td style='padding: 5px;'>".$orderT."</td>
                                                        <td style='padding: 5px;'>$".number_format($orderAmount,2)."</td>
                                                    </tr>
                                                ";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php require($sRoot."/a/include/footer.php"); ?>