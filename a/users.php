<?php
	$sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot."/a/include/navbar.php");

	$getUsersQuery = "SELECT * FROM ob_users WHERE NOT `uID` = '$globalUserUID' ORDER BY `uID` DESC";
	$getUsers = mysqli_query($con, $getUsersQuery);
?>
    <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Users</div>
                            </div>
                            <div class="panel-body">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr style="border-bottom: 1px solid #CCC;">
                                            <th style="font-weight: 300; padding: 5px;">ID</th>
                                            <th style="font-weight: 300; padding: 5px;">Account Type</th>
                                            <th style="font-weight: 300; padding: 5px;">Name</th>
                                            <th style="font-weight: 300; padding: 5px;">Email</th>
                                            <th style="font-weight: 300; padding: 5px;">Total Balance</th>
                                            <th style="font-weight: 300; padding: 5px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($row = $getUsers->fetch_assoc()) {
                                                $userID = $row['uID'];
                                                $userFirstName = $row['firstName'];
                                                $userLastName = $row['lastName'];
                                                $userEmail = $row['email'];
                                                $userUnpaid = $row['unpaidBalance'];
                                                $userPaid = $row['paidBalance'];
                                                $aType = $row['accountType'];

                                                $userFinalBalance = $userUnpaid + $userPaid;

                                                if($aType == "0") {
                                                	$accountType = "Customer";
                                                }elseif($aType == "1") {
                                                	$accountType = "Booster";
                                                }elseif($aType == "2") {
                                                	$accountType = "Admin";
                                                }

                                                echo "
                                                    <tr>
                                                    	<td style='padding: 5px;'>".$userID."</td>
                                                    	<td style='padding: 5px;'>".$accountType."</td>
                                                        <td style='padding: 5px;'>".$userFirstName." ".$userLastName."</td>
                                                        <td style='padding: 5px;'>".$userEmail."</td>
                                                        <td style='padding: 5px;'>$".number_format($userFinalBalance,2)."</td>
                                                        <td style='padding: 5px;'><a href='/a/pBooster.php?id=".$userID."'>Promote to Booster</a> &middot; <a href='/a/pAdmin.php?id=".$userID."'>Promote to Admin</a></td>
                                                    </tr>
                                                ";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php require($sRoot."/a/include/footer.php"); ?>