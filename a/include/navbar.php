<?php
	ob_start(); $sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot."/app/connect.php");

	if($globalUserAccountType == "2") { // Admin
        
    }elseif($globalUserAccountType == "1") { // Booster
        header("Location: /booster/");
    }elseif($globalUserAccountType == "0") { // Normal User
        header("Location: /pricing.php");
    }else{ // Other
        header("Location: /");
    }
?>

<!DOCTYPE html>

<html>

<head>
    <title>Overwatch Boost - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="/static/css/admin/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Navigation -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="logo">
                        <h1><a href="/">BUYOVERWATCHBOOST</a></h1>
                    </div>
                </div>
                <div class="col-md-5"></div>
                <div class="col-md-2">
                    <div class="navbar navbar-inverse" role="banner">
                        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $globalUserFirstName; ?> <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li><a href="/logout.php">LogOut</a></li>
                                    </ul>
                                </li> 
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navigation End -->

    <div class="page-content">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar content-box" style="display: block;">
                    <ul class="nav">
                        <li><a href="/a/">Dashboard</a></li>
                        <li><a href="/a/active.php">Active Orders</a></li>
                        <li><a href="/a/completed.php">Completed Orders</a></li>
                        <li><a href="/a/payments.php">Payments</a></li>
                        <li><a href="/a/users.php">Users</a></li>
                        <li><a href="/a/payouts.php">Payouts</a></li>
                        <li><a href="/a/coupons.php">Coupons</a></li>
                    </ul>
                </div>
            </div>