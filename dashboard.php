<?php
    $page = "dashboard";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/navbar.php');
    
    if(isset($userSession)) {
        // Logged in
    }else{
        header("Location: /"); // Not logged in
    }
    
    if(isset($_GET['success'])) {
        $getStatus = $_GET['success'];
        if($getStatus == "false") {
            $paymentStatus = "Tip failed. Please try again.";
        }
    }
    
    if(isset($_GET['oID'])) {
        $orderExists = "SELECT * FROM ob_orders WHERE `uniqueID` = '".$_GET['oID']."'";
        $orderExistsQuery = $con->query($orderExists);
        $getOrderInfo = mysqli_fetch_array($orderExistsQuery);
        
        $oID = $getOrderInfo['oID'];
        $uID = $getOrderInfo['uniqueID'];
        $currentRank = $getOrderInfo['currentRank'];
        $desiredRank = $getOrderInfo['desiredRank'];
        $boosterID = $getOrderInfo['boosterUID'];
        $boosterUsername = $getOrderInfo['bidUsername'];
        $custEmail = $getOrderInfo['gameEmail'];
        $custUsername = $getOrderInfo['gameBattleTag'];
        $custPassword = $getOrderInfo['gamePassword'];
        $orderStream = $getOrderInfo['streaming'];
        $streamURL = $getOrderInfo['livestreamURL'];
        $orderPause = $getOrderInfo['orderPause'];
        $custRegion = $getOrderInfo['userRegion'];
        $custPlatform = $getOrderInfo['userPlatform'];
        $orderType = $getOrderInfo['orderType'];
        $hasPlayed = $getOrderInfo['hasPlayed'];
        $pComplete = $getOrderInfo['playComplete'];
        
        if($orderExistsQuery->num_rows > 0) {
            // Do nothing
        }else{
            header("Location: /?noSuchOrder"); // Order doesn't exist
        }
    }else{
        header("Location: /?noSuchOrder"); // No OrderID in URl
    }
    
    if(isset($_POST['changePause'])) {
        if($orderPause == 0) {
            mysqli_query($con, "UPDATE ob_orders SET `orderPause` = 1 WHERE `uniqueID` = '".$uID."'");
            header("Location: dashboard.php?oID=$uID");
        }elseif($orderPause == 1) {
            mysqli_query($con, "UPDATE ob_orders SET `orderPause` = 0 WHERE `uniqueID` = '".$uID."'");
            header("Location: dashboard.php?oID=$uID");
        }
    }
    
    if($orderPause == 0) {
        $pauseValue = "Pause Order";
    }elseif($orderPause == 1) {
        $pauseValue = "Unpause Order";
    }

    $timeCreated = date("h:i:sA");
    $dateMade = date("m/d/Y");

    $fullDate = date("m/d/y h:i:sA");

    if(isset($_POST['submitAccInfo'])) {
        $getPlatform = $_POST['platformSelection']; // 1, 2, or 3
        $getRegion = $_POST['regionSelection']; // 1, 2, or 3
        $getAccUsername = $_POST['accUsername'];
        $getAccEmail = $_POST['accEmail'];
        $getAccPassword = mysqli_real_escape_string($con, $_POST['accPassword']);

        mysqli_query($con, "UPDATE ob_orders SET `userRegion`='$getRegion', `userPlatform`='$getPlatform', `gameBattleTag`='$getAccUsername', `gameEmail`='$getAccEmail', `gamePassword`='$getAccPassword', `biddingDisabled`='0', `bidCreated`='$fullDate' WHERE `uniqueID`='$uID'");
        header("Location: #");
    }
    
    $getNotes = "SELECT * FROM ob_orders WHERE `oID` = '$oID'";
    $getNotesQuery = $con->query($getNotes);
    $getNote = mysqli_fetch_array($getNotesQuery);

    $theNotes = $getNote['orderNotes'];

    if($theNotes == "No notes.") {
        $noteTextArea = '<textarea class="form-control" rows="7" placeholder="'.$theNotes.'" name="orderNotes" id="orderNotes"></textarea>';
    }else{
        $noteTextArea = '<textarea class="form-control" rows="7" name="orderNotes" id="orderNotes">'.$theNotes.'</textarea>';
    }
    
    if(isset($_POST['submitNotes'])) {
        $orderNoteInfo = htmlentities($_POST['orderNotes'], ENT_QUOTES, 'UTF-8');

        mysqli_query($con, "UPDATE ob_orders SET `orderNotes` = '$orderNoteInfo' WHERE `oID` = '$oID'");
        header("Location: #");
    }
    
    if($orderType == "0") { // Skill Rating
        // Basic Info
        $orderTitle = "Skill Rating";
        $firstCol = "Starting Rank";
        $secondCol = "Current In-Game Rank";
        $thirdCol = "Desired Rank";
        $rankOne = $currentRank;
        $rankThree = $desiredRank;
        $orderPercent = "0";

        $cStart = null;
        $cEnd = null;
        
        if($custUsername == "username") { // User has not set a username
            // Basic Info
            $tableOutput = "<center><b>No Game Info at this time.</b></center>";
            $orderStatus = "(Awaiting User Input)";
            $rError = null;
            $pError = null;
            $uError = null;
        
            // Get Current rank Emblem
            if($currentRank >= '0' & $currentRank <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($currentRank >= '1500' & $currentRank <= '1999') {
                $cRankImage = 'tier2.png';
            }elseif($currentRank >= '2000' & $currentRank <= '2499') {
                $cRankImage = 'tier3.png';
            }elseif($currentRank >= '2500' & $currentRank <= '2999') {
                $cRankImage = 'tier4.png';
            }elseif($currentRank >= '3000' & $currentRank <= '3499') {
                $cRankImage = 'tier5.png';
            }elseif($currentRank >= '3500' & $currentRank <= '3999') {
                $cRankImage = 'tier6.png';
            }elseif($currentRank >= '4000' & $currentRank <= '5000') {
                $cRankImage = 'tier7.png';
            }elseif($currentRank > '5000') {
                $cRankImage = 'tier7.png';
            }
            
            // Placeholder API Rank Emblem and Rank
            $apiRankImage = "tier1.png";
            $apiRankText = "0";
            
            // Get Desired Rank Emblem
            if($desiredRank >= '0' & $desiredRank <= '1499') {
                $dRankImage = 'tier1.png';
            }elseif($desiredRank >= '1500' & $desiredRank <= '1999') {
                $dRankImage = 'tier2.png';
            }elseif($desiredRank >= '2000' & $desiredRank <= '2499') {
                $dRankImage = 'tier3.png';
            }elseif($desiredRank >= '2500' & $desiredRank <= '2999') {
                $dRankImage = 'tier4.png';
            }elseif($desiredRank >= '3000' & $desiredRank <= '3499') {
                $dRankImage = 'tier5.png';
            }elseif($desiredRank >= '3500' & $desiredRank <= '3999') {
                $dRankImage = 'tier6.png';
            }elseif($desiredRank >= '4000' & $desiredRank <= '5000') {
                $dRankImage = 'tier7.png';
            }elseif($desiredRank > '5000') {
                $dRankImage = 'tier7.png';
            }
        }else{
            // Get Region, Otherwise throw error
            if($custRegion == 1) {
                $uRegion = "us";
                $rError = null;
            }elseif($custRegion == 2) {
                $uRegion = "eu";
                $rError = null;
            }elseif($custRegion == 3) {
                $uRegion = "kr";
                $rError = null;
            }elseif($custRegion == 0) {
                $uRegion = null;
                $rError = "<p class='alert alert-danger'>You must set a region if you would like to get your current in-game rank.</p>";
            }
            
            // Get Platform, Otherwise throw error
            if($custPlatform == 1) {
                $uPlatform = "pc";
                $pError = null;
            }elseif($custPlatform == 2) {
                $uPlatform = "psn";
                $pError = null;
            }elseif($custPlatform == 3) {
                $uPlatform = "xbl";
                $pError = null;
            }elseif($custPlatform == 0) {
                $uPlatform = null;
                $pError = "<p class='alert alert-danger'>You must set a platform if you would like to get your current in-game rank.</p>";
            }
            
            // Get Current rank Emblem
            if($rankOne >= '0' & $rankOne <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '1500' & $rankOne <= '1999') {
                $cRankImage = 'tier2.png';
            }elseif($rankOne >= '2000' & $rankOne <= '2499') {
                $cRankImage = 'tier3.png';
            }elseif($rankOne >= '2500' & $rankOne <= '2999') {
                $cRankImage = 'tier4.png';
            }elseif($rankOne >= '3000' & $rankOne <= '3499') {
                $cRankImage = 'tier5.png';
            }elseif($rankOne >= '3500' & $rankOne <= '3999') {
                $cRankImage = 'tier6.png';
            }elseif($rankOne >= '4000' & $rankOne <= '5000') {
                $cRankImage = 'tier7.png';
            }elseif($rankOne > '5000') {
                $cRankImage = 'tier7.png';
            }
            
            if($custRegion == 0 OR $custPlatform == 0) {
                $apiRankImage = 'tier1.png';
                $apiRankText = "0";
            }else{
                $custUsername = str_replace('#', '-', $custUsername);
                $apiURL = @file_get_contents('http://ow-api.herokuapp.com/profile/'.$uPlatform.'/'.$uRegion.'/'.$custUsername);
                
                if($apiURL) {
                    $getAPIJSON = json_decode($apiURL);
                }else{
                    $getAPIJSON = null;
                }
                
                if($getAPIJSON == null) {
                    $apiRankText = "0";
                    $uError = "<p class='alert alert-danger'>There has been an error fetching your account from the API. Please make sure everything is correct.</p>";
                    $orderStatus = "(Awaiting User Input)";
                    $orderPercent = "0";
                }else{
                    $uError = null;
                    $apiRankText = $getAPIJSON->competitive->rank;

                    // $apiRankText = "2951"; // Debugger
                    
                    $percentOne = $apiRankText - $rankOne;
                    $percentTwo = $rankThree - $rankOne;
                    $percentThree = $percentOne / $percentTwo;
                    $orderPercent = $percentThree * 100;
                    
                    if($apiRankText >= $rankThree) {
                        $apiRankText = $rankThree;
                        $orderStatus = "(Completed)";
                    }elseif($apiRankText < $rankThree) {
                        $apiRankText = $apiRankText;
                        $orderStatus = "(In Progress)";
                    }
                }

                $findGameMatches = "SELECT * FROM ob_matches WHERE `oID`='$uID' ORDER BY mID DESC LIMIT 6";
                $findMatchesResult = mysqli_query($con, $findGameMatches);
                $currentDT = date("m/d/Y h:i A");

                if($findMatchesResult->num_rows > 0) {
                    if($hasPlayed == "0") {
                        mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange) VALUES ('$uID', '$currentDT', '$boosterUsername', '$apiRankText', '0')");
                        mysqli_query($con, "UPDATE ob_orders SET `hasPlayed` = '1' WHERE `uniqueID` = '$uID'");
                        header("Location: #");
                    }else{
                        while($row = $findMatchesResult->fetch_assoc()) {
                            $orderDate = $row['datetime'];
                            $orderBooster = $row['booster'];
                            $orderCurrent = $row['rating'];
                            $orderChange = $row['ratingChange'];
    
                            $limitQuery = "SELECT * FROM ob_matches WHERE `oID`='$uID' ORDER BY mID DESC LIMIT 1";
                            $limitResult = mysqli_query($con, $limitQuery);
    
                            while($rowLimit = $limitResult->fetch_assoc()) {
                                $currentResult = $rowLimit['rating'];
                            }
    
                            // $apiRankText = "3410"; // Debugger
    
                            if($apiRankText == $currentResult) {
                                $matchResult = substr($orderChange, 0, 1);
                                if($matchResult == "+") {
                                    $matchResult = '<span class="badge victory">Victory</span>';
                                }elseif($matchResult == "-") {
                                    $matchResult = '<span class="badge defeat">Defeat</span>';
                                }else{
                                    $matchResult = '<span class="badge victory">First Result</span>';
                                }
                            }else{
                                $finalResult = $apiRankText - $currentResult;
                                $currentMatchResult = substr($finalResult, 0, 1);
                                if($currentMatchResult == "-") {
                                    $matchOutput = $finalResult;
                                }else{
                                    $matchOutput = "+".$finalResult;
                                }
    
                                mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterUsername', '$apiRankText', '$matchOutput', '0')");
                                header("Location: #");
                            }
                        }
                    }
                }else{
                    if($custUsername == "username") {
                        $tableOutput = "<center><b>No Game Info at this time.</b></center>";
                    }else{
                        if($hasPlayed == "0") {
                            mysqli_query($con, "INSERT INTO ob_matches (`oID`, `datetime`, `booster`, `rating`, `ratingChange`, `ratingLoss`) VALUES ('$uID', '$currentDT', '$boosterUsername', '$apiRankText', '0', '0')");
                            mysqli_query($con, "UPDATE ob_orders SET `hasPlayed` = '1' WHERE `uniqueID` = '$uID'");
                            header("Location: #");
                        }else{
                            $tableOutput = "<center><b>No Game Info at this time.</b></center>";
                        }
                    }
                }
            }
            
            // Get Rank Elbem (From API Rank)
            if($apiRankText >= '0' & $apiRankText <= '1499') {
                $apiRankImage = 'tier1.png';
            }elseif($apiRankText >= '1500' & $apiRankText <= '1999') {
                $apiRankImage = 'tier2.png';
            }elseif($apiRankText >= '2000' & $apiRankText <= '2499') {
                $apiRankImage = 'tier3.png';
            }elseif($apiRankText >= '2500' & $apiRankText <= '2999') {
                $apiRankImage = 'tier4.png';
            }elseif($apiRankText >= '3000' & $apiRankText <= '3499') {
                $apiRankImage = 'tier5.png';
            }elseif($apiRankText >= '3500' & $apiRankText <= '3999') {
                $apiRankImage = 'tier6.png';
            }elseif($apiRankText >= '4000' & $apiRankText <= '5000') {
                $apiRankImage = 'tier7.png';
            }elseif($apiRankText > '5000') {
                $apiRankImage = 'tier7.png';
            }
            
            // Get Desired Rank Emblem
            if($rankThree >= '0' & $rankThree <= '1499') {
                $dRankImage = 'tier1.png';
            }elseif($rankThree >= '1500' & $rankThree <= '1999') {
                $dRankImage = 'tier2.png';
            }elseif($rankThree >= '2000' & $rankThree <= '2499') {
                $dRankImage = 'tier3.png';
            }elseif($rankThree >= '2500' & $rankThree <= '2999') {
                $dRankImage = 'tier4.png';
            }elseif($rankThree >= '3000' & $rankThree <= '3499') {
                $dRankImage = 'tier5.png';
            }elseif($rankThree >= '3500' & $rankThree <= '3999') {
                $dRankImage = 'tier6.png';
            }elseif($rankThree >= '4000' & $rankThree <= '5000') {
                $dRankImage = 'tier7.png';
            }elseif($rankThree > '5000') {
                $dRankImage = 'tier7.png';
            }
        }
    }elseif($orderType == "1") { // Solo / Duo
        // Basic Info
        $orderTitle = "Solo/Duo";
        $firstCol = "Current In-Game Rank";
        $secondCol = "Total Wins";
        $thirdCol = "Needed Wins";
        $rankOne = "0";
        $rankThree = $desiredRank;
        $orderPercent = "0";

        $cStart = "<!--";
        $cEnd = "-->";

        if($custUsername == "username") { // User has not set a username
            $orderStatus = "(Awaiting User Input)";
            $rError = null;
            $pError = null;
            $uError = null;

            // Get Current rank Emblem
            if($rankOne >= '0' & $rankOne <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '1500' & $rankOne <= '1999') {
                $cRankImage = 'tier2.png';
            }elseif($rankOne >= '2000' & $rankOne <= '2499') {
                $cRankImage = 'tier3.png';
            }elseif($rankOne >= '2500' & $rankOne <= '2999') {
                $cRankImage = 'tier4.png';
            }elseif($rankOne >= '3000' & $rankOne <= '3499') {
                $cRankImage = 'tier5.png';
            }elseif($rankOne >= '3500' & $rankOne <= '3999') {
                $cRankImage = 'tier6.png';
            }elseif($rankOne >= '4000' & $rankOne <= '5000') {
                $cRankImage = 'tier7.png';
            }elseif($rankOne > '5000') {
                $cRankImage = 'tier7.png';
            }else{
                $cRankImage = "tier1.png";
            }

            // Placeholder API Rank Emblem and Rank
            $apiRankImage = "00.png";
            $apiRankText = "0";

            // Desired Rank Template
            $dRankImage = "00.png";
        }else{
            // Get Region, Otherwise throw error
            if($custRegion == 1) {
                $uRegion = "us";
                $rError = null;
            }elseif($custRegion == 2) {
                $uRegion = "eu";
                $rError = null;
            }elseif($custRegion == 3) {
                $uRegion = "kr";
                $rError = null;
            }elseif($custRegion == 0) {
                $uRegion = null;
                $rError = "<p class='alert alert-danger'>You must set a region if you would like to get your current in-game rank.</p>";
            }

            // Get Platform, otherwise throw error
            if($custPlatform == 1) {
                $uPlatform = "pc";
                $pError = null;
            }elseif($custPlatform == 2) {
                $uPlatform = "psn";
                $pError = null;
            }elseif($custPlatform == 3) {
                $uPlatform = "xbl";
                $pError = null;
            }elseif($custPlatform == 0) {
                $uPlatform = null;
                $pError = "<p class='alert alert-danger'>You must set a platform if you would like to get your current in-game rank.</p>";
            }

            if($custRegion == 0 OR $custPlatform == 0) {
                $apiRankImage = 'tier1.png';
                $apiRankText = "0";
            }else{
                $custUsername = str_replace('#', '-', $custUsername);
                $apiURL = @file_get_contents('http://ow-api.herokuapp.com/profile/'.$uPlatform.'/'.$uRegion.'/'.$custUsername);
                
                if($apiURL) {
                    $getAPIJSON = json_decode($apiURL);
                }else{
                    $getAPIJSON = null;
                }

                if($getAPIJSON == null) {
                    $apiRankText = "0";
                    $uError = "<p class='alert alert-danger'>There has been an error fetching your account from the API. Please make sure everything is correct.</p>";
                    $orderStatus = "(Awaiting User Input)";
                    $orderPercent = "0";
                }else{
                    $uError = null;
                    $apiRank = $getAPIJSON->competitive->rank;
                    $rankOne = $apiRank;

                    $apiRankText = $getAPIJSON->games->competitive->won;
                    $apiLosses = $getAPIJSON->games->competitive->lost;

                    $rankThree = $desiredRank;
                    
                    $orderPercent = "0";
                    $currentDT = date("d/m/Y h:i A");

                    $getMatches = "SELECT * FROM ob_matches WHERE `oID` = '".$uID."'";
                    $matchesQuery = $con->query($getMatches);
                    $getMatchInfo = mysqli_fetch_array($matchesQuery);

                    $matchWins = $getMatchInfo['rating'];
                    $matchLosses = $getMatchInfo['ratingChange'];

                    if($matchesQuery->num_rows < 1) {
                        mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterFirstName', '$apiRankText', '$apiLosses', '$rankThree')");
                        header("Location: #");
                    }elseif($matchesQuery->num_rows > 0) {
                        // Rating = Wins
                        // RatingChange = Loss
                        // RatingLoss = Needed Wins

                        $rankThree = $getMatchInfo['ratingLoss'];

                        // $apiRankText = "19"; // Debugger
                        // $apiLosses = "55"; // Debugger
                        if($apiRankText > $matchWins) {
                            $matchDifference = $apiRankText - $matchWins;
                            $matchFinal = $rankThree - $matchDifference;
                            if($matchFinal <= 0) {
                                $orderStatus = "(Completed)";
                                $matchFinal = 0;
                                $rankThree = "0";
                            }else{

                            }
                            mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterFirstName', '$apiRankText', '$apiLosses', '$matchFinal')");
                            header("Location: #");
                        }elseif($apiRankText == $matchWins) {

                        }
                        if($apiLosses > $matchLosses) {
                            $matchDifference = $apiLosses - $matchLosses;
                            $matchFinal = $rankThree + $matchDifference;
                            mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterFirstName', '$apiRankText', '$apiLosses', '$matchFinal')");
                            header("Location: #");
                        }elseif($apiRankText == $matchLosses) {

                        }
                    }
                }
            }

            // Get Current rank Emblem
            if($rankOne >= '0' & $rankOne <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '1500' & $rankOne <= '1999') {
                $cRankImage = 'tier2.png';
            }elseif($rankOne >= '2000' & $rankOne <= '2499') {
                $cRankImage = 'tier3.png';
            }elseif($rankOne >= '2500' & $rankOne <= '2999') {
                $cRankImage = 'tier4.png';
            }elseif($rankOne >= '3000' & $rankOne <= '3499') {
                $cRankImage = 'tier5.png';
            }elseif($rankOne >= '3500' & $rankOne <= '3999') {
                $cRankImage = 'tier6.png';
            }elseif($rankOne >= '4000' & $rankOne <= '5000') {
                $cRankImage = 'tier7.png';
            }elseif($rankOne > '5000') {
                $cRankImage = 'tier7.png';
            }

            // Basic Info
            $dRankImage = "00.png";
            $apiRankImage = "00.png";
            $orderStatus = "(In Progress)";
        }

        if($pComplete == "1") {
            $orderStatus = "(Completed)";
            $rankThree = "0";
        }
    }elseif($orderType == "2") { // Placement Match
        // Basic Info
        $orderTitle = "Placement Match";
        $orderStatus = "(Pending)";
        $firstCol = "Current In-Game Rank";
        $secondCol = "Total Games Played";
        $thirdCol = "Games Left";
        $rankOne = $currentRank; // SHOULD be rank returned by API
        $rankThree = $desiredRank; // Amount of games left
        $orderPercent = "0";

        if($custUsername == "username") {
            // Basic info
            $orderStatus = "(Awaiting User Input)";
            $rError = null;
            $pError = null;
            $uError = null;

            // Get Current rank Emblem
            if($rankOne >= '0' & $rankOne <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '1500' & $rankOne <= '1999') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '2000' & $rankOne <= '2499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '2500' & $rankOne <= '2999') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '3000' & $rankOne <= '3499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '3500' & $rankOne <= '3999') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '4000' & $rankOne <= '5000') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne > '5000') {
                $cRankImage = 'tier1.png';
            }else{
                $cRankImage = "tier1.png";
            }
            $rankOne = "0";

            // Placeholder API Rank Emblem and Rank
            $apiRankImage = "00.png";
            $apiRankText = "0";

            // Desired Rank Template
            $dRankImage = "00.png";
            $cStart = "";
            $cEnd = "";
        }else{
            // Basic Info
            $orderStatus = "(In Progress)";
            $cStart = "<!--";
            $cEnd = "-->";
            $uError = null;
            
            // Get Region, Otherwise throw error
            if($custRegion == 1) {
                $uRegion = "us";
                $rError = null;
            }elseif($custRegion == 2) {
                $uRegion = "eu";
                $rError = null;
            }elseif($custRegion == 3) {
                $uRegion = "kr";
                $rError = null;
            }elseif($custRegion == 0) {
                $uRegion = null;
                $rError = "<p class='alert alert-danger'>You must set a region if you would like to get your current in-game rank.</p>";
            }

            // Get Platform, Otherwise throw error
            if($custPlatform == 1) {
                $uPlatform = "pc";
                $pError = null;
            }elseif($custPlatform == 2) {
                $uPlatform = "psn";
                $pError = null;
            }elseif($custPlatform == 3) {
                $uPlatform = "xbl";
                $pError = null;
            }elseif($custPlatform == 0) {
                $uPlatform = null;
                $pError = "<p class='alert alert-danger'>You must set a platform if you would like to get your current in-game rank.</p>";
            }

            if($custRegion == 0 OR $custPlatform == 0) {
                $apiRankImage = "tier1.png";
                $apiRankText = "0";
            }else{
                $custUsername = str_replace('#', '-', $custUsername);
                $apiURL = @file_get_contents('http://ow-api.herokuapp.com/profile/'.$uPlatform.'/'.$uRegion.'/'.$custUsername);

                if($apiURL) {
                    $getAPIJSON = json_decode($apiURL);
                }else{
                    $getAPIJSON = null;
                }

                if($getAPIJSON == null) {
                    $apiRankText = "0";
                    $uError = "<p class='alert alert-danger'>There has been an error fetching your account from the API. Please make sure everything is correct.</p>";
                    $orderStatus = "(Awaiting User Input)";
                    $orderPercent = "0";
                }else{
                    $uError = null;
                    $apiRank = $getAPIJSON->competitive->rank;
                    $rankOne = $apiRank;

                    $apiRankText = $getAPIJSON->games->competitive->played;

                    $rankThree = $desiredRank;

                    $orderPercent = "0";
                    $currentDT = date("m/d/Y h:i A");

                    $getMatches = "SELECT * FROM ob_matches WHERE `oID` = '".$uID."'";
                    $matchesQuery = $con->query($getMatches);
                    $getMatchInfo = mysqli_fetch_array($matchesQuery);
                    
                    $matchesLeft = $getMatchInfo['ratingChange'];
                    $mRC = $getMatchInfo['ratingChange'];

                    $apiTotalMatches = $apiRankText + $rankThree;
                    $apiMatchDifference = $apiTotalMatches - $apiRankText;

                    if($pComplete == "0") {
                        // $apiRankText = "25"; // Debugger
                        // $apiMatchDifference = "1"; // Debugger
                        if($matchesQuery->num_rows < 1) {
                            mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterUsername', '$apiRankText', '$rankThree', '0')");
                            header("Location: #");
                        }elseif($matchesQuery->num_rows > 0) {
                            if($apiMatchDifference < $matchesLeft) {
                                $mDiff = $matchesLeft - $apiMatchDifference;
                                $matchFinal = $matchesLeft - $mDiff;
                                $mFinal = $apiMatchDifference - $mRC;

                                if($mFinal < 0) {
                                    mysqli_query($con, "INSERT INTO ob_matches (oID, datetime, booster, rating, ratingChange, ratingLoss) VALUES ('$uID', '$currentDT', '$boosterUsername', '$apiRankText', '$matchFinal', '0')");
                                header("Location: #");
                                }else{
                                }
                            }elseif($apiMatchDifference == $matchesLeft){
                                $mDiff = $matchesLeft - $apiMatchDifference;
                                $matchFinalSub = $mRC - $mDiff;
                                if($matchFinalSub <= 0) {
                                    $orderStatus = "(Completed)";
                                    $matchFinal = 0;
                                    header("Location: #");
                                }
                            }
                            $rankThree = $matchesLeft;
                        }
                    }else{
                        $rankThree = "0";
                    }
                }
            }

            // Get Current rank Emblem
            if($rankOne >= '0' & $rankOne <= '1499') {
                $cRankImage = 'tier1.png';
            }elseif($rankOne >= '1500' & $rankOne <= '1999') {
                $cRankImage = 'tier2.png';
            }elseif($rankOne >= '2000' & $rankOne <= '2499') {
                $cRankImage = 'tier3.png';
            }elseif($rankOne >= '2500' & $rankOne <= '2999') {
                $cRankImage = 'tier4.png';
            }elseif($rankOne >= '3000' & $rankOne <= '3499') {
                $cRankImage = 'tier5.png';
            }elseif($rankOne >= '3500' & $rankOne <= '3999') {
                $cRankImage = 'tier6.png';
            }elseif($rankOne >= '4000' & $rankOne <= '5000') {
                $cRankImage = 'tier7.png';
            }elseif($rankOne > '5000') {
                $cRankImage = 'tier7.png';
            }

            // Basic Info
            $dRankImage = "00.png";
            $apiRankImage = "00.png";
        }

        if($pComplete == "1") {
            $orderStatus = "(Completed)";
        }
    }
?>

<!-- Main Content -->

<div class="section" id="demo">
    <div class="container">
        <!-- New Order Success Alert -->
        <div class="row order-row">
            <div class="col-12 margin-top-row small-piece">
                <div class="row">
                    <div class="col-12 col-lg-7">
                        <div class="block">
                            <div class="block-head">
                                <?php echo $orderTitle." ".$orderStatus; ?>
                                <br />
                                <!-- API Errors -->
                                <?php
                                    echo $rError;
                                    echo $pError;
                                    echo $uError;
                                ?>
                            </div>
                            <div class="block-body no-pad">
                                <div class="rank-list">
                                    <div class="rank">
                                        <p class="head"><?php echo $firstCol; ?></p>
                                        <img src="/static/img/rankImages/<?php echo $cRankImage; ?>" class="img-responsive" />
                                        <p class="value"><?php echo $rankOne; ?></p>
                                    </div>
                                    <div class="rank">
                                        <p class="head"><?php echo $secondCol; ?></p>
                                        <img src="/static/img/rankImages/<?php echo $apiRankImage; ?>" class="img-responsive" />
                                        <p class="value"><?php echo $apiRankText; ?></p>
                                    </div>
                                    <div class="rank">
                                        <p class="head"><?php echo $thirdCol; ?></p>
                                        <img src="/static/img/rankImages/<?php echo $dRankImage; ?>" class="img-responsive" />
                                        <p class="value"><?php echo $rankThree; ?></p>
                                    </div>
                                </div>
                                <?php echo $cStart; ?> <div class="order-progress">
                                    <p class="head">Progress</p>
                                    <div class="order-progress-slider" data-value="<?php echo $orderPercent; ?>"></div>
                                </div> <?php echo $cEnd; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5">
                        <!-- Payment Error Notification -->
                        <div class="block">
                            <div class="block-head">Like our services?</div>
                            <div class="block-body text-center">
                                <?php
                                    if($boosterID == "0") {
                                ?> 
                                <form>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="10" />
                                            <span class="input-group-addon inv">.00</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-order" style="cursor: default;" disabled>Leave a Tip</button>
                                    </div>
                                </form>
                                <?php
                                    }else{
                                ?>
                                <form action="/tipCheckout.php?oID=<?php echo $uID; ?>&bID=<?php echo $boosterID; ?>" method="POST">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" class="form-control" placeholder="10" id="tipAmount" name="tipAmount" required />
                                            <span class="input-group-addon inv">.00</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-order" id="submitTip" name="submitTip">Leave a Tip</button>
                                    </div>
                                </form>
                                <?php
                                    }
                                ?>
                            </div>
                        </div> <!-- Else: Tip Enabled -->
                        <div class="block">
                            <div class="block-body">
                                <div class="booster">
                                    <span class="booster-head">Booster</span>
                                    <span class="booster-name">
                                        <?php
                                            if($boosterID == "0") {
                                                echo "None";
                                            }else{
                                                echo $boosterUsername;
                                            }
                                        ?>
                                    </span>
                                    <!-- Stream Button -->
                                    <div class="streaming">
                                        <form action="" method="POST">
                                            <a><input type="submit" id="changePause" name="changePause" class="btn btn-order changePause" value="<?php echo $pauseValue; ?>" /></a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 margin-top-row big-piece">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="block">
                            <div class="block-head">Options</div>
                            <div class="block-body">
                                <ul class="nav nav-pills nav-fill nav-options">
                                    <li class="nav-item"><a class="nav-link active" href="#gameAccountTab" data-toggle="tab"><i class="fa fa-gamepad"></i> Game Account</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#herosTab" data-toggle="tab"><i class="fa fa-bolt"></i> Heroes</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#notesTab" data-toggle="tab"><i class="fa fa-pencil"></i> Notes</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#boosterTab" data-toggle="tab"><i class="fa fa-space-shuttle"></i> Booster</a></li>
                                </ul>
                                <div class="tab-content options-tab-content">
                                    <div class="tab-pane active show shorten-center fade" id="gameAccountTab" role="tabpanel">
                                        <p class="info">Please submit your information so your booster can sign into your account.</p>
                                        <!-- Region -->
                                        <!-- Platform -->
                                        
                                        <?php
                                            if($custRegion == "0") {
                                                $s0 = 'selected="selected"';
                                            }elseif($custRegion == "1") {
                                                $s1 = 'selected="selected"';
                                            }elseif($custRegion == "2") {
                                                $s2 = 'selected="selected"';
                                            }elseif($custRegion == "3") {
                                                $s3 = 'selected="selected"';
                                            }

                                            if($custPlatform == "0") {
                                                $p0 = 'selected="selected"';
                                            }elseif($custPlatform == "1") {
                                                $p1 = 'selected="selected"';
                                            }elseif($custPlatform == "2") {
                                                $p2 = 'selected="selected"';
                                            }elseif($custPlatform == "3") {
                                                $p3 = 'selected="selected"';
                                            }
                                        ?>
                                        
                                        <form action="" method="POST">
                                            <div class="form-elements">
                                                <div class="form-elements">
                                                    <div class="form-group">
                                                        <label class="control-label">Platform</label>
                                                        <select class="form-control" name="platformSelection" id="platformSelection">
                                                            <option value="1" <?php if(isset($p1)) {echo $p1;} if(isset($p0)) {echo $p0;} ?>>PC</option>
                                                            <option value="2" <?php if(isset($p2)) {echo $p2;} ?>>Playstation</option>
                                                            <option value="3" <?php if(isset($p3)) {echo $p3;} ?>>Xbox</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Region</label>
                                                        <select class="form-control" name="regionSelection" id="regionSelection">
                                                            <option value="1" <?php if(isset($s1)) {echo $s1;} if(isset($s0)) {echo $s0;} ?>>US</option>
                                                            <option value="2" <?php if(isset($s2)) {echo $s2;} ?>>EU</option>
                                                            <option value="3" <?php if(isset($s3)) {echo $s3;} ?>>Asia</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Account</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                            <input type="text" class="form-control" id="accEmail" name="accEmail" value="<?php echo $custEmail; ?>" /> <!-- Set Value -->
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                            <input type="text" class="form-control" id="accUsername" name="accUsername" value="<?php echo $custUsername; ?>" /> <!-- Set Value -->
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                            <input type="password" class="form-control" id="accPassword" name="accPassword" value="<?php echo $custPassword; ?>" /> <!-- Set Value -->
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-order" id="submitAccInfo" name="submitAccInfo" value="Submit" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane shorten-center" id="herosTab" role="tabpanel">
                                        <p class="info">Sorry, choosing heroes is not available at this time. Please check back $boosterUsername.</p>
                                        <!-- <p class="info">Please select at least 6 heroes. If you won't select any, then your booster will choose heroes to play by themselves.</p>
                                        <div class="hero-selection" id="options-hero-selector">
                                            <div class="hero-row">
                                                <div class="hero-class">
                                                    <img src="/static/img/demo/heros/offense.png" />
                                                </div>
                                                <div class="heros">
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/genji.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/mccree.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/pharah.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/reaper.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/soldier76.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/sombra.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/tracer.png" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hero-row">
                                                <div class="hero-class">
                                                    <img src="/static/img/demo/heros/defense.png" />
                                                </div>
                                                <div class="heros">
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/bastion.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/hanzo.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/junkrat.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/mei.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/torbjorn.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/widowmaker.png" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hero-row">
                                                <div class="hero-class">
                                                    <img src="/static/img/demo/heros/tank.png" />
                                                </div>
                                                <div class="heros">
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/dva.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/orisa.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/reinhardt.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/roadhog.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/winston.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/zarya.png" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hero-row">
                                                <div class="hero-class">
                                                    <img src="/static/img/demo/heros/support.png" />
                                                </div>
                                                <div class="heros">
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/ana.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/lucio.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/mercy.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/symmetra.png" />
                                                    </div>
                                                    <div class="hero">
                                                        <img src="/static/img/demo/heros/zenyatta.png" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="tab-pane shorten-center" id="notesTab" role="tabpanel">
                                        <!-- Get Notes from DB -->
                                        <p class="info">You can leave additional information here.</p>
                                        <div class="form-elements">
                                            <form action="" method="POST">
                                                <div class="form-group">
                                                    <?php echo $noteTextArea; ?>
                                                </div>
                                                <div class="form-group">
                                                    <input class="btn btn-order" type="submit" value="Submit" id="submitNotes" name="submitNotes">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane shorten-center" id="boosterTab" role="tabpanel">
                                        <p class="info">Sorry, choosing a previous booster is not available at this time.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="block live-chat-block">
                            <div class="block-head">Live Chat</div>
                            <div class="block-body no-pad">
                                <div class="live-chat-container">
                                    <div class="chat-input">
                                        <input type="text" class="form-control" placeholder="Chat Disabled." />
                                        <i class="fa fa-send"></i>
                                    </div>
                                    <div class="chat-container">
                                        <div class="message-box">
                                            <p class="intro"><b>Admin</b> <span>00:00</span></p>
                                            <div class="message">Hi! Sorry, our chat is still in the works. If you need assistance, or need to communicate with your booster, please contact our 24/7 support.</div>
                                            <!--
                                            <div class="message-box inverse">
                                                <p class="intro"><b>You</b> <span>11:19</span></p>
                                                <div class="message">
                                                    User Message
                                                </div>
                                            </div>
                                            -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- If OrderType = Skill Rating or Solo / Duo or Placement -->
            <div class="col-12 margin-top-row">
                <div class="block game-history">
                    <div class="block-head">Game History</div>
                    <div class="block-body no-pad">
                        <?php

                            if($orderType == "0") { // Skill Rating
                                if(isset($tableOutput)) {
                                    echo $tableOutput;
                                }else{
                                    $skillratingMatches = "SELECT * FROM ob_matches WHERE `oID`='$uID' ORDER BY mID DESC LIMIT 6";
                                    $srMatchesResult = $con->query($skillratingMatches);

                                    echo '
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Booster</th>
                                                    <th>Result</th>
                                                    <th>Skill Rating</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    ';
                                        while($row = $srMatchesResult->fetch_assoc()) {
                                            $sdDate = $row['datetime'];
                                            $sdBooster = $row['booster'];
                                            $sdWins = $row['rating'];
                                            $sdLosses = $row['ratingChange'];
                                            
                                            echo '<tr>';
                                            echo '<td>'.$sdDate.'</td>';
                                            echo '<td>'.$sdBooster.'</td>';
                                            echo '<td>'.$sdWins.'</td>';
                                            echo '<td>'.$sdLosses.'</td>';
                                            echo '</tr>';
                                        }
                                    echo '
                                            </body>
                                        </table>
                                    ';
                                }
                            }elseif($orderType == "1") { // Skill Rating
                                $soloduoMatches = "SELECT * FROM ob_matches WHERE `oID`='$uID' ORDER BY mID DESC LIMIT 6";
                                $sdmatchResult = $con->query($soloduoMatches);
                                echo '
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Booster</th>
                                                <th>Wons</th>
                                                <th>Losses</th>
                                                <th>Games Left</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';
                                    while($row = $sdmatchResult->fetch_assoc()) {
                                        $sdDate = $row['datetime'];
                                        $sdBooster = $row['booster'];
                                        $sdWins = $row['rating'];
                                        $sdLosses = $row['ratingChange'];
                                        $sdGamesLeft = $row['ratingLoss'];
                                        
                                        echo '<tr>';
                                        echo '<td>'.$sdDate.'</td>';
                                        echo '<td>'.$sdBooster.'</td>';
                                        echo '<td>'.$sdWins.'</td>';
                                        echo '<td>'.$sdLosses.'</td>';
                                        echo '<td>'.$sdGamesLeft.'</td>';
                                        echo '</tr>';
                                    }
                                echo '
                                        </body>
                                    </table>
                                ';
                            }elseif($orderType == "2") { // Placement Match
                                $placementMatches = "SELECT * FROM ob_matches WHERE `oID`='$uID' ORDER BY mID DESC LIMIT 6";
                                $pmmatchResult = $con->query($placementMatches);
                                echo '
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Booster</th>
                                                <th>Total Matches</th>
                                                <th>Plays Needed</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                ';
                                while($row = $pmmatchResult->fetch_assoc()) {
                                    $pmDate = $row['datetime'];
                                    $pmBooster = $row['booster'];
                                    $pmTotalPlays = $row['rating'];
                                    $pmPlaysLeft = $row['ratingChange'];
                                    
                                    echo '<tr>';
                                    echo '<td>'.$pmDate.'</td>';
                                    echo '<td>'.$pmBooster.'</td>';
                                    echo '<td>'.$pmTotalPlays.'</td>';
                                    echo '<td>'.$pmPlaysLeft.'</td>';
                                    echo '</tr>';
                                }
                                echo '
                                        </tbody>
                                    </table>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Main Content -->

<?php require($sRoot.'/include/footer.php'); ?>