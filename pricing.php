    <?php $page = "pricing"; require('./include/navbar.php'); ?>

      <!-- MAIN CONTENT -->

      <?php
        if(isset($_GET['success'])) {
            $getPaymentResult = $_GET['success'];

            if($getPaymentResult == "false") {
                $badPayment = '<div class="alert alert-danger"><b>Payment unsuccessful. Please try again.</b></div>';
            }
        }
      ?>

        <div class="section" id="pricing">
            <div class="container">
                <div class="main-content-panel text-left">
                    <h2 class="head">Overwatch Boost</h2>
                    <h5 class="tagline">We operate on all platforms <i class="fa fa-windows"></i> <img src="static/img/pricing/xbox-icon.png"> <img src="static/img/pricing/playstation-icon.png"></h5>

                    <?php if(isset($badPayment)) {echo $badPayment;}else{} ?>

                    <div class="row main-row">

                        <div class="col-12">

                            <ul class="nav nav-pills nav-fill">
                              <li class="nav-item">
                                <a class="nav-link active" href="#skillRatingTab" data-toggle="tab" role="tab">Skill Rating</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#soloDuoGamesTab" data-toggle="tab" role="tab">Solo/Duo Games</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#placementGamesTab" data-toggle="tab" role="tab">Placement Games</a>
                              </li>
                            </ul>

                        </div>

                        <div class="col-12">
                            <div class="tab-content" id="pricingTabs">
                                <?php
                                    require($sRoot."/include/skillRating.php");
                                    require($sRoot."/include/soloDuo.php");
                                    require($sRoot."/include/placement.php");
                                ?>
                                
                            </div>
                        </div>

                        
                    </div>  

                </div>
            </div>
        </div>

        <!-- ==============
          REVIEWS
         =============== -->



        <div id="reviews" class="pricing-reviews">

            <div class="row">

                <div class="col-md-6 review-col-left">

                    <div class="reviews-container">

                        <div class="swiper-wrapper">

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="4000">

                                <p class="review-text">
                                    "I'm never buying my LoL accounts from another site."
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="4000">

                                <p class="review-text">
                                    "Best LoL account I've ever purchased"
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                        </div>

                    </div>

                </div>

                <div class="col-md-6 review-col-right">

                    <div class="reviews-container">

                        <div class="swiper-wrapper">

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="3000">

                                <p class="review-text">
                                    "Best LoL account I've ever purchased"
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                            <!-- review starts here -->
                            <div class="review swiper-slide" data-swiper-autoplay="3000">

                                <p class="review-text">
                                    "I'm never buying my LoL accounts from another site."
                                </p>

                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                            <!-- review ends here -->

                        </div>

                    </div>

                </div>

                <img src="static/img/reviewio.png" alt="Reviews.io Logo" class="img-responsive hidden-sm">

            </div>

            <div class="text container">

                <p class="count">Tier1Boost is rated <b>5.00</b> stars by Review.io based on <b>800</b> merchant reviews.</p>

                <p class="confidence">
                    All our customer reviews are from genuine Tier1Boost customers and are independently
                    <br>collected &amp; verified by Reviews.co.uk. To view our full review page <a href="#">click here</a>.
                </p>

            </div>

        </div>

        <!-- =============
          REVIEWS - END
         ============== -->

         <!-- HOW IT WORKS -->

            <div id="howandwhy">

                <div class="big-block padding-top padding-bottom">
                    <div class="container">
                        <p class="head">What is Overwatch boosting and how does it work?</p>
                        <p class="desc">Boostards provides “boosting” services for Overwatch players who feel like they are stuck at a certain skill rating in the game to climb out of elo hell and into a place where they can realize their true potential. Luck can be a huge factor in the Overwatch competitive environment and what we aim to do is to take the luck factor out of the game, improve the gaming experience for players having a bad time, and offer a bit of help to those who need it.</p>
                        <p class="desc">We currently provide 2 kinds of services, across all platforms (PC, XBOX, PS4) – Skill Rating Boost where we pilot your account to a certain rank for you, and Duo Queue Boosting where you play on your account, duo or group up with our professional boosters, and get to the rank you desire by yourself, but with some friendly help along the way.</p>
                        <p class="desc">We offer personal and customized solutions here at Boostards. If you want a job to be done quick and hassle free, Skill Rating Boost is the way to go. If you want to experience the climb yourself, Duo Queue Boosting is something you would definitely enjoy. If there is something that you’d like but don’t see on our website, just talk to us on our live chat or send us an email, and we’ll be happy to arrange something for you.</p>
                    </div>
                </div>

                <div class="big-block padding-top padding-bottom">
                    <div class="container">
                        <p class="head">What is Overwatch boosting and how does it work?</p>
                        <p class="desc">Boostards provides “boosting” services for Overwatch players who feel like they are stuck at a certain skill rating in the game to climb out of elo hell and into a place where they can realize their true potential. Luck can be a huge factor in the Overwatch competitive environment and what we aim to do is to take the luck factor out of the game, improve the gaming experience for players having a bad time, and offer a bit of help to those who need it.</p>
                        <p class="desc">We currently provide 2 kinds of services, across all platforms (PC, XBOX, PS4) – Skill Rating Boost where we pilot your account to a certain rank for you, and Duo Queue Boosting where you play on your account, duo or group up with our professional boosters, and get to the rank you desire by yourself, but with some friendly help along the way.</p>
                        <p class="desc">We offer personal and customized solutions here at Boostards. If you want a job to be done quick and hassle free, Skill Rating Boost is the way to go. If you want to experience the climb yourself, Duo Queue Boosting is something you would definitely enjoy. If there is something that you’d like but don’t see on our website, just talk to us on our live chat or send us an email, and we’ll be happy to arrange something for you.</p>
                    </div>

                    <img src="static/img/pricing/tracer.webp" class="img-responsive footer-bottom-img">

                </div>

            </div>

         <!-- ============ -->

      <?php require($sRoot.'/include/footer.php'); ?>