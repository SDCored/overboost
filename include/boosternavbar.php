<?php

    ob_start(); $sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot . "/app/connect.php");

    if($globalUserAccountType == "0") { // Normal User Account
        header("location: /");
        $adminLink = "";
    }else{ // Booster or Admin
        $adminLink = "";
        if($globalUserAccountType == "2") {
            $adminLink = '<li class="nav-item" style="line-height: 22px;"><a href="/a/" class="nav-link">ADMIN</a></li>'; // For some reason, the admin link doesn't line up with the other 2 links on this navbar. Setting a line-height of 22px seemed to help tho
        }
    }

    if(isset($userSession)) {
        // Nada
    }else{
        header("Location: /");
    }

    if(isset($_GET['IncorrectBid'])) {
        $incorrectBid = "Your bid is either too small or not valid.";
    }
    
    if($page == "pricing") {
        $pageType = "pricing";
    }elseif($page == "demo") {
        $pageType = "demo";
    }elseif($page == "current") {
        $pageType = "demo";
    }elseif($page == "dashboard") {
        $pageType = "demo";
    }else{
        $pageType = "demo";
    }

?>

<!DOCTYPE HTML>

<!-- ================================= 

App: Tier1Boost
Version: 1.0
Contact:
   - http://www.hardikmanktala.com
   - chat@hardikmanktala.com
   - Skype: hardik.manktala

   =================================== -->

<html lang="en-us">

<head>
    <!-- ===============================================
         Title and Meta Tags
         ================================================ -->
    <meta charset="utf-8">
    <title>Overwatch Boost</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="This is the meta description of the site">
    <meta name="keywords" content="some, keywords, like, this">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- ===============================================
         CSS
         ================================================ -->
    <link type="text/css" rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/tether.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/tether-theme-arrows.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/tether-theme-arrows-dark.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/bootstrap-slider.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/swiper.min.css">
    <link type="text/css" id="main" href="/static/css/root.css" rel="stylesheet">
    <link type="text/css" href="/static/css/responsive.css" rel="stylesheet">
    <script type="text/javascript" href="/static/js/jquery-3.2.1.min.js"></script>
    <!-- ============================================ -->
</head>

<body data-pageSlug="<?php echo $pageType; ?>">

    <!-- <div class="modal fade adminModal" id="skillRatingBidModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="header-strip">

                    <div class="header-main">
                        <div class="tier start">
                            <img src="static/img/demo/ranks/1.png" alt="tier">
                            <p class="small">Gold 5 0 LP</p>
                            <p>Start</p>
                        </div>
                        <div class="tier fa">
                            <i class="fa fa-angle-double-right"></i>
                        </div>
                        <div class="tier end">
                            <img src="static/img/demo/ranks/2.png" alt="tier">
                            <p class="small">Gold 4 0 LP</p>
                            <p>Desired</p>
                        </div>
                    </div>

                    <div class="counter-strip">

                        <div class="hero-counter admin-counter" data-minutes="50" data-seconds="20">
                            <div class="row">
                                <div class="col timer-component">
                                    <div class="fyre-minutes">
                                        <p class="num">00</p>
                                        <p class="tt minute-slug">minutes</p>
                                    </div>
                                </div>
                                <div class="col timer-component">
                                    <div class="fyre-seconds">
                                        <p class="num">00</p>
                                        <p class="tt second-slug">seconds</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="body-strip">

                    <div class="bid-info">

                        <div class="left-block">
                            <p class="head">Current Bid</p>
                            <p class="value">$30</p>
                        </div>

                        <div class="right-block">
                            <p class="head">Autowin</p>
                            <p class="value">$50</p>
                        </div>

                    </div>

                    <div class="form-group">
                        <input type="text" placeholder="Input your bid" class="form-control" name="inputBid">
                        <button type="submit" class="btn btn-submit-bid">Submit Bid</button>
                    </div>

                </div>

            </div>
        </div>
    </div> -->


    <!-- =======================
      NAVBAR
      ======================== -->

    <nav class="navbar navbar-toggleable-md navbar-light bg-faded fixed-top admin-nav scrolled" id="primary-nav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>

            <button id="adminSidebarToggle"><i class="fa fa-align-left"></i></button>
            <a class="navbar-brand" href="#">BUYOVERWATCHBOOST</a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">

                </ul>
                <ul class="navbar-nav">
                    <!-- right items -->
                    <li class="nav-item">
                        <a class="nav-link" href="#"><img src="/static/img/admin/avatar.jpg" alt="profile pic" class="nav-prof-pic"><span class="name">[BOOSTER] <?php echo strToUpper($globalUserFirstName); ?></span></a>
                    </li>
                    <?php echo $adminLink; ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout.php"><i class="fa fa-power-off"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- =======================
      NAVBAR - END
      ======================== -->