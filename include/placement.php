                                <div class="tab-pane fade" id="placementGamesTab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12 col-xl-8">
                                            <form action="checkout3.php" method="POST">
                                            <div class="row text-row text-center">
                                                <div class="white-overlay"></div>
                                                <div class="col-sm-6">
                                                    <div class="text-block">
                                                        <p class="text">Previous Season Rank</p>
                                                         <select data-placeholder="Unranked" class="inside-text select" tabindex="2" name="rankType">
                                                            <option value="Unranked">Unranked</option>
                                                            <option value="rank1">0000 - 1499</option>
                                                            <option value="rank2">1500 - 1999</option>
                                                            <option value="rank3">2000 - 2499</option>
                                                            <option value="rank4">2500 - 2999</option>
                                                            <option value="rank5">3000 - 3499</option>
                                                            <option value="rank6">3500 - 3999</option>
                                                            <option value="rank7">4000+</option>
                                                         </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="text-block">
                                                        <p class="text">Number of Games</p>
                                                        <div class="text-group pricing-range-text" id="pricingRangeText3">
                                                            <button type="button" class="btn btn-left"><i class="fa fa-minus"></i></button>
                                                            <input type="text" class="inside-text" required autocomplete="off" placeholder="Boo" value="1" disabled>
                                                            <button type="button" class="btn btn-right"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="pricingSlider"><div class="pricing-slider placement-games">
                                                    </div>
                                                    <div class="slider-bottom-ruler">
                                                            <div class="number">1</div>
                                                            <div class="number">2</div>
                                                            <div class="number">3</div>
                                                            <div class="number">4</div>
                                                            <div class="number">5</div>
                                                            <div class="number">6</div>
                                                            <div class="number">7</div>
                                                            <div class="number">8</div>
                                                            <div class="number">9</div>
                                                            <div class="number">10</div>
                                                            

                                                        </div>
                                                    </div>

                                                    <div class="btn-group pricing-btn-group">
                                                        <a href="#" class="btn btn-streaming"><i class="fa fa-twitch"></i> Streaming (+15%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                        <a href="#" class="btn btn-streaming"><i class="fa fa-user-plus"></i> Duo Queue (+50%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                    </div>
                                                </div>

                                                <input type="text" name="hiddenGameAmount" id="hiddenGameAmount" value="1" style="display: none;" />
                                                <input type="text" name="hiddenAmount" id="hiddenAmount" value="3.25" style="display: none;" />

                                                <div class="col-sm-12">
                                                    <p class="text-mate">
                                                    Placement Match of <b><span id="numberOfGames">0</span></b> games for  <b class="strike">$<span id="discountPrice">4.06</span></b>  <b>$<span id="originalPrice">3.25</span></b>
                                                    </p>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="promo-row">
                                                        <!-- TODO: Later<div class="promo-enter">
                                                            <img src="http://placehold.it/70x70" alt="placeholder">
                                                            <form method="post">
                                                                <p class="head">Promotional Code <i class="fa fa-question" data-toggle="tooltip" title="Something"></i></p>
                                                                <div class="form-group">
                                                                    <input type="text" required>
                                                                    <input type="submit" class="btn btn-primary" value="Apply">
                                                                </div>
                                                            </form>
                                                        </div> -->
                                                        <div class="purchase" style="width: 100%;">
                                                            <button type="submit" class="btn btn-purchase" style="width: 100%;"><i class="fa fa-paypal"></i> Purchase</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </form>
                                        </div>
                                        <div class="col-md-12 col-xl-4">
                                            <div class="faq-ques">
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>