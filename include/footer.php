<footer class="footer footer-home">

            <div class="container">

                

                <div class="content-strip">

                    <div class="row">

                        <div class="col-12 col-md-6 col-lg-3 paragraph-col">
                            <div class="logo-strip">
                                <p>OverwatchBoost</p>
                            </div>
                            <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec nisi eu est suscipit pretium. Proin convallis dui et aliquam vestibulum. Praesent aliquet, est vitae eleifend elementum, orci odio malesuada felis, id congue sem erat eu nunc. Suspendisse placerat dapibus lacus a molestie. Suspendisse potenti. </p>

                        </div>

                        <div class="col-12 col-md-6 col-lg-3 link-list-col">
                        <p class="contact-head">Some Links</p>
                            <ul class="footer-links">

                                <li><a href="#"><i class="fa fa-caret-right"></i> About OverwatchBoost</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Why OverwatchBoost</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> OverwatchBoost Reviews</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> OverwatchBoost Pricing</a></li>

                            </ul>

                        </div>

                        <div class="col-12 col-md-6 col-lg-3 link-list-col-2">
                        <p class="contact-head">Links</p>
                            <ul class="footer-links">

                                <li><a href="#"><i class="fa fa-caret-right"></i> FAQs</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Contact Us</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Terms &amp; Conditions</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Sitemap</a></li>

                            </ul>

                        </div>

                        <div class="col-12 col-md-6 col-lg-3 contact-col">
                            <p class="contact-head">More Links</p>

                            <ul class="footer-links">

                                <li><a href="#"><i class="fa fa-caret-right"></i> FAQs</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Contact Us</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Terms &amp; Conditions</a></li>

                                <li><a href="#"><i class="fa fa-caret-right"></i> Sitemap</a></li>

                            </ul>

                        </div>

                    </div>

                </div>

                <div class="copyright-strip">
                    <p>2016 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec nisi eu est suscipit pretium. Proin convallis dui et aliquam vestibulum.</p>
                </div>

            </div>

        </footer>

        <!-- </div>

          </div> -->

    </div>

    </div>

    <!-- ===============================================
      SCRIPTS
      ================================================= -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="/static/js/libs/tether.min.js"></script>
    <script src="/static/js/libs/bootstrap.min.js"></script>
    <script src="/static/js/libs/bootstrap-slider.min.js"></script>
    <script src="/static/js/libs/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/static/js/root.js?id=<?php echo mt_rand(); ?>"></script>
    <script type="text/javascript" src="/static/js/counter.js?id=<?php echo mt_rand(); ?>"></script>
    <script src="/static/js/libs/particles.min.js"></script>
    <script src="/static/js/libs/swiper.jquery.min.js"></script>
    <script src="/static/js/updateRating.js"></script>
    
</body>

</html>