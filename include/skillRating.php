<div class="tab-pane fade show active" id="skillRatingTab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12 col-xl-8">
                                            <form action="checkout.php" method="POST" autocomplete="off" id="skillRatingForm">
                                                <div class="row text-row text-center">
                                                    <div class="white-overlay"></div>
                                                    <div class="col-sm-6">
                                                        <div class="text-block">
                                                            <p class="text">Current Skill Rating</p>
                                                            <input type="text" class="inside-text csr_1_text" name="csr_1_text" required autocomplete="off" placeholder="Boo" value="0" id="csr_1_text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="text-block">
                                                            <p class="text">DESIRED SKILL RATING</p>
                                                            <div class="text-group pricing-range-text" id="pricingRangeText1">
                                                                <button type="button" class="btn btn-left"><i class="fa fa-minus"></i></button>
                                                                <input type="text" class="inside-text desiredRank" name="desiredRank" required autocomplete="off" placeholder="Boo" value="50" disabled id="desiredRank">
                                                                <button type="button" class="btn btn-right"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="pricingSlider"><div class="pricing-slider skill-rating">
                                                        </div>
                                                        <div class="slider-bottom-ruler">
                                                                <div class="number">100</div>
                                                                <div class="number">4400</div>

                                                            </div>
                                                        </div>

                                                        <div class="btn-group pricing-btn-group" >
                                                            <a href="#" class="btn btn-streaming"><i class="fa fa-twitch"></i> Streaming (+15%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                            <a href="#" class="btn btn-streaming"><i class="fa fa-user-plus"></i> Duo Queue (+50%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <p class="text-mate">
                                                        Skill Rating boost from <b><span id="startingRating">10</span></b> to <b><span id="toRating">100</span></b> or more for  <b class="strike">$<span id="priceOrignal">105.63</span></b>  <b><span id="priceDiscount">$95.06</span></b>
                                                            <div id="inputUpdate">
                                                                <input type="text" id="pricingHiddenInput" name="inputPriceHidden" value="110" style="display: none" />

                                                                <input type="text" id="desiredRankHidden" name="desiredRankHidden" value="50" style="display: none" />

                                                                <input type="text" id="hiddenEmail" name="hiddenEmail" value="<?php if(isset($userSession)) { echo $globalUserEmail; }else{ $globalUserEmail = "none";} ?>" style="display: none;">
                                                            </div>
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="promo-row">
                                                            <!-- <div class="promo-enter">
                                                                <img src="http://placehold.it/70x70" alt="placeholder">
                                                                <form method="post">
                                                                    <p class="head">Promotional Code <i class="fa fa-question" data-toggle="tooltip" title="Something"></i></p>
                                                                    <div class="form-group">
                                                                        <input type="text">
                                                                        <input type="submit" class="btn btn-primary" value="Apply">
                                                                    </div>
                                                                </form>
                                                            </div> -->
                                                            <div class="purchase" style="width: 100%;">
                                                                <button type="submit" class="btn btn-purchase" style="width: 100%;"><i class="fa fa-paypal"></i> Purchase</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>


                                        </div>
                                        <div class="col-md-12 col-xl-4">
                                            <div class="faq-ques">
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>