<?php
    ob_start(); require("app/connect.php"); $sRoot = $_SERVER['DOCUMENT_ROOT'];
    
    if($page == "pricing") {
        $pageType = "pricing";
        $nBar = "scrolled";
    }elseif($page == "demo") {
        $pageType = "demo";
        $nBar = "scrolled";
    }elseif($page == "current") {
        $pageType = "demo";
        $nBar = "scrolled";
    }elseif($page == "dashboard") {
        $pageType = "demo";
        $nBar = "scrolled";
    }else{
        $pageType = "home";
        $nBar = "";
    }
?>

<!DOCTYPE HTML>

<!-- ================================= 

App: OverwatchBoost
Version: 1.0
Contact:
   - http://www.hardikmanktala.com
   - chat@hardikmanktala.com
   - Skype: hardik.manktala

=================================== -->

<html lang="en-us">

<head>
    <!-- ===============================================
         Title and Meta Tags
         ================================================ -->
    <meta charset="utf-8">
    <title>Overwatch Boost</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="This is the meta description of the site">
    <meta name="keywords" content="some, keywords, like, this">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- ===============================================
         CSS
         ================================================ -->
    <link type="text/css" rel="stylesheet" href="/static/css/bootstrap.min.css?id=<?php echo mt_rand(); ?>">
    <link type="text/css" rel="stylesheet" href="/static/css/tether.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/tether-theme-arrows.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/tether-theme-arrows-dark.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/bootstrap-slider.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/swiper.min.css">
    <link type="text/css" rel="stylesheet" href="/static/css/dev.css">
    <link type="text/css" id="main" href="/static/css/root.css" rel="stylesheet">
    <link type="text/css" href="/static/css/responsive.css" rel="stylesheet">
    <!-- ============================================ -->
</head>

<body data-pageSlug="<?php echo $pageType; ?>">

    <div id="wrapper">

        <!-- =======================
      LOGIN MODAL
      ======================== -->
      <?php

    if(isset($_POST['loginSubmit'])) {
        $email = strip_tags($_POST['loginEmail']);
        $password = strip_tags($_POST['loginPassword']);

        $email = $con->real_escape_string($email);
        $password = $con->real_escape_string($password);

        $query = $con->query("SELECT * FROM ob_users WHERE email='$email'");
        $row = $query->fetch_array();

        $count = $query->num_rows;

        $hashedPassword = md5($password);

        if(($hashedPassword == $row['password']) && $count=="1") {
            $_SESSION['userEmail'] = $row['email'];
            $userSession = $_SESSION['userEmail'];
            header("Location: /a/");
        }else{
            $msg = "<div class='alert alert-error'>Incorrect Email or Password!</div>";
        }
        $con->close();
    }

    ?>

        <div class="modal fade" id="loginModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-net"></div>
                <div class="modal-color-overlay"></div>

                <div class="modal-body">
                    <h1>Sign In</h1>

                    <?php if(isset($msg)) {echo $msg;} ?>

                    <form class="form-horizontal" name="loginForm" action="" method="POST">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                            <input type="email" class="form-control" id="loginEmail" name="loginEmail" placeholder="Email" />
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" id="loginPassword" name="loginPassword" placeholder="Password" />
                        </div>
                        <input type="submit" value="Sign In" class="btn btn-form" id="loginSubmit" name="loginSubmit" />

                        <a href="#">Forgot your password?</a>
                        <a href="#registerModal" data-toggle="modal" id="openRegisterModal">Don't have an account?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <!-- =======================
      LOGIN MODAL - END
      ======================== -->

        <!-- =======================
      REGISTER MODAL
      ======================== -->

        <?php

        if(isset($_POST['registerSubmit'])) {
            function get_client_ip() {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                    $ipaddress = getenv('HTTP_CLIENT_IP');
                else if(getenv('HTTP_X_FORWARDED_FOR'))
                    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                else if(getenv('HTTP_X_FORWARDED'))
                    $ipaddress = getenv('HTTP_X_FORWARDED');
                else if(getenv('HTTP_FORWARDED_FOR'))
                    $ipaddress = getenv('HTTP_FORWARDED_FOR');
                else if(getenv('HTTP_FORWARDED'))
                   $ipaddress = getenv('HTTP_FORWARDED');
                else if(getenv('REMOTE_ADDR'))
                    $ipaddress = getenv('REMOTE_ADDR');
                else
                    $ipaddress = 'UNKNOWN';
                return $ipaddress;
            }

            $userIP = get_client_ip();

            $regUsername = htmlentities($_POST['registerUsername'], ENT_QUOTES, 'UTF-8');
            $regSkype = htmlentities($_POST['registerSkype'], ENT_QUOTES, 'UTF-8');
            $regEmail = htmlentities($_POST['registerEmail'], ENT_QUOTES, 'UTF-8');
            $regConfirmEmail = htmlentities($_POST['registerConfirmEmail'], ENT_QUOTES, 'UTF-8');
            $regPassword = htmlentities($_POST['registerPassword'], ENT_QUOTES, 'UTF-8');
            $regConfirmPassword = htmlentities($_POST['registerConfirmPassword'], ENT_QUOTES, 'UTF-8');
            $regLocation = htmlentities($_POST['registerLocation'], ENT_QUOTES, 'UTF-8');

            $getCurrentEmail = "SELECT * FROM ob_users WHERE `email` = '$regEmail'";
            $findEmailResult = $con->query($getCurrentEmail);

            if($regEmail == $regConfirmEmail) {
                if($findEmailResult->num_rows > 0) {
                    $registerError = "<div class='alert alert-danger'>The email you provided is already in use.</div>";
                }else{
                    if($regPassword == $regConfirmPassword) {
                        $regPassword = md5($regPassword);

                        mysqli_query($con, "INSERT INTO ob_users (`firstName`, `lastName`, `email`, `skype`, `password`, `ppEmail`, `ipAddress`, `platform`, `region`, `skillRating`, `unpaidBalance`, `paidBalance`) VALUES ('$regUsername', '', '$regEmail', '$regSkype', '$regPassword', '$regEmail', '$userIP', '0', '$regLocation', '0', '0.00', '0.00')") or die(mysqli_error($con));
                        header("Location: /pricing.php");
                    }else{
                        $registerError = "<div class='alert alert-danger'>The passwords you provided were not the same, please try again.</div>";
                    }
                }
            }else{
                $registerError = "<div class='alert alert-danger'>The emails you provided did not match.</div>";
            }
        }

    ?>

    <div class="modal fade" id="registerModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-net"></div>
                <div class="modal-color-overlay"></div>
                <div class="modal-body">
                    <h1>Register</h1>

                    <?php if(isset($registerError)) {echo $registerError;} ?>

                    <form class="form-horizontal" name="registerForm" id="registerForm" action="" method="POST">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" name="registerUsername" id="registerUsername" required />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-skype"></i></span>
                                    <input type="text" class="form-control" placeholder="Skype (Optional)" aria-describedby="basic-addon1" name="registerSkype" id="registerSkype" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>
                                    <input type="email" class="form-control" placeholder="Email" aria-describedby="basic-addon1" name="registerEmail" id="registerEmail" required />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                    <input type="email" class="form-control" placeholder="Confirm Email Address" aria-describedby="basic-addon1" name="registerConfirmEmail" id="registerConfirmEmail" required />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock"></i></span>
                                    <input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" name="registerPassword" id="registerPassword" required />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="Confirm Password" aria-describedby="basic-addon1" name="registerConfirmPassword" id="registerConfirmPassword" required />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-location-arrow"></i></span>
                                    <input type="text" class="form-control" placeholder="Location (To be used on VPN)" aria-describedby="basic-addon1" id="registerLocation" name="registerLocation" required>
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Register" class="btn btn-form" name="registerSubmit" id="registerSubmit">
                        <a href="#loginModal" data-toggle="modal" id="openLoginModal">Already have an account?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <!-- =======================
      REGISTER MODAL - END
      ======================== -->
        
  <!-- =======================
      NAVBAR
      ======================== -->

        <nav class="navbar navbar-toggleable navbar-light bg-faded fixed-top <?php echo $nBar; ?>" id="top-nav">
            <div class="container">

                <div class="collapse navbar-collapse" id="topNavCollapse">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a href="#" class="nav-link"><i class="fa fa-facebook"></i></a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link"><i class="fa fa-skype"></i></a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link"><i class="fa fa-twitter"></i></a>
                        </li>

                    </ul>

                    <ul class="navbar-nav">

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span id="defaultLanguage"><i class="fa fa-globe"></i> &nbsp;English</span>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item language-list" href="#"> More coming soon..</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#" id="navbarDropdownMenuLink">
                                <span id="defaultCurrency"><i class="fa fa-dollar dropdown-icon"></i> USD</span>
                            </a>
                        </li>                        

                    </ul>

                </div>
            </div>
        </nav>

        <nav class="navbar navbar-toggleable-md navbar-light bg-faded fixed-top <?php echo $nBar; ?>" id="primary-nav">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
                <a class="navbar-brand" href="/">BUYOVERWATCHBOOST</a>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mr-auto left-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/demo.php">DEMO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">REVIEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">TUTORIAL</a>
                        </li>

                    </ul>
                    <ul class="navbar-nav">
                        <!-- right items -->
                        <?php
                            if(isset($userSession)) {
                                if($globalUserAccountType == "0") { // Normal User Account
                                    $adminLink = "";
                                    $boosterLink = "";
                                }else{ // Booster or Admin
                                    if($globalUserAccountType == "1") {
                                        $boosterLink = '<li class="nav-item"><a href="/booster/" class="nav-link" id="login-btn">BOOSTER DASHBOARD</a></li>';
                                        $adminLink = "";
                                    }
                                    if($globalUserAccountType == "2") {
                                        $boosterLink = '<li class="nav-item"><a href="/booster/" class="nav-link" id="login-btn">BOOSTER DASHBOARD</a></li>';
                                        $adminLink = '<li class="nav-item" style="line-height: 22px;"><a href="/a/" class="nav-link" id="login-btn"> ADMIN</a></li>'; // For some reason, the admin link doesn't line up with the other 2 links on this navbar. Setting a line-height of 22px seemed to help tho
                                    }
                                }
                                echo '
                                    <li class="nav-item">
                                        <a class="nav-link ctaBtn inverse" href="#" id="login-btn">'.strToUpper($globalUserFirstName).'</a>
                                    </li>
                                    '.$boosterLink.'
                                    '.$adminLink.'
                                    <li class="nav-item">
                                        <a class="nav-link" href="/logout.php" id="prices-btn"><i class="fa fa-power-off"></i></a>
                                    </li>
                                ';
                            }else{
                                echo '
                                    <li class="nav-item">
                                        <a class="nav-link ctaBtn inverse" href="#loginModal" data-toggle="modal" id="login-btn">LOGIN</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="/pricing.php" id="prices-btn">PRICES</a>
                                    </li>
                                ';
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- =======================
      NAVBAR - END
      ======================== -->