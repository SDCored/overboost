<div class="tab-pane fade" id="soloDuoGamesTab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12 col-xl-8">
                                            <form  action="checkout2.php" method="POST">
                                            <div class="row text-row text-center">
                                                <div class="white-overlay"></div>
                                                <div class="col-sm-6">
                                                    <div class="text-block">
                                                        <p class="text">Current Skill Rating</p>
                                                        <input type="text" class="inside-text" required autocomplete="off" placeholder="Boo" value="0" id="csr_2_text">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="text-block">
                                                        <p class="text">Number of Games</p>
                                                        <div class="text-group pricing-range-text" id="pricingRangeText2">
                                                            <button type="button" class="btn btn-left"><i class="fa fa-minus"></i></button>
                                                            <input type="text" class="inside-text" required autocomplete="off" placeholder="Boo" value="1" disabled>
                                                            <button type="button" class="btn btn-right"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="pricingSlider"><div class="pricing-slider solo-duo-gaming">
                                                    </div>
                                                        <div class="slider-bottom-ruler">
                                                            <div class="number">1</div>
                                                            <div class="number">2</div>
                                                            <div class="number">3</div>
                                                            <div class="number">4</div>
                                                            <div class="number">5</div>
                                                            <div class="number">6</div>
                                                            <div class="number">7</div>
                                                            <div class="number">8</div>
                                                            <div class="number">9</div>
                                                            <div class="number">10</div>
                                                            <div class="number">11</div>
                                                            <div class="number">12</div>
                                                            <div class="number">13</div>
                                                            <div class="number">14</div>
                                                            <div class="number">15</div>
                                                            <div class="number">16</div>
                                                            <div class="number">17</div>
                                                            <div class="number">18</div>
                                                            <div class="number">20</div>

                                                        </div>
                                                    </div>


                                                    <div class="btn-group pricing-btn-group" >
                                                        <a href="#" class="btn btn-streaming"><i class="fa fa-twitch"></i> Streaming (+15%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                        <a href="#" class="btn btn-streaming"><i class="fa fa-user-plus"></i> Duo Queue (+50%) <i class="fa fa-question question" data-toggle="tooltip" data-placement="top" title="Something"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <p class="text-mate">
                                                    Solo / Duo Games with Skill Rating Starting at <b><span id="sdCurrentRating">0</span></b>, with <b><span id="sdGamesAmount">1</span></b> games requested for  <b class="strike">$<span id="notDiscounted">00.00</span></b>  <b>$<span id="cPrice">3.75</span></b>
                                                    </p>
                                                </div>

                                                <input type="text" id="currentSR" name="currentSR" value="0" style="display: none;" />
                                                <input type="text" id="gameAmounts" name="gameAmounts" value="0" style="display: none;" />
                                                <input type="text" id="orderAmount" name="orderAmount" value="0" style="display: none;" />

                                                <input type="text" id="hiddenEmail" name="hiddenEmail" value="<?php if(isset($userSession)) { echo $globalUserEmail; }else{ $globalUserEmail = "none";} ?>" style="display: none;">

                                                <div class="col-sm-12">
                                                    <div class="promo-row">
                                                        <!-- TODO: Later <div class="promo-enter">
                                                            <img src="http://placehold.it/70x70" alt="placeholder">
                                                            <form method="post">
                                                                <p class="head">Promotional Code <i class="fa fa-question" data-toggle="tooltip" title="Something"></i></p>
                                                                <div class="form-group">
                                                                    <input type="text" required>
                                                                    <input type="submit" class="btn btn-primary" value="Apply">
                                                                </div>
                                                            </form>
                                                        </div> -->
                                                        <div class="purchase" style="width: 100%;">
                                                            <button type="submit" class="btn btn-purchase" style="width: 100%;"><i class="fa fa-paypal"></i> Purchase</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </form>
                                        </div>
                                        <div class="col-md-12 col-xl-4">
                                            <div class="faq-ques">
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                                <div class="faq-block">
                                                    <p class="question">
                                                        Why can I only select in ranges of 50 skill rating?
                                                    </p>
                                                    <p class="answer">
                                                        We do this since the SR gain for different accounts can vary greatly, so if you buy till 2000 SR, your order will be complete once we achieve 2000 or more SR.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>