<?php
    $page = "";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/boosternavbar.php');    
    require($sRoot.'/booster/boostFiles/sidebar.php');
?>

    <!-- Rules -->
    <div class="tab-pane bg-white" id="rules-tab" role="tabpanel">
        <div class="container">
            <h1 class="tab-head text-left color-primary">Rules &amp; Fees</h1>
            <ul class="rule-list">
                <li>Claiming an order and not playing within 1 hour: <span>$20</span></li>
                <li>Disrespectful to fellow Booster: <span>$20</span></li>
                <li>Disrespectful to Mod / Admin: <span>$50</span></li>
                <li>Failure to adhere to customer requirements: <span>$50</span></li>
                <li>Flaming In-Game: <span>$50</span></li>
                <li>Chat Restriction: <span>$100</span></li>
                <li>Advertisement of any kind: <span>$100</span></li>
                <li>Disrespectful to Customer: <span>$100</span></li>
                <li>Attempting to poach Customers: <span>$150</span></li>
                <li>Scripting / Cheating: <span>Immediate Account Closure</span></li>
                <li>Customer Account Ban: <span>Immediate Account Closure</span></li>
            </ul>
        </div>
    </div>

<?php require($sRoot.'/booster/boostFiles/footer.php'); ?>