<?php
    $page = "";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/boosternavbar.php');    
    require($sRoot.'/booster/boostFiles/sidebar.php');

    $getCompletedOrders = mysqli_query($con, "SELECT * FROM ob_orders WHERE `boosterUID` = '$globalUserUID' AND `playComplete` = '1' ORDER BY `oID` DESC");

    if(isset($_GET['orderCompeleted'])) {
        $orderCompleteNotif = "<center><div class='alert alert-success'><b>The order has been completed and the funds have been deposited into your account.</b></div></center>";
    }else{
        $orderCompleteNotif = null;
    }
?>

    <!-- Completed Orders -->
    <div class="tab-pane" id="completed-orders-tab" role="tabpanel">
        <div id="completed-orders">
            <?php echo $orderCompleteNotif; ?>
            <h1 class="tab-head text-center">Completed Orders</h1>
            
            <div class="tab-content">
                <div class="tab-pane fade show active no-padding" id="skill-rating-comod" role="tabpanel">
                    <div class="responsive-table">
                        <table class="admin-table responsive-mate display" id="skill-rating-comod-table">
                            <thead>
                                <tr class="head">
                                    <th>Date <i class="fa fa-sort"></i></th>
                                    <th>Region <i class="fa fa-sort"></i></th>
                                    <th>Platform <i class="fa fa-sort"></i></th>
                                    <th>Payout <i class="fa fa-sort"></i></th>
                                    <th>Status <i class="fa fa-sort"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    while($row = $getCompletedOrders->fetch_assoc()) {
                                        $orderDate = $row['dateCreated'];
                                        $orderRegion = $row['userRegion'];
                                        $orderPlatform = $row['userPlatform'];
                                        $orderStartRank = $row['currentRank'];
                                        $orderEndRank = $row['desiredRank'];
                                        $orderPayout = $row['bidAmount'];

                                        if($orderRegion == 1) {
                                            $orderRegion = 'US';
                                        }elseif($orderRegion == 2) {
                                            $orderRegion = 'EU';
                                        }elseif($orderRegion == 3) {
                                            $orderRegion = 'Asia';
                                        }else{
                                            $orderRegion = 'N/A';
                                        }

                                        if($orderPlatform == 0){
                                            $orderPlatform = '<i class="fa fa-times-circle"></i>';
                                        }elseif($orderPlatform == 1) {
                                            $orderPlatform = '<i class="fa fa-windows"></i>';
                                        }elseif($orderPlatform == 2) {
                                            $orderPlatform = '<img src="/static/img/demo/playstation-icon.png">';
                                        }elseif($orderPlatform == 3) {
                                            $orderPlatform = '<img src="/static/img/demo/xbox-icon.png">';
                                        }else{
                                            $orderPlatform = '<i class="fa fa-times-circle"></i>';
                                        }

                                        // Get Current Rank Emblem
                                        if($orderStartRank >= '0' & $orderStartRank <= '1499') {
                                            $cRankImage = 'tier1.png';
                                        }elseif($orderStartRank >= '1500' & $orderStartRank <= '1999') {
                                            $cRankImage = 'tier2.png';
                                        }elseif($orderStartRank >= '2000' & $orderStartRank <= '2499') {
                                            $cRankImage = 'tier3.png';
                                        }elseif($orderStartRank >= '2500' & $orderStartRank <= '2999') {
                                            $cRankImage = 'tier4.png';
                                        }elseif($orderStartRank >= '3000' & $orderStartRank <= '3499') {
                                            $cRankImage = 'tier5.png';
                                        }elseif($orderStartRank >= '3500' & $orderStartRank <= '3999') {
                                            $cRankImage = 'tier6.png';
                                        }elseif($orderStartRank >= '4000' & $orderStartRank <= '5000') {
                                            $cRankImage = 'tier7.png';
                                        }elseif($orderStartRank > '5000') {
                                            $cRankImage = 'tier7.png';
                                        }

                                        // Get Desired Rank Emblem
                                        if($orderEndRank >= '0' & $orderEndRank <= '1499') {
                                            $dRankImage = 'tier1.png';
                                        }elseif($orderEndRank >= '1500' & $orderEndRank <= '1999') {
                                            $dRankImage = 'tier2.png';
                                        }elseif($orderEndRank >= '2000' & $orderEndRank <= '2499') {
                                            $dRankImage = 'tier3.png';
                                        }elseif($orderEndRank >= '2500' & $orderEndRank <= '2999') {
                                            $dRankImage = 'tier4.png';
                                        }elseif($orderEndRank >= '3000' & $orderEndRank <= '3499') {
                                            $dRankImage = 'tier5.png';
                                        }elseif($orderEndRank >= '3500' & $orderEndRank <= '3999') {
                                            $dRankImage = 'tier6.png';
                                        }elseif($orderEndRank >= '4000' & $orderEndRank <= '5000') {
                                            $dRankImage = 'tier7.png';
                                        }elseif($orderEndRank > '5000') {
                                            $dRankImage = 'tier7.png';
                                        }

                                        echo '
                                            <tr>
                                                <td><p class="perk-desc">'.$orderDate.'</p></td>
                                                <td><p class="perk-desc">'.$orderRegion.'</p></td>
                                                <td><p class="perk-desc">'.$orderPlatform.'</p></td>
                                                <td><p class="perk-desc text-green"><b>+$'.number_format($orderPayout,2).'</b></b></td>
                                                <td><p class="perk-desc text-green"><b>Completed</b></p></td>
                                            </tr>
                                        ';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require($sRoot.'/booster/boostFiles/footer.php'); ?>