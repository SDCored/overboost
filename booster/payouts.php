<?php
    $page = "";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/boosternavbar.php');
    
    $getSkillRatings = "SELECT * FROM ob_orders WHERE orderType = 0 AND boosterUID = 0 ORDER BY `oid` DESC"; // Skill Rating Query
    $skillRatingsQuery = $con->query($getSkillRatings);

    $getSoloDuo = "SELECT * FROM ob_orders WHERE orderType = 1 AND boosterUID = 0 ORDER BY `oid` DESC"; // Solo / Duo Query
    $soloDuoQuery = $con->query($getSoloDuo);

    $getPlacement = "SELECT * FROM ob_orders WHERE orderType = 2 AND boosterUID = 0 ORDER BY `oid` DESC"; // Placement Query
    $placementQuery = $con->query($getPlacement);
    
    require($sRoot.'/booster/boostFiles/sidebar.php');

    require($sRoot.'/booster/boostFiles/payout.php');

    require($sRoot.'/booster/boostFiles/footer.php');
?>