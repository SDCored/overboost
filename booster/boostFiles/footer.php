        </div>
    </div>
</div>

    <!-- ===============================================
      SCRIPTS
      ================================================= -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="/static/js/libs/tether.min.js"></script>
    <script src="/static/js/libs/bootstrap.min.js"></script>
    <script src="/static/js/libs/particles.min.js"></script>
    <script src="/static/js/libs/bootstrap-slider.min.js"></script>
    <script src="/static/js/libs/swiper.jquery.min.js"></script>
    <script src="/static/js/counter.js?id=<?php echo mt_rand(); ?>"></script>
    <script src="/static/js/root.js"></script>
    <script src="/static/js/updateRating.js"></script>

</body>

</html>