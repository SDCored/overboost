<div class="admin-wrapper">
    <div class="admin-sidebar">
        <div class="intro-admin">
            <div>
                <p class="head">Booster Name</p>
                <br />
                <p class="value"><?php echo $globalUserFirstName; ?></p>
            </div>
            <div>
                <p class="head">Balance</p>
                <br />
                <p class="value text-green"><b>$<?php echo number_format($globalUserBoosterBalance, 2); ?></b></p>
            </div>
        </div>
        
        <ul class="nav nav-tabs">
            <li class="nav-item"><a href="index.php" class="nav-link"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class="nav-item"><a href="current.php" class="nav-link"><i class="fa fa-reorder"></i> <span>Current Order</span></a></li>

            <li class="nav-item"><a href="completed.php" class="nav-link"><i class="fa fa-check"></i> <span>Completed Orders</span></a></li>

            <li class="nav-item"><a href="payouts.php" class="nav-link"><i class="fa fa-money"></i> <span>Payouts</span></a></li>

            <li class="nav-item"><a href="rules.php" class="nav-link"><i class="fa fa-book"></i> <span>Rules &amp; Fees</span></a></li>

            <li class="nav-item"><a href="faq.php" class="nav-link"><i class="fa fa-question"></i> <span>FAQ</span></a></li>
        </ul>
    </div>
    <div class="admin-content">
        <div id="sidebarContent">
            <div class="intro-admin small-screen">
                <div>
                    <p class="head">Booster Name</p>
                    <p class="value"><?php echo $globalUserFirstName; ?></p>
                </div>
                <div>
                    <p class="head">Balance</p>
                    <p class="value text-green"><b>$<?php echo number_format($globalUserBoosterBalance); ?></b></p>
                </div>
            </div>