                        <div class="tab-pane fade no-padding" id="placement-matches" role="tabpanel">

                            <div class="responsive-table">
                                <table class="admin-table responsive-mate display" id="placement-matches-table">
                                    <thead>
                                        <tr class="head">
                                            <th>Date <i class="fa fa-sort"></i></th>
                                            <th>Region <i class="fa fa-sort"></i></th>
                                            <th>Platform <i class="fa fa-sort"></i></th>
                                            <th>Options <i class="fa fa-sort"></i></th>
                                            <th>Previous Rating <i class="fa fa-sort"></i></th>
                                            <th>Amount <i class="fa fa-sort"></i></th>
                                            <th>Amount <i class="fa fa-sort"></i></th>
                                            <th>Action <i class="fa fa-sort"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    while($row = $placementQuery->fetch_assoc()) {
                                        $userRegion = $row['userRegion'];
                                        if($userRegion == 0){
                                            $userRegion = 'None';
                                        }elseif($userRegion == 1) {
                                            $userRegion = 'US';
                                        }elseif($userRegion == 2) {
                                            $userRegion = 'EU';
                                        }elseif($userRegion == 3) {
                                            $userRegion = 'Asia';
                                        }else{
                                            $userRegion = 'None';
                                        }

                                        $userPlatform = $row['userPlatform'];
                                        if($userPlatform == 0){
                                            $userPlatform = '<i class="fa fa-times-circle"></i>';
                                        }elseif($userPlatform == 1) {
                                            $userPlatform = '<i class="fa fa-windows"></i>';
                                        }elseif($userPlatform == 2) {
                                            $userPlatform = '<img src="/static/img/demo/playstation-icon.png">';
                                        }elseif($userPlatform == 3) {
                                            $userPlatform = '<img src="/static/img/demo/xbox-icon.png">';
                                        }else{
                                            $userPlatform = '<i class="fa fa-times-circle"></i>';
                                        }

                                        $userStream = $row['streaming'];
                                        $userDuoQueue = $row['duoQueue'];
                                        if($userStream === "0" && $userDuoQueue === "0") {
                                            $userOptions = '<i class="fa fa-times-circle"></i>';
                                        }else{
                                            $userOptions = '';
                                            if($userStream == "1") {
                                                $userOptions = '<i class="fa fa-twitch"></i>';
                                            }else{}

                                            if($userDuoQueue == "1") {
                                                $userOptions = '<i class="fa fa-users"></i>';
                                            }else{}
                                        }

                                        $currentRank = $row['currentRank'];
                                        if($currentRank == 'rank1') {
                                            $cRankImage = "tier1.png";
                                        }elseif($currentRank == 'rank2' ) {
                                            $cRankImage = "tier2.png";
                                        }elseif($currentRank == 'rank3') {
                                            $cRankImage = "tier3.png";
                                        }elseif($currentRank == 'rank4') {
                                            $cRankImage = "tier4.png";
                                        }elseif($currentRank == 'rank5') {
                                            $cRankImage = "tier5.png";
                                        }elseif($currentRank == 'rank6') {
                                            $cRankImage = "tier6.png";
                                        }elseif($currentRank == 'rank7') {
                                            $cRankImage = "tier7.png";
                                        }else{
                                            $cRankImage = "tier1.png";
                                        }

                                        $desiredRank = $row['desiredRank'];
                                        if($desiredRank >= '0' & $desiredRank <= '1499') {
                                            $dRankImage = "tier1.png";
                                        }elseif($desiredRank >= '1500' & $desiredRank <= '1999') {
                                            $dRankImage = "tier2.png";
                                        }elseif($desiredRank >= '2000' & $desiredRank <= '2499') {
                                            $dRankImage = "tier3.png";
                                        }elseif($desiredRank >= '2500' & $desiredRank <= '2999') {
                                            $dRankImage = "tier4.png";
                                        }elseif($desiredRank >= '3000' & $desiredRank <= '3499') {
                                            $dRankImage = "tier5.png";
                                        }elseif($desiredRank >= '3500' & $desiredRank <= '3999') {
                                            $dRankImage = "tier6.png";
                                        }elseif($desiredRank >= '4000' & $desiredRank <= '5000') {
                                            $dRankImage = "tier7.png";
                                        }elseif($desiredRank > '5000') {
                                            $dRankImage = "tier7.png";
                                        }

                                        if($currentBid === "0.00") {
                                            $bidAmount = $row['amountPaid'] * 0.75;
                                            $bidAmountFinal = number_format($bidAmount);
                                        }else{
                                            $bidAmount = number_format($currentBid);
                                        }

                                        $bidDisabled = $row['biddingDisabled'];
                                        if($bidDisabled == 0) {
                                            $bidButton = '<a href="#placementClaimModal'.$row['oID'].'" data-toggle="modal" class="btn btn-table btn-claim">Claim</a>';
                                        }elseif($bidDisabled == 1) {
                                            $bidButton = '<a href="#" title="Customer has not set username and password."  disabled="disabled" class="btn btn-table btn-claim disableClick" style="background: #AAA; pointer-events: none;">Claim</a>';
                                        }
                                        
                                        echo '
                                            <tr>
                                                <td><p class="perk-desc">'.$row['dateCreated'].'</p></td>
                                                <td><p class="perk-desc">'.$userRegion.'</p></td>
                                                <td><p class="perk-desc">'.$userPlatform.'</p></td>
                                                <td><p class="perk-desc">'.$userOptions.'</p></td>
                                                <td><div class="start"><img src="/static/img/rankImages/'.$cRankImage.'" alt="tiericon" class="tier-icon"></div></td>
                                                <td><p class="perk-desc">'.$desiredRank.' Games</p></td>
                                                <td><p class="perk-desc">$'.number_format($bidAmount,2).'</p></td>
                                                <td>'.$bidButton.'</td>
                                            </tr>
                                        ';
                                        
                                        echo '
                                            <div class="modal fade adminModal" id="placementClaimModal'.$row['oID'].'">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="header-strip">
                                                            <div class="header-main">
                                                                <div class="tier start">
                                                                    <img src="/static/img/rankImages/'.$cRankImage.'" alt="tiericons">
                                                                    <p class="small">'.$currentRank.' LP</p>
                                                                    <p>Start</p>
                                                                </div>
                                                                <div class="tier fa">
                                                                    <i class="fa fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="tier start">
                                                                    <img src="/static/img/rankImages/'.$dRankImage.'" alt="tiericons">
                                                                    <p class="small">'.$desiredRank.' LP</p>
                                                                    <p>Start</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="body-strip">
                                                            <div class="bid-info">
                                                                <div class="left-block">
                                                                    <p class="head">Amount</p>
                                                                    <p class="value">$'.number_format($bidAmount,2).'</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <a href="/booster/boostFiles/postBid.php?id='.$row['oID'].'" class="btn btn-submit-bid">Claim Order</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ';
                                    }

                                    ?>
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>