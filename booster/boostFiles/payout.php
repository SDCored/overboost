<?php

	$getUserPayouts = "SELECT * FROM ob_payouts WHERE `uID` = '$globalUserUID' ORDER BY `pID` DESC";
    $userPayoutQuery = $con->query($getUserPayouts);

    if(isset($_POST['submitPayoutRequest'])) {
    	$getPayoutEmail = $con->real_escape_string($_POST['payoutEmail']);
    	$getPayoutAmount = $con->real_escape_string($_POST['payoutAmount']);

    	if($globalUserBoosterBalance == "0.00") {
    		$badPayout = "<span class='alert alert-danger' style='width: 100%; display: block; text-align: center; font-weight: bold;'>No balance to remove.</span>";
    	}else{
    		if($getPayoutAmount > $globalUserBoosterBalance) {
		    	$badPayout = "<span class='alert alert-danger' style='width: 100%; display: block; text-align: center; font-weight: bold;'>Requested payout is too large. Please use an amount lower than or equal to your current balance.</span>";
		    	}else{
		    		if($getPayoutAmount <= 0) {
		    			$badPayout = "<span class='alert alert-danger' style='width: 100%; display: block; text-align: center; font-weight: bold;'>Requested payout is too small. Please use an amount higher than 0.</span>";
		    		}else{
		    			$currentDate = date("m/d/Y");
		    			$balanceAfterPayout = $globalUserBoosterBalance - $getPayoutAmount;
		    			$paidBalanceAfter = $globalUserBoosterPaidBalance + $getPayoutAmount;
		    			mysqli_query($con, "INSERT INTO ob_payouts (uID, payoutEmail, payoutAmount, date, payoutStatus) VALUES ('$globalUserUID', '$getPayoutEmail', '$getPayoutAmount', '$currentDate', '0')") or die(mysqli_error($con));
		    			mysqli_query($con, "UPDATE ob_users SET `unpaidBalance` = '$balanceAfterPayout', `paidBalance` = '$paidBalanceAfter' WHERE `uID` = '$globalUserUID'") or die(mysqli_error($con));
		    			$goodPayout = "<span class='alert alert-success' style='width: 100%; display: block; text-align: center; font-weight: bold;'>Payment has been requested.</span>";
		    			header("Location: #");
		    		}
		    	}
		    }
    	}

?>

<div class="tab-pane" id="payouts-tab" role="tabpanel">
	<h1 class="tab-head">Payouts</h1>

	<?php if(isset($badPayout)) {echo $badPayout;} ?>
	<?php if(isset($goodPayout)) {echo $goodPayout;} ?>

	<div class="responsive-table">
		<table class="admin-table responsive-mate display" id="payouts-table">
			<thead>
				<tr class="head">
					<th>Date <i class="fa fa-sort"></i></th>
					<th>Payout Email <i class="fa fa-sort"></i></th>
					<th>Payout Amount <i class="fa fa-sort"></i></th>
					<th>Status <i class="fa fa-sort"></i></th>
				</tr>
			</thead>
			<tbody>
				<?php

					if(mysqli_num_rows($userPayoutQuery) > 0) {
						while($row = $userPayoutQuery->fetch_assoc()) {
							$getStatus = $row['payoutStatus'];

							if($getStatus == 0) {
								$payoutStatus = "<span class='other'>Requested</span>";
							}else{
								$payoutStatus = "<span class='success'>Paid</span>";
							}

							echo '
								<tr>
									<td><p class="perk-desc">'.$row['date'].'</p></td>
									<td><p class="perk-desc">'.$row['payoutEmail'].'</p></td>
									<td><p class="perk-desc">$'.$row['payoutAmount'].'</p></td>
									<td><p class="perk-desc">'.$payoutStatus.'</p></td>
								</tr>
							';
						}
					}else{
						$noPayouts = "Use the form below to request a payout!";
					}

					if(isset($noPayouts)) {
						echo '<th>'.$noPayouts.'</th>';
					}

				?>
			</tbody>
		</table>
	</div>

	<div class="rest-content-holder bg-white">
		<h4>Request a Payout</h4>

		<div class="form-group">
			<form action="" method="POST">
				<input type="email" class="form-control" placeholder="Payout Email" required name="payoutEmail" id="payoutEmail" />
				<input type="text" class="form-control" placeholder="Payout Amount" required name="payoutAmount" id="payoutAmount" />

				<button type="submit" class="btn btn-payout" name="submitPayoutRequest" id="submitPayoutRequest">Confirm</button>
			</form>
		</div>
	</div>
</div>