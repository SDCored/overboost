                        <div class="tab-pane fade show active no-padding" id="skill-rating" role="tabpanel">
                            <div class="responsive-table">
                                <table class="admin-table responsive-mate display" id="skill-rating-table">
                                    <thead>
                                        <tr class="head">
                                            <th>Date <i class="fa fa-sort"></i></th>
                                            <th>Region <i class="fa fa-sort"></i></th>
                                            <th>Platform<i class="fa fa-sort"></i></th>
                                            <th>Options<i class="fa fa-sort"></i></th>
                                            <th>Start<i class="fa fa-sort"></i></th>
                                            <th>Desired<i class="fa fa-sort"></i></th>
                                            <th>Amount<i class="fa fa-sort"></i></th>
                                            <th>Action <i class="fa fa-sort"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    while($row = $skillRatingsQuery->fetch_assoc()) {
                                        // Variable Stuff
                                        $userRegion = $row['userRegion'];
                                        if($userRegion == 0){
                                            $userRegion = 'None';
                                        }elseif($userRegion == 1) {
                                            $userRegion = 'US';
                                        }elseif($userRegion == 2) {
                                            $userRegion = 'EU';
                                        }elseif($userRegion == 3) {
                                            $userRegion = 'Asia';
                                        }else{
                                            $userRegion = 'None';
                                        }

                                        $userPlatform = $row['userPlatform'];
                                        if($userPlatform == 0){
                                            $userPlatform = '<i class="fa fa-times-circle"></i>';
                                        }elseif($userPlatform == 1) {
                                            $userPlatform = '<i class="fa fa-windows"></i>';
                                        }elseif($userPlatform == 2) {
                                            $userPlatform = '<img src="/static/img/demo/playstation-icon.png">';
                                        }elseif($userPlatform == 3) {
                                            $userPlatform = '<img src="/static/img/demo/xbox-icon.png">';
                                        }else{
                                            $userPlatform = '<i class="fa fa-times-circle"></i>';
                                        }

                                        $userStream = $row['streaming'];
                                        $userDuoQueue = $row['duoQueue'];
                                        if($userStream === "0" && $userDuoQueue === "0") {
                                            $userOptions = '<i class="fa fa-times-circle"></i>';
                                        }else{
                                            $userOptions = '';
                                            if($userStream == "1") {
                                                $userOptions = '<i class="fa fa-twitch"></i>';
                                            }else{}

                                            if($userDuoQueue == "1") {
                                                $userOptions = '<i class="fa fa-users"></i>';
                                            }else{}
                                        }

                                        $amountPaid = $row['amountPaid'];
                                        $currentBid = $row['bidAmount'];

                                        if($currentBid === "0.00") {
                                            $bidAmount = $amountPaid * 0.75;
                                            $bidAmountFinal = number_format($bidAmount);
                                        }else{
                                            $bidAmount = number_format($currentBid);
                                        }

                                        if($currentBid === "0.00") {
                                            $bidPercent = 05;
                                            $getBidAmount = $amountPaid * 0.75;
                                            $bidPercentSub = ($bidPercent / 100) * $getBidAmount;
                                            $minimumBidAmount = $getBidAmount - $bidPercentSub;
                                        }else{
                                            $bidPercent = 05;
                                            $bidPercentSub = ($bidPercent / 100) * $currentBid;
                                            $minimumBidAmount = $currentBid - $bidPercentSub;
                                        }

                                        $bidUsername = $row['bidUsername'];

                                        if($bidUsername == "none") {
                                            $currentBidUser = "None";
                                        }else{
                                            $currentBidUser = $bidUsername;
                                        }

                                        $currentRank = $row['currentRank'];
                                        $desiredRank = $row['desiredRank'];

                                        if($currentRank >= '0' & $currentRank <= '1499') {
                                            $cRankImage = "tier1.png";
                                        }elseif($currentRank >= '1500' & $currentRank <= '1999') {
                                            $cRankImage = "tier2.png";
                                        }elseif($currentRank >= '2000' & $currentRank <= '2499') {
                                            $cRankImage = "tier3.png";
                                        }elseif($currentRank >= '2500' & $currentRank <= '2999') {
                                            $cRankImage = "tier4.png";
                                        }elseif($currentRank >= '3000' & $currentRank <= '3499') {
                                            $cRankImage = "tier5.png";
                                        }elseif($currentRank >= '3500' & $currentRank <= '3999') {
                                            $cRankImage = "tier6.png";
                                        }elseif($currentRank >= '4000' & $currentRank <= '5000') {
                                            $cRankImage = "tier7.png";
                                        }elseif($currentRank > '5000') {
                                            $cRankImage = "tier7.png";
                                        }

                                        if($desiredRank >= '0' & $desiredRank <= '1499') {
                                            $dRankImage = "tier1.png";
                                        }elseif($desiredRank >= '1500' & $desiredRank <= '1999') {
                                            $dRankImage = "tier2.png";
                                        }elseif($desiredRank >= '2000' & $desiredRank <= '2499') {
                                            $dRankImage = "tier3.png";
                                        }elseif($desiredRank >= '2500' & $desiredRank <= '2999') {
                                            $dRankImage = "tier4.png";
                                        }elseif($desiredRank >= '3000' & $desiredRank <= '3499') {
                                            $dRankImage = "tier5.png";
                                        }elseif($desiredRank >= '3500' & $desiredRank <= '3999') {
                                            $dRankImage = "tier6.png";
                                        }elseif($desiredRank >= '4000' & $desiredRank <= '5000') {
                                            $dRankImage = "tier7.png";
                                        }elseif($desiredRank > '5000') {
                                            $cRankImage = "tier7.png";
                                        }

                                        $getIfCurrentOrder = mysqli_query($con, "SELECT * FROM ob_orders WHERE `boosterUID` = '$globalUserUID' AND `playComplete` = '0'");

                                        if(mysqli_num_rows($getIfCurrentOrder) > 0) {
                                            $bidDisabled = $row['biddingDisabled'];
                                            $bidButton = '<a href="#" title="You have an order you need to finish."  disabled="disabled" class="btn btn-table btn-claim disableClick" style="background: #AAA; pointer-events: none;">Claim</a>';
                                        }else{
                                            $bidDisabled = $row['biddingDisabled'];
                                            if($bidDisabled == 0) {
                                                $bidButton = '<a href="#skillRatingBidModal'.$row['oID'].'" data-toggle="modal" class="btn btn-table btn-claim">Claim</a>';
                                            }elseif($bidDisabled == 1) {
                                                $bidButton = '<a href="#" title="Customer has not set username and password."  disabled="disabled" class="btn btn-table btn-claim disableClick" style="background: #AAA; pointer-events: none;">Claim</a>';
                                            }
                                        }

                                        $autoWin = $amountPaid * 0.25;

                                        echo '
                                        <tr>
                                            <td><p class="perk-desc">'.$row['dateCreated'].'</p></td>
                                            <td><p class="perk-desc">'.$userRegion.'</p></td>
                                            <td><p class="perk-desc">'.$userPlatform.'</p></td>
                                            <td><p class="perk-desc">'.$userOptions.'</p></td>
                                            <td><div class="start"><img src="/static/img/rankImages/'.$cRankImage.'" alt="tiericon" class="tier-icon"><p>'.$currentRank.'</p></div></td>
                                            <td><div class="start"><img src="/static/img/rankImages/'.$dRankImage.'" alt="tiericon" class="tier-icon"><p>'.$desiredRank.'</p></div></td>
                                            <td><p class="perk-desc">$'.number_format($bidAmount, 2).'</p></td>
                                            <td>'.$bidButton.'</td>
                                        </tr>
                                        ';

                                        echo '
                                            <div class="modal fade adminModal" id="skillRatingBidModal'.$row['oID'].'">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="header-strip">
                                                            <div class="header-main">
                                                                <div class="tier start">
                                                                    <img src="/static/img/rankImages/'.$cRankImage.'" alt="tier">
                                                                    <p class="small">'.$currentRank.' LP</p>
                                                                    <p>Start</p>
                                                                </div>
                                                                <div class="tier fa">
                                                                    <i class="fa fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="tier end">
                                                                    <img src="/static/img/rankImages/'.$dRankImage.'" alt="tier">
                                                                    <p class="small">'.$desiredRank.' LP</p>
                                                                    <p>Desired</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="body-strip">
                                                            <div class="bid-info">
                                                                <div class="left-block">
                                                                    <p class="head">Amount</p>
                                                                    <p class="value">$'.number_format($bidAmount,2).'</p>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <a href="/booster/boostFiles/postBid.php?id='.$row['oID'].'" class="btn btn-submit-bid">Claim Order</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ';

                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>