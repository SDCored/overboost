<?php
    $page = "";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/boosternavbar.php');    
    require($sRoot.'/booster/boostFiles/sidebar.php');
?>

    <!-- FAQ -->
    <div class="tab-pane bg-white" id="faq-tab" role="tabpanel">
        <div class="container">
            <h1 class="Tab-head text-left color-primary">FAQ</h1>
            
            <div class="faq-box">
                <p class="head">How does your order claiming system work?</p>
                <p class="desc">We work using a bidding system. When a customer orders a boost, the order shows up on the dashboard and you have 1 hour to bid on the order. The lowest bidding booster will be assigned the order once the hour has expired. Alternatively, you can also auto-win the order by bidding the buyout price.</p>
            </div>

            <div class="faq-box">
                <p class="head">What is the buyout price and what does it do?</p>
                <p class="desc">The buyout price is a quick and easy way to secure an order for yourself. By bidding on the selected order and bidding the buyout price, you will instantly be assigned the order and will be able to get started on the job without any delay or waiting time.</p>
            </div>

            <div class="faq-box">
                <p class="head">What is the base pay for orders on BuyEloBoosting?</p>
                <p class="desc">The base pay we offer to boosters is 75%, which is considerably higher than any other boosting company out there. Boosters working for us end up making much more than other websites simply because of the amount of orders we have and the payout we offer.</p>
            </div>

            <div class="faq-box">
                <p class="head">Are there requirements in terms of progression/divisions?</p>
                <p class="desc">Yes, you MUST complete at least ONE DIVISION every 24 hours. This is a MINIMUM. Failure to complete this will result in a warning and a $25 fee, which will be deducted from your balance. (you can read about fees by clicking on “Rules & Fees” in the sidebar)
                </p>
            </div>

            <div class="faq-box">
                <p class="head">Can I claim an order and keep it for later?</p>
                <p class="desc">No, you MUST begin playing within an hour of picking up the order, failure to do this will result in a $20 fee and a warning. Doing this multiple times will result in account closure.</p>
            </div>

            <div class="faq-box">
                <p class="head">I tried claiming an order and it gave me an error message, why is this happening?</p>
                <p class="desc">Certain tiers may be restricted because your rank or winrate is not high enough. If you believe this is a mistake, please contact our administration team via skype (contact@buyeloboosting.com).</p>
            </div>

            <div class="faq-box">
                <p class="head">Something came up and I cannot complete the order I am doing, what do I do?</p>
                <p class="desc">Notify an administrator via skype (contact@buyeloboosting.com on skype). Doing this will result in a fee, which could be bigger if you’re regressed on the orders initial status.</p>
            </div>

            <div class="faq-box">
                <p class="head">The customer has selected preferred champions, roles and summoner spell placement, do I have to follow these?</p>
                <p class="desc">Yes, the customer paid for these options, therefore you must comply (when possible). Failure to do this will result in a $50 fee, which will be deducted from your balance.</p>
            </div>

            <div class="faq-box">
                <p class="head">How do I request a payout?</p>
                <p class="desc">To request a payout, go to the “Payouts” page on the sidebar and input the amount you would like to request for your payout. The minimum payout is $100 and you may only request one payout per week.</p>
            </div>

            <div class="faq-box">
                <p class="head">When are payouts sent?</p>
                <p class="desc">Payouts are sent out within 1 week of the Payout request.</p>
            </div>

            <div class="faq-box">
                <p class="head">Where are payouts sent to?</p>
                <p class="desc">Payouts are sent to the payment email you provided us when we created your booster account.</p>
            </div>
        </div>
    </div>

<?php require($sRoot.'/booster/boostFiles/footer.php'); ?>