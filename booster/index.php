<?php
    $page = "index";
    $sRoot = $_SERVER['DOCUMENT_ROOT'];
    require($sRoot.'/include/boosternavbar.php');
    
    $getSkillRatings = "SELECT * FROM ob_orders WHERE orderType = 0 AND boosterUID = 0 ORDER BY `oid` DESC"; // Skill Rating Query
    $skillRatingsQuery = $con->query($getSkillRatings);

    $getSoloDuo = "SELECT * FROM ob_orders WHERE orderType = 1 AND boosterUID = 0 ORDER BY `oid` DESC"; // Solo / Duo Query
    $soloDuoQuery = $con->query($getSoloDuo);

    $getPlacement = "SELECT * FROM ob_orders WHERE orderType = 2 AND boosterUID = 0 ORDER BY `oid` DESC"; // Placement Query
    $placementQuery = $con->query($getPlacement);
    
    require($sRoot.'/booster/boostFiles/sidebar.php');
?>

    <!-- Dashboard -->
    <div class="tab-pane" id="dashboard-tab">
        <!-- Dashboard Table Tabs -->
        <ul class="nav nav-tabs full-width-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#skill-rating" role="tab">Skill Rating</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#solo-duo" role="tab">Solo/Duo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#placement-matches" role="tab">Placement</a>
            </li>
        </ul>
        
        <!-- Dashboard Tables -->
        <div class="tab-content">
            <?php
                require($sRoot.'/booster/boostFiles/SkillRating.php');
                require($sRoot.'/booster/boostFiles/SoloDuo.php');
                require($sRoot.'/booster/boostFiles/Placement.php');
            ?>
        </div>
        
        <!-- Chat -->
        <div class="row admin-chat-row">
            <div class="col-md-6 left-col">
                <div class="admin-chat">
                    <div class="live-chat-holder">
                        <div class="chat-messages">
                            <div class="message">
                                <p class="name">Customer</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message inverse">
                                <p class="name">Booster</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message">
                                <p class="name">Customer</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message inverse">
                                <p class="name">Booster</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message">
                                <p class="name">Customer</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message inverse">
                                <p class="name">Booster</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message inverse">
                                <p class="name">Booster</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                            <div class="message inverse">
                                <p class="name">Booster</p>
                                <p class="date">22/22/2212 22:04:33</p>
                                <div class="box">
                                    <p class="msg">Hello!</p>
                                </div>
                            </div>
                        </div>
                        <div class="chat-reply">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your message">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-send"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Support -->
            <div class="col-md-6 right-col">
                <div class="admin-chat-aside">
                    <div class="support text-center">
                        <p class="head">Booster Support</p>
                        <p class="tag">Facing an issue? Let us know.</p>
                        <a href="skype:boosters@buyoverwatchboost.com?add"><i class="fa fa-skype"></i></a>
                        <a href="skype:boosters@buyoverwatchboost.com?add"><i class="fa fa-user"></i></a>
                        <p class="smalltag">Administrator</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require($sRoot.'/booster/boostFiles/footer.php'); ?>