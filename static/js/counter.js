



(function (e) {
  /* e.fn.countdown = function (t, n) {
  function i() {
    eventDate = Date.parse(r.date) / 1e3;
    currentDate = Math.floor(e.now() / 1e3);
    if (eventDate <= currentDate) {
      n.call(this);
      clearInterval(interval)
    }
    seconds = eventDate - currentDate;
    days = Math.floor(seconds / 86400);
    seconds -= days * 60 * 60 * 24;
    hours = Math.floor(seconds / 3600);
    seconds -= hours * 60 * 60;
    minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;

    var daySlug = $('.fyre-counter .day-slug');
    var hourSlug = $('.fyre-counter .hour-slug');
    var minuteSlug = $('.fyre-counter .minute-slug');
    var secondSlug = $('.fyre-counter .second-slug');

    days == 1 ? daySlug.text("day") : daySlug.text("days");
    hours == 1 ? hourSlug.text("hour") : hourSlug.text("hours");
    minutes == 1 ? minuteSlug.text("minute") : minuteSlug.text("minutes");
    seconds == 1 ? secondSlug.text("second") : secondSlug.text("seconds");
    if (r["format"] == "on") {
      days = String(days).length >= 2 ? days : "0" + days;
      hours = String(hours).length >= 2 ? hours : "0" + hours;
      minutes = String(minutes).length >= 2 ? minutes : "0" + minutes;
      seconds = String(seconds).length >= 2 ? seconds : "0" + seconds
    }
    if (!isNaN(eventDate)) {
      thisEl.find(".fyre-days .num").text(days);
      thisEl.find(".fyre-hours .num").text(hours);
      thisEl.find(".fyre-minutes .num").text(minutes);
      thisEl.find(".fyre-seconds .num").text(seconds)
    } else {
      alert("Invalid date. Example: 30 Tuesday 2013 15:50:00");
      clearInterval(interval);
    }
  } */
  var thisEl = e(this);
  var r = {
    date: null,
    format: null
  };
  t && e.extend(r, t);
  i();
  interval = setInterval(i, 1e3)

  })(jQuery);

$(document).ready(function() {

	 function e() {
    var e = new Date;
    e.setDate(e.getDate() + 60);
    dd = e.getDate();
    mm = e.getMonth() + 1;
    y = e.getFullYear();
    futureFormattedDate = mm + "/" + dd + "/" + y;
    return futureFormattedDate
  }


  var heroCounterNew = $('.hero-counter-new');
  var givenDateNew = heroCounterNew.data('end-date');

  heroCounterNew.countdown({
    date: givenDateNew,
    format: "on"
  });



});