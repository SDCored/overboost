/* =========================

App: OverwatchBoost
Version: 1.0
Author: Hardik Manktala
Contact:
    - http://www.hardikmanktala.com
    - chat@hardikmanktala.com
    - Skype: hardik.manktala

========================= */

/* ======================

root.js - Main Script

======================= */

function getPricing(val, type) {
    var toReturn = 0;
    
    val = parseInt(val, 10);
    
    
    sliderPrices.forEach(function(rangeData, i) {
        
        if ((val >= rangeData.minRangeValue) && (val <= rangeData.maxRangeValue)) {
            // console.log('val within limits!');
            
            if (type == 'base') {
                toReturn = rangeData.basePrice;
            }
            else {
                toReturn = rangeData.rangePrice;
            }
            
            return false;
        }
    });
    
    // console.log('type: ' + type);
    
    // console.log('toReturn: ' + toReturn);
    return toReturn;
    
    
}


var sliderPrices = [
    {
        "minRangeValue": 0,
        "maxRangeValue": 249,
        "basePrice": 1000,
        "rangePrice": 10000
    },
    {
        "minRangeValue": 250,
        "maxRangeValue": 499,
        "basePrice": 1500,
        "rangePrice": 15000
    },
    {
        "minRangeValue": 500,
        "maxRangeValue": 749,
        "basePrice": 2000,
        "rangePrice": 20000
    },
    {
        "minRangeValue": 750,
        "maxRangeValue": 999,
        "basePrice": 2500,
        "rangePrice": 25000
    },
    {
        "minRangeValue": 1000,
        "maxRangeValue": 1249,
        "basePrice": 3000,
        "rangePrice": 30000
    },
    {
        "minRangeValue": 1250,
        "maxRangeValue": 1499,
        "basePrice": 3500,
        "rangePrice": 35000
    },
    {
        "minRangeValue": 1500,
        "maxRangeValue": 1749,
        "basePrice": 4000,
        "rangePrice": 40000
    },
    {
        "minRangeValue": 1750,
        "maxRangeValue": 1999,
        "basePrice": 4500,
        "rangePrice": 45000
    },
    {
        "minRangeValue": 2000,
        "maxRangeValue": 2249,
        "basePrice": 5000,
        "rangePrice": 50000
    },
    {
        "minRangeValue": 2250,
        "maxRangeValue": 2499,
        "basePrice": 5500,
        "rangePrice": 55000
    },
    {
        "minRangeValue": 2500,
        "maxRangeValue": 2749,
        "basePrice": 6000,
        "rangePrice": 60000
    },
    {
        "minRangeValue": 2750,
        "maxRangeValue": 2999,
        "basePrice": 6500,
        "rangePrice": 65000
    },
    {
        "minRangeValue": 3000,
        "maxRangeValue": 3249,
        "basePrice": 7000,
        "rangePrice": 70000
    },
    {
        "minRangeValue": 3250,
        "maxRangeValue": 3499,
        "basePrice": 7500,
        "rangePrice": 75000
    },
    {
        "minRangeValue": 3500,
        "maxRangeValue": 3749,
        "basePrice": 8000,
        "rangePrice": 80000
    },
    {
        "minRangeValue": 3750,
        "maxRangeValue": 3999,
        "basePrice": 8500,
        "rangePrice": 85000
    },
    {
        "minRangeValue": 4000,
        "maxRangeValue": 4249,
        "basePrice": 9000,
        "rangePrice": 90000
    },
    {
        "minRangeValue": 4250,
        "maxRangeValue": 4499,
        "basePrice": 9500,
        "rangePrice": 95000
    }
];

var pageSlug = $('body').attr('data-pageSlug');
$(document).ready(function() {

    navbarScroll();
    particles();

    $("[data-toggle='tooltip']").tooltip();

    /*============================================
    ScrollTo Links
    ==============================================*/
    $('a.scrollto').on('click', function(e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing');
    });


    if(pageSlug === "pricing") {
             pricingBtnGroup();
            pricingRangeText();
            pricingBootstrapSlider();
    }

    if(pageSlug === "demo" || pageSlug === "order") {
        orderProgressSlider();

        var ops = $('#options-platform-selector');

        var platform = ops.find('.platform');

        platform.each(function() {
            var p = $(this);
            var ps = p.siblings('.platform');
            p.on('click', function() {
                if(!p.hasClass('active')) {
                    p.toggleClass('active');
                    ps.removeClass('active');
                }
            });
        });

        /*============heros selection=========*/

        var herosSelect = $('#options-hero-selector');
        var hero = herosSelect.find('.hero');
        var count = herosSelect.find('.hero.active').length;
        var heroBtn = $('#heroSubmitBtn');

        /*checkHeroCount();*/

        hero.each(function() {
            var h = $(this);

            h.on('click', function() {
                h.toggleClass('active');

                if(h.hasClass('active')) {
                    count = count + 1;
                } else {
                    count = count - 1;
                }

    /*          checkHeroCount();*/

            });

        });

        /*function checkHeroCount() {
            if(count < 6) {
                heroBtn.addClass('btn-deactivated');
                heroBtn.html("Select atleast 6 heros");
                heroBtn.attr('disabled', 'disabled');
            } else if(count >= 6) {
                heroBtn.removeClass('btn-deactivated');
                heroBtn.html('Submit');
                heroBtn.removeAttr('disabled');
            }
        }*/

    }
   
    /*===============*/
    function orderProgressSlider() {
        var orderProgress = $('.order-progress-slider');

        var orderProgressParent = orderProgress.parent();

        var givenVal = parseInt(orderProgress.attr('data-value'), 10);

        orderProgress.bootstrapSlider({
            tooltip: 'always',
            min: 1,
            max: 100,
            step: 1,
            value: givenVal
        });     

        var tooltipIn = orderProgressParent.find('.tooltip-inner');

        tooltipIn.html(tooltipIn.html() + '%');

        orderProgress.bootstrapSlider('disable');

    }
    /*================*/

    /*==============*/
    function pricingBtnGroup() {

        var gp = $('.pricing-btn-group');

        gp.each(function(index) {
            var g = $(this);
            
            btns = g.find('.btn');

            btns.each(function() {
                var b = $(this);
                var sb = b.siblings('.btn');

                b.on('click', function() {
                        b.toggleClass('active');
                        sb.removeClass('active');
                    
                });

            });
        });

        
    }

    function pricingRangeText() {

        var skillRatingSlider = $('.pricing-slider.skill-rating');
        var soloDuoGamingSlider = $('.pricing-slider.solo-duo-gaming');
        var placementGamesSlider = $('.pricing-slider.placement-games');

        var csr1 = $('#csr_1_text');
        var csr2 = $('#csr_2_text');

        var range = $('#pricingRangeText1');
        var leftb = range.find('.btn-left');
        var rightb = range.find('.btn-right');
        var text = range.find('input');
        var textVal = parseInt(text.val(), 10);
        var textFromRating = $('#startingRating');

        csr1.on('change input propertychange', function() {
            if(parseInt(csr1.val(), 10) >= 0) {
                text.val(parseInt(csr1.val(), 10) + 50);
                textVal = parseInt(text.val(), 10);
                skillRatingSlider.bootstrapSlider('setValue', textVal);
                textFromRating.text(textVal - 50);
            }
        });
        

        leftb.on('click', function() {
            if(textVal > parseInt(csr1.val(), 10) + 50) {
                textVal = parseInt(text.val(),10) - 50;
                text.val(textVal);
                skillRatingSlider.bootstrapSlider('setValue', skillRatingSlider.bootstrapSlider('getValue') - 50);
            }
        });

        rightb.on('click', function() {
            if(textVal < 4400 && skillRatingSlider.bootstrapSlider('getValue') < 4400 && parseInt(csr1.val(), 10) < 4350) {
                textVal = parseInt(text.val(),10) + 50;
                text.val(textVal);
                skillRatingSlider.bootstrapSlider('setValue', skillRatingSlider.bootstrapSlider('getValue') + 50);
            }
            
        });

        var rangea = $('#pricingRangeText2');
        var leftba = rangea.find('.btn-left');
        var rightba = rangea.find('.btn-right');
        var texta = rangea.find('input');
        var textVala = parseInt(texta.val(), 10);
        var sdTextFromRating = $('#sdCurrentRating');

        var hiddenDesiredRank = $('desiredHiddenRank');
        var hiddenSR = $('#currentSR');

        csr2.on('change input propertychange', function() {
            if(parseInt(csr2.val(), 10) >= 1) {
                texta.val(parseInt(csr2.val(), 10) + 1);
                hiddenDesiredRank.val(parseInt(csr2.val(), 10) + 1);
                textVala = parseInt(texta.val(), 10);
                soloDuoGamingSlider.bootstrapSlider('setValue', textVala);
                sdTextFromRating.text(textVala - 1);
                hiddenSR.val(textVala - 1); // Setting hidden SR input
            }
            
        });

        leftba.on('click', function() {
            if(textVala > parseInt(csr2.val(), 10) + 1) {
                textVala = parseInt(texta.val(), 10) - 1;
                texta.val(textVala);
                soloDuoGamingSlider.bootstrapSlider('setValue', soloDuoGamingSlider.bootstrapSlider('getValue') - 1);
            }
        });

        rightba.on('click', function() {
            if(textVala < 20 && soloDuoGamingSlider.bootstrapSlider('getValue') < 20 && parseInt(csr1.val(), 10) < 20) {
                textVala = parseInt(texta.val(), 10) + 1;
                texta.val(textVala);
                soloDuoGamingSlider.bootstrapSlider('setValue', soloDuoGamingSlider.bootstrapSlider('getValue') + 1);
            }
        });

        var rangeb = $('#pricingRangeText3');
        var leftbb = rangeb.find('.btn-left');
        var rightbb = rangeb.find('.btn-right');
        var textb = rangeb.find('input');
        var textValb = parseInt(textb.val(), 10);

        leftbb.on('click', function() {
            if(textValb > 1) {
                textValb = parseInt(textb.val(), 10) - 1;
                textb.val(textValb);
                placementGamesSlider.bootstrapSlider('setValue', placementGamesSlider.bootstrapSlider('getValue') - 1);
            }
        });

        rightbb.on('click', function() {
            if(textValb < 10) {
                textValb = parseInt(textb.val(), 10) + 1;
                textb.val(textValb);
                placementGamesSlider.bootstrapSlider('setValue', placementGamesSlider.bootstrapSlider('getValue') + 1);
            }
            
        });

    }

    function pricingBootstrapSlider() {
        var skillRating = $('.pricing-slider.skill-rating');


        skillRating.bootstrapSlider({
            tooltip: 'always',
            min: 100,
            max: 4400,
            step: 50,
            value: 100
        });

        skillRating.on('change', function() {
            var range = $('#pricingRangeText1');
            var text = range.find('input');

            var rangePrice = $('#inputUpdate');
            var textPrice = rangePrice.find('input');

            var textDiscount = $('#priceDiscount');
            var textOriginal = $('#priceOrignal');
            var textToRating = $('#toRating');
            var textFromRating = $('#startingRating');

            var valueString = skillRating.bootstrapSlider('getValue');
            var valueToString = valueString.toString();

            var rangePrice = getPricing(valueString, 'range');
            var basePrice = getPricing(textFromRating.text(), 'base');
            
            // console.log('rangePrice: ' + rangePrice);
            // console.log('basePrice: ' + basePrice);
            var calculatedPrice = (rangePrice + basePrice) / 100;
            // console.log('valueFinalPrice: ' + valueFinalPrice);
            
            //var valueFinalPrice = getPrice(valueString);
            var valueFinalPrice = (+valueToString * ((100 + 15) / 100));

            var hiddenPrice = $('#pricingHiddenInput');

            text.val(skillRating.bootstrapSlider('getValue'));
            textPrice.val((skillRating.bootstrapSlider('getValue')));
            //textDiscount.text('$' + valueToString);
            textDiscount.text('$' + calculatedPrice.toFixed(2));
            textOriginal.text((calculatedPrice.toFixed(2) * 1.1).toFixed(2));
            textToRating.text(valueToString);
            hiddenPrice.val(calculatedPrice.toFixed(2));
        });
        
        skillRating.trigger('change'); // force update the values on page load

        var soloDuoGaming = $('.pricing-slider.solo-duo-gaming');


        soloDuoGaming.bootstrapSlider({
            tooltip: 'always',
            min: 1,
            max: 20,
            step: 1,
            value: 1
        });

        soloDuoGaming.on('change', function() {
            var range = $('#pricingRangeText2');
            var text = range.find('input');
            text.val(soloDuoGaming.bootstrapSlider('getValue'));
            var gameAmount = $('#gameAmounts');
            var currentPrice = $('#cPrice');
            var undPrice = $('#notDiscounted');
            var orderAmount = $('#orderAmount');

            var calculatedPrice = (soloDuoGaming.bootstrapSlider('getValue')) * 3.75;

            var sdStartingRating = $('#sdCurrentRating'); // Current rating
            var sdAmountGames = $('#sdGamesAmount'); //Number of Games

            sdAmountGames.text(soloDuoGaming.bootstrapSlider('getValue')); // Setting amount of games
            gameAmount.val(soloDuoGaming.bootstrapSlider('getValue')); // Setting hidden input of game amounts
            currentPrice.text(calculatedPrice.toFixed(2));
            undPrice.text((calculatedPrice * 1.25).toFixed(2));
            orderAmount.val(calculatedPrice.toFixed(2));


        });

        var placementGames = $('.pricing-slider.placement-games');


        placementGames.bootstrapSlider({
            tooltip: 'always',
            min: 1,
            max: 10,
            step: 1,
            value: 1
        });

        placementGames.on('change', function() {
            var range = $('#pricingRangeText3');
            var text = range.find('input');
            var amountOfGames = $('#numberOfGames');
            var hiddenGames = $('#hiddenGameAmount');
            var hiddenAmount = $('#hiddenAmount');
            var oPrice = $('#originalPrice');
            var dPrice = $('#discountPrice');
            var oPriceText = ((placementGames.bootstrapSlider('getValue')) * 3.25);
            text.val(placementGames.bootstrapSlider('getValue'));
            var discountPrice = oPriceText * 1.25;

            amountOfGames.text(placementGames.bootstrapSlider('getValue'));
            hiddenGames.val(placementGames.bootstrapSlider('getValue'));
            oPrice.text(oPriceText.toFixed(2));
            dPrice.text(discountPrice.toFixed(2));
            hiddenAmount.val(oPriceText.toFixed(2));
        });



    }
    /*==============*/
    

    handleCollapse();
    handleModals();
    alignModal();
    swapNavbarCurrency();
    swapNavbarLanguage();
    if(pageSlug === "home") {
        reviewsSlider();
        gameHolder();
        featuresSlider();
        orderProgressSlider();
    }

    if(pageSlug === "pricing") {
        reviewsSlider();
    }



    

});

function featuresSlider() {
    var featuresSlider = new Swiper('.features-container', {
        speed: 400,
        spaceBetween: 0,
        slidesPerView: 1,
        autoplay: true,
        loop: true,
        pagination: '.features-pagination',
        paginationClickable: true,
        effect: 'coverflow',
        parallax: true,
        coverflow: {
          rotate: 50,
          stretch: 100,
          depth: 100,
          modifier: 1,
          slideShadows : false
        },
        autoplayDisableOnInteraction: true,
        breakpoints: {
            500: {
                effect: 'slide',
                spaceBetween: 100
            }
        }
    });
}



function gameHolder() {
    var leftHolder = $('#leftGameHolder');
    var rightHolder = $('#rightGameHolder');
    var interval = 8000;

    initialAnim();



    function initialAnim() {
        leftHolder.switchClass('top', 'bottom', interval, 'easeInOutQuad');
        rightHolder.switchClass('bottom', 'top', interval, 'easeInOutQuad');
        setTimeout(finalAnim, interval);
    }

    function finalAnim() {
        leftHolder.switchClass('bottom', 'top', interval, 'easeInOutQuad');
        rightHolder.switchClass('top', 'bottom', interval, 'easeInOutQuad');
        setTimeout(initialAnim, interval);
    }

}

function reviewsSlider() {
    var reviewsSlide = new Swiper('.reviews-container', {
        speed: 400,
        slidesPerView: 1,
        direction: 'vertical',
        autoplay: true,
        spaceBetween: 100,
        effect: 'slide'
    });

    var reviewsSlide = new Swiper('.reviews-container.reviews-2', {
        speed: 400,
        slidesPerView: 1,
        direction: 'vertical',
        autoplay: true,
        spaceBetween: 100,
        effect: 'slide'
    });
}

function alignModal() {
    // Align Modal

    function alignModalIn(){
        var modalDialog = $(this).find(".modal-dialog");
        
        // Applying the top margin on modal dialog to align it vertically center
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }

    alignModalIn();

    // Align modal when it is displayed
    $(".modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $(".modal:visible").each(alignModal);
    });  
}



function handleModals() {
    // Handling Modals
     var closeRegisterModal = $('#openLoginModal');
     var closeLoginModal = $('#openRegisterModal');

     closeRegisterModal.on('click', function() {
        $('#registerModal').modal('hide');
     });

     closeLoginModal.on('click', function() {
        $('#loginModal').modal('hide');
     });

}

function handleCollapse() {
    // Handling Collapse
        var buyCollapse = $('#buyLolAccountCollapse');
        $(document).on('click', function (event) {
            var clickover = $(event.target);
            
            var _opened = buyCollapse.hasClass("show");
            if (_opened === true) {
                buyCollapse.removeClass("show");

            }
        });

         $(window).on('scroll', function() {
            if($(window).scrollTop() !== 0) {
                buyCollapse.removeClass("show");
            }
        });
}

 function navbarScroll() {



} 





function swapNavbarCurrency() {

    var defaultC = $('#defaultCurrency');
    var otherC = $('#otherCurrency');

    

    otherC.on('click', function() {
        var otherText = otherC.html();
        var defaultText = defaultC.html();
        otherC.html(defaultText);
        defaultC.html(otherText);
    });

}

function swapNavbarLanguage() {

    var defaultL = $('#defaultLanguage');
    var langList = $('.language-list');

    langList.each(function(index) {
        var l = $(this);

        l.on('click', function() {
            var dt = defaultL.html();
            var newt = l.html();

            l.html(dt);
            defaultL.html(newt);

        });

    });

}

function particles() {

    if(pageSlug==="home") {
        // ParticlesJS
        particlesJS.load('cover-particles', 'static/js/libs/particlesjs-config.json');

        // Order Tutorial Particles
        particlesJS.load('faq-particles', 'static/js/libs/particlesjs-testi-config.json');

    }
    
}

function particles() {

    if(pageSlug==="home") {
        // ParticlesJS
        particlesJS.load('cover-particles', 'static/js/libs/particlesjs-config.json');

        // Order Tutorial Particles
        particlesJS.load('faq-particles', 'static/js/libs/particlesjs-testi-config.json');

    }
    
}