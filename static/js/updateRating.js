define('boostards', ['jquery', 'bootstrap', 'jquery.validation', 'jquery.mask', 'jquery.easypiechart', 'ion.rangeSlider', 'owl.carousel', 'spin', 'ladda', 'reconnecting-websocket'], function($) {

    // Countdown

    var countdown = $('#season-end-countdown');
    if (countdown.length) {
        var seconds = parseInt(countdown.attr('data-seconds'));
        var clock = $('#season-end-countdown').FlipClock(seconds, {
            clockFace: 'DailyCounter',
            countdown: true
        });
    }

    // Header

    var navbarScroll = function() {
        if ($(this).scrollTop() > 0) {
            $('body').removeClass('header-on-top');
        } else {
            $('body').addClass('header-on-top');
        }
    };
    $(window).scroll(navbarScroll);
    navbarScroll();

    // Modal review

    $('.show-modal-review').click(function(event) {
        event.preventDefault();
        $('#modal-review .modal-content').empty().load($(this).attr('href'));
        $('#modal-review').modal('show');
    });

    // Authentication

    var showModalLogin = function(event) {
        event.preventDefault();
        $('#modal-registration').modal('hide');
        $('#modal-reset').modal('hide');
        $('#modal-login .modal-content').empty().load($(this).attr('href'), modalUpdate);
        $('#modal-login').modal('show');
    };

    var showModalRegistration = function(event) {
        event.preventDefault();
        $('#modal-login').modal('hide');
        $('#modal-reset').modal('hide');
        $('#modal-registration .modal-content').empty().load($(this).attr('href'), modalUpdate);
        $('#modal-registration').modal('show');
    };

    var showModalReset = function(event) {
        event.preventDefault();
        $('#modal-registration').modal('hide');
        $('#modal-login').modal('hide');
        $('#modal-reset .modal-content').empty().load($(this).attr('href'), modalUpdate);
        $('#modal-reset').modal('show');
    };

    var modalUpdate = function() {

        var forms = $('#modal-login form, #modal-registration form, #modal-reset form, #order-registration form');
        if (forms.length > 0) {
            forms.validate({
                submit: {
                    settings: {
                        inputContainer: '.form-group',
                        errorClass: 'has-error',
                        errorListClass: 'form-tooltip-error'
                    },
                    callback: {
                        onSubmit: function(node, formData) {
                            if (formData.subscribed == 1) {
                                formData.subscribed = true
                            } else {
                                delete formData.subscribed
                            }
                            $.post($(node).attr('action'), formData, function(data) {
                                if (data == 'Ok') {
                                    location.reload();
                                } else if (data.indexOf('http') == 0) {
                                    location.href = data;
                                } else {
                                    $(node).parent().parent().html(data);
                                    modalUpdate();
                                }
                            }.bind(node));
                            return false;
                        }
                    }
                }
            });
        }

        $('.show-modal-registration').unbind("click");
        $('.show-modal-registration').click(showModalRegistration);
        $('.show-modal-login').unbind("click");
        $('.show-modal-login').click(showModalLogin);
        $('.show-modal-reset').unbind("click");
        $('.show-modal-reset').click(showModalReset);

    };

    modalUpdate();

    // Charts

    if ($('.chart').length) {
        $('.chart').easyPieChart({
            easing: 'easeOutBounce',
            barColor: '#5eb404',
            trackColor: '#e3e3e3',
            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
    }

    // Purchase Skill Rating

    var currentSkillRating = -1;
    var desiredSkillRating = -1;

    var showDesiredSkillRatingTooltip = true;
    var desiredSkillRatingSlider;

    var skillRatingPrices;
    var skillRatingMax;
    var streamPercent;
    var gamesScale;

    var discountPercent;

    var amount;
    var amountWithDiscount;

    var duoSkillRating;
    var streamSkillRating;

    // Detailed Skill Rating Prices

    var skillRatingPricesDetailed;
    function generateSkillRatingPricesDetailed() {
        skillRatingPricesDetailed = new Array(skillRatingPrices.length * 50);
        for (var i = 0; i < skillRatingPrices.length; i++) {
            var priceAdded = 0.0;
            var priceTotal = skillRatingPrices[i];
            for (var j = 0; j < 50; j++) {
                var price = Math.floor(priceTotal * 100 / 50 * (j + 1) - priceAdded);
                skillRatingPricesDetailed[i * 50 + j] = price / 100;
                priceAdded += price
            }
        }
    }

    // Discounts

    function updateDiscount() {
        discountPercent = parseInt($('.order-discount').attr('data-discount-percent'));
    }

    $('.order-create').on('click', '.order-discount-add', function(event) {
        event.preventDefault();
        $(this).prop('disabled', true);
        var form = $(this).parents('form');
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function(data) {
            $('.order-discount-container').html(data);
            updateDiscount();
            updateSkillRatingOrderSummaryAndPrice();
            updateGames();
            updatePlacement();
            updateTop500();
            $('.order-discount').find('label').tooltip();
        });
    });

    $('.order-create').on('click', '.order-discount-remove', function(event) {
        event.preventDefault();
        $.ajax({
            method: 'POST',
            url: $(this).attr('href')
        }).done(function(data) {
            $('.order-discount-container').html(data);
            updateDiscount();
            updateSkillRatingOrderSummaryAndPrice();
            updateGames();
            updatePlacement();
            updateTop500();
            $('.order-discount').find('label').tooltip();
        });
    });

    $('.order-create').on('input', '.discount-code', function() {
        $(this).val($(this).val().toUpperCase())
    });

    function applyDiscount() {
        amountWithDiscount = amount;
        if (discountPercent > 0) {
            amountWithDiscount = amount * (100 - discountPercent) / 100;
        }
    }

    function updateSkillRatingIcon(elem, skillRating) {
        var rank = 0;
        if (skillRating >= 0) {
            if (skillRating < 1500) {
                rank = 1;
            } else if (skillRating < 2000) {
                rank = 2;
            } else if (skillRating < 2500) {
                rank = 3;
            } else if (skillRating < 3000) {
                rank = 4;
            } else if (skillRating < 3500) {
                rank = 5;
            } else if (skillRating < 4000) {
                rank = 6;
            } else if (skillRating <= 5000) {
                rank = 7;
            }
        }
        for (var i = 1; i <= 7; i++) {
            if (i == rank) {
                elem.addClass('rank-' + i);
            } else {
                elem.removeClass('rank-' + i);
            }
        }
    }

    function updateSkillRatingTooltips() {
        var currentSkillRatingInput = $('#current-skill-rating');
        var desiredSkillRatingInput = $('#desired-skill-rating');
        if (currentSkillRating < 0) {
            currentSkillRatingInput.focus();
            currentSkillRatingInput.tooltip('show');
            showDesiredSkillRatingTooltip = true;
            desiredSkillRatingInput.tooltip('hide');
        } else {
            currentSkillRatingInput.tooltip('hide');
            if (showDesiredSkillRatingTooltip && (desiredSkillRating >= 0)) {
                desiredSkillRatingInput.tooltip('show');
            } else {
                desiredSkillRatingInput.tooltip('hide');
            }
        }
    }

    function currentSkillRatingChanged() {
        var currentSkillRatingInput = $('#current-skill-rating');
        var desiredSkillRatingInput = $('#desired-skill-rating');
        if (currentSkillRating >= skillRatingMax - 50) {
            currentSkillRating = skillRatingMax - 50;
            currentSkillRatingInput.val(currentSkillRating);
        }
        updateSkillRatingIcon(currentSkillRatingInput.parent(), currentSkillRating);
        if (currentSkillRating < 0) {
            desiredSkillRating = -1;
            desiredSkillRatingInput.val('');
        } else if (desiredSkillRating <= currentSkillRating) {
            if (currentSkillRating < 1400) {
                desiredSkillRating = 1500;
            } else if (currentSkillRating < 1900) {
                desiredSkillRating = 2000;
            } else if (currentSkillRating < 2400) {
                desiredSkillRating = 2500;
            } else if (currentSkillRating < 2900) {
                desiredSkillRating = 3000;
            } else if (currentSkillRating < 3400) {
                desiredSkillRating = 3500;
            } else if (currentSkillRating <= 3900) {
                desiredSkillRating = 4000;
            } else {
                desiredSkillRating = skillRatingMax;
            }
            desiredSkillRatingInput.val(desiredSkillRating);
        }
        var desiredSkillRatingMinus = $('#desired-skill-rating-minus');
        var desiredSkillRatingPlus = $('#desired-skill-rating-plus');
        if (desiredSkillRating < 0) {
            desiredSkillRatingMinus.prop('disabled', true);
            desiredSkillRatingInput.prop('disabled', true);
            desiredSkillRatingPlus.prop('disabled', true);
            desiredSkillRatingSlider.update({
                disable: true
            });
        } else {
            desiredSkillRatingMinus.prop('disabled', false);
            desiredSkillRatingInput.prop('disabled', false);
            desiredSkillRatingPlus.prop('disabled', false);
            var min = (Math.ceil(currentSkillRating / 50) + 1) * 50;
            desiredSkillRatingSlider.update({
                min: min,
                from: desiredSkillRating,
                disable: false
            });
            if (desiredSkillRating < min) {
                desiredSkillRating = min;
                $('#desired-skill-rating').val(desiredSkillRating);
            }
            currentSkillRatingInput.blur();
        }
        updateSkillRatingIcon(desiredSkillRatingInput.parent(), desiredSkillRating);
        updateSkillRatingTooltips();
        updateSkillRatingOrderSummaryAndPrice();
    }

    function desiredSkillRatingChanged(updateSlider) {
        updateSlider = typeof updateSlider !== 'undefined' ? updateSlider : true;
        var desiredSkillRatingInput = $('#desired-skill-rating');
        desiredSkillRating = parseInt(desiredSkillRatingInput.val());
        duoSkillRating = $('#duo-skill-rating').prop('checked');
        updateSkillRatingMax();
        if (isNaN(desiredSkillRating) || (desiredSkillRating <= (currentSkillRating + 50))) {
            desiredSkillRating = (Math.ceil(currentSkillRating / 50) + 1) * 50;
        }
        if (desiredSkillRating % 50 > 0) {
            desiredSkillRating = (Math.ceil(desiredSkillRating / 50) + 1) * 50;
        }
        if (desiredSkillRating > skillRatingMax) {
            desiredSkillRating = skillRatingMax;
            currentSkillRatingChanged();
        }
        if (desiredSkillRating > 0) {
            desiredSkillRatingInput.val(desiredSkillRating);
        } else {
            desiredSkillRatingInput.val('');
        }
        updateSkillRatingIcon(desiredSkillRatingInput.parent(), desiredSkillRating);
        if (updateSlider) {
            desiredSkillRatingSlider.update({
                from: desiredSkillRating,
                max: skillRatingMax
            });
        }
        updateSkillRatingOrderSummaryAndPrice();
        updateSkillRatingTooltips();
    }

    function updateSkillRatingMax() {
        if (duoSkillRating) {
            skillRatingMax = parseInt($('#tab-skill-rating').attr('data-duo-skill-rating-max'));
        } else {
            skillRatingMax = parseInt($('#tab-skill-rating').attr('data-skill-rating-max'));
        }
    }

    function calculateSkillRatingAmount(current, desired) {
        var amount = 0;
        for (var i = current; i < desired; i++) {
            amount += skillRatingPricesDetailed[i];
        }
        /*var from = Math.floor(current / 50);
        var to = Math.floor(desired / 50);
        var amount = 0;
        while (from < to) {
            amount += parseFloat(skillRatingPrices[from]);
            from++;
        }*/
        return amount;
    }

    function updateSkillRatingOrderSummaryAndPrice() {
        var skillRatingTab = $('#tab-skill-rating');
        if (skillRatingTab.length <= 0) return;
        duoSkillRating = $('#duo-skill-rating').prop('checked');
        streamSkillRating = $('#stream-skill-rating').prop('checked');
        updateSkillRatingMax();
        if (desiredSkillRating > skillRatingMax) {
            desiredSkillRating = skillRatingMax;
            desiredSkillRatingChanged();
            currentSkillRatingChanged();
        }
        if ((currentSkillRating < 0) || (desiredSkillRating < 0)) {
            skillRatingTab.find('.order-description').html('&nbsp;');
            skillRatingTab.find('.order-purchase').prop('disabled', true);
        } else {
            amount = calculateSkillRatingAmount(currentSkillRating, desiredSkillRating);
            var form = $('#form-skill-rating');
            if (duoSkillRating) {
                form.find('.hidden-category').val('DuoSkillRating');
                form.find('.hidden-stream').val('false');
                amount = amount * 1.5;
            } else {
                form.find('.hidden-category').val('SkillRating');
                if (streamSkillRating) {
                    form.find('.hidden-stream').val('true');
                    amount *= (100 + streamPercent) / 100;
                } else {
                    form.find('.hidden-stream').val('false');
                }
            }
            applyDiscount();
            form.find('.hidden-current').val(currentSkillRating);
            form.find('.hidden-desired').val(desiredSkillRating);
            form.find('.hidden-amount').val(amountWithDiscount.toFixed(2));
            var description = (duoSkillRating ? 'Duo ' : '') + 'Skill Rating boost from <b>' + currentSkillRating + '</b> to <b>' + desiredSkillRating + '</b> or more for';
            if (amount != amountWithDiscount) {
                description += ' <span style="text-decoration:line-through">&nbsp;$' + amount.toFixed(2) + '&nbsp;</span>';
            }
            description += ' <b>$' + amountWithDiscount.toFixed(2) + '</b>';
            skillRatingTab.find('.order-description').html(description);
            skillRatingTab.find('.order-purchase').prop('disabled', false);
        }
    }

    var plusMinusTimeout;
    var plusMinusInterval;

    function desiredSkillRatingDecrease() {
        if (desiredSkillRating >= 0) {
            desiredSkillRating -= 50;
            $('#desired-skill-rating').val(desiredSkillRating);
        }
        showDesiredSkillRatingTooltip = false;
        desiredSkillRatingChanged();
    }

    function desiredSkillRatingIncrease() {
        if (desiredSkillRating >= 0) {
            desiredSkillRating += 50;
            $('#desired-skill-rating').val(desiredSkillRating);
        }
        showDesiredSkillRatingTooltip = false;
        desiredSkillRatingChanged();
    }

    // Purchase Games

    var games = 10;
    var duoGames = false;
    var streamGames = false;

    var gamesSlider;

    var gamesPrices;

    function updateGames() {
        var gamesTab = $('#tab-games');
        if (gamesTab.length <= 0) return;
        var duoCurrentSkillRatingInput = $('#games-current-skill-rating');
        if (currentSkillRating >= 0) {
            var tmp = currentSkillRating;
            amount = 0;
            for (var i = 1; i <= games; i++) {
                if (tmp >= 5000) {
                    tmp = 4999;
                }
                amount += parseFloat(gamesPrices[Math.floor(tmp / 50)]);
                tmp += gamesScale;
            }
            duoGames = $('#duo-games').prop('checked');
            streamGames = $('#stream-games').prop('checked');
            var form = $('#form-games');
            if (duoGames) {
                form.find('.hidden-category').val('DuoGames');
                form.find('.hidden-stream').val('false');
                amount *= 1.2;
            } else {
                form.find('.hidden-category').val('Games');
                if (streamGames) {
                    form.find('.hidden-stream').val('true');
                    amount *= (100 + streamPercent) / 100;
                } else {
                    form.find('.hidden-stream').val('false');
                }
            }
            applyDiscount();
            form.find('.hidden-current').val(currentSkillRating);
            form.find('.hidden-games').val(games);
            form.find('.hidden-amount').val(amountWithDiscount.toFixed(2));
            var description = '';
            if (duoGames) {
                description = 'Get <b>' + games + ' Duo Queue Game' + (games > 1 ? 's' : '') + '</b> boost for';
            } else {
                description = 'Get <b>' + games + ' Solo Game' + (games > 1 ? 's' : '') + '</b> boost for';
            }
            if (amount != amountWithDiscount) {
                description += ' <span style="text-decoration:line-through">&nbsp;$' + amount.toFixed(2) + '&nbsp;</span>';
            }
            description += ' <b>$' + amountWithDiscount.toFixed(2) + '</b>';
            gamesTab.find('.order-description').html(description);
            gamesTab.find('.order-purchase').prop('disabled', false);
        } else if (!gamesTab.find('.order-purchase').prop('disabled')) {
            duoCurrentSkillRatingInput.tooltip('show');
            gamesTab.find('.order-description').html('&nbsp;');
            gamesTab.find('.order-purchase').prop('disabled', true);
        }
        updateSkillRatingIcon(duoCurrentSkillRatingInput.parent(), currentSkillRating);
    }

    function gamesDecrease() {
        if (games > 1) {
            games--;
            $('#games').val(games);
            gamesSlider.update({
                from: games
            });
        }
        updateGames();
    }

    function gamesIncrease() {
        if (games < 20) {
            games++;
            $('#games').val(games);
            gamesSlider.update({
                from: games
            });
        }
        updateGames();
    }

    // Purchase Placement

    var placementRank = 0;
    var placementGames = 10;
    var duoPlacement = false;
    var streamPlacement = false;

    var placementGamesSlider;

    var placementPrices;

    function updatePlacement() {
        var placementTab = $('#tab-placement');
        if (placementTab.length <= 0) return;
        duoPlacement = $('#duo-placement').prop('checked');
        streamPlacement = $('#stream-placement').prop('checked');
        amount = parseInt(placementGames) * parseFloat(placementPrices[parseInt(placementRank)]);
        var form = $('#form-placement');
        if (duoPlacement) {
            form.find('.hidden-category').val('DuoPlacement');
            form.find('.hidden-stream').val('false');
            amount *= 1.5;
        } else {
            form.find('.hidden-category').val('Placement');
            if (streamPlacement) {
                form.find('.hidden-stream').val('true');
                amount *= (100 + streamPercent) / 100;
            } else {
                form.find('.hidden-stream').val('false');
            }
        }
        applyDiscount();
        form.find('.hidden-current').val(placementRank);
        form.find('.hidden-games').val(placementGames);
        form.find('.hidden-amount').val(amountWithDiscount.toFixed(2));
        var description = '';
        if (duoPlacement) {
            description = 'Get <b>' + placementGames + ' Duo Placement Game' + (placementGames > 1 ? 's' : '') + '</b> boost for';
        } else {
            description = 'Get <b>' + placementGames + ' Placement Game' + (placementGames > 1 ? 's' : '') + '</b> boost for';
        }
        if (amount != amountWithDiscount) {
            description += ' <span style="text-decoration:line-through">&nbsp;$' + amount.toFixed(2) + '&nbsp;</span>';
        }
        description += ' <b>$' + amountWithDiscount.toFixed(2) + '</b>';
        placementTab.find('.order-description').html(description);
        placementTab.find('.order-purchase').prop('disabled', false);
    }

    function placementGamesDecrease() {
        if (placementGames > 1) {
            placementGames--;
            $('#placement-games').val(placementGames);
            placementGamesSlider.update({
                from: placementGames
            });
        }
        updatePlacement();
    }

    function placementGamesIncrease() {
        if (placementGames < 10) {
            placementGames++;
            $('#placement-games').val(placementGames);
            placementGamesSlider.update({
                from: placementGames
            });
        }
        updatePlacement();
    }

    // Top 500

    var top500Games = 50;

    var top500GamesSlider;

    var top500CutOff;
    var top500GamePrice;
    var top500Multiplier;

    function updateTop500() {
        var top500Tab = $('#tab-top500');
        if (top500Tab.length <= 0) return;
        var top500SkillRatingInput = $('#top500-current-skill-rating');
        if (currentSkillRating >= 0) {
            amount = calculateSkillRatingAmount(currentSkillRating, top500CutOff) * top500Multiplier;
            var estimatedGames = 0;
            var sr = top500CutOff;
            while (sr > currentSkillRating) {
                if (sr > 4000) {
                    estimatedGames += 3
                } else if (sr > 3000) {
                    estimatedGames += 2
                } else {
                    estimatedGames += 1
                }
                sr -= 50
            }
            var additionalGames = 50 - top500Games - estimatedGames;
            if (additionalGames > 0) {
                amount += additionalGames * top500GamePrice;
            }
            applyDiscount();
            var form = $('#form-top500');
            form.find('.hidden-current').val(currentSkillRating);
            form.find('.hidden-games').val(top500Games);
            form.find('.hidden-amount').val(amountWithDiscount.toFixed(2));
            var description = 'Skill Rating boost from <b>' + currentSkillRating + '</b> to <b>Top 500</b> for';
            if (amount != amountWithDiscount) {
                description += ' <span style="text-decoration:line-through">&nbsp;$' + amount.toFixed(2) + '&nbsp;</span>';
            }
            description += ' <b>$' + amountWithDiscount.toFixed(2) + '</b>';
            top500Tab.find('.order-description').html(description);
            top500Tab.find('.order-purchase').prop('disabled', false);
        } else if (!top500Tab.find('.order-purchase').prop('disabled')) {
            top500SkillRatingInput.tooltip('show');
            top500Tab.find('.order-description').html('&nbsp;');
            top500Tab.find('.order-purchase').prop('disabled', true);
        }
        updateSkillRatingIcon(top500SkillRatingInput.parent(), currentSkillRating);
    }

    function top500GamesDecrease() {
        if (top500Games > 0) {
            top500Games--;
            $('#top500-games').val(top500Games);
            top500GamesSlider.update({
                from: top500Games
            });
        }
        updateTop500();
    }

    function top500GamesIncrease() {
        if (top500Games < 50) {
            top500Games++;
            $('#top500-games').val(top500Games);
            top500GamesSlider.update({
                from: top500Games
            });
        }
        updateTop500();
    }

    // Purchase Init

    if ($('#tab-top500').length) {
        skillRatingPrices = $('#tab-top500').attr('data-prices').split(',');
        generateSkillRatingPricesDetailed();
        top500CutOff = parseInt($('#tab-top500').attr('data-top500-cut-off'));
        top500GamePrice = parseInt($('#tab-top500').attr('data-top500-game-price'));
        top500Multiplier = parseFloat($('#tab-top500').attr('data-top500-multiplier'));
        updateDiscount();

        $('#top500-current-skill-rating').mask('0000');

        $('#top500-current-skill-rating').on('input', function() {
            var newCurrentSkillRating = parseInt($(this).val());
            if (isNaN(newCurrentSkillRating)) {
                $(this).tooltip('show');
                currentSkillRating = -1;
                updateTop500();
            } else {
                $(this).tooltip('hide');
                if (newCurrentSkillRating >= top500CutOff - 100) {
                    newCurrentSkillRating = top500CutOff - 100;
                    $('#top500-current-skill-rating').val(newCurrentSkillRating);
                }
                if ((currentSkillRating >= 0) || (newCurrentSkillRating > 1000)) {
                    currentSkillRating = newCurrentSkillRating;
                    updateTop500();
                }
            }
            updateTop500();
        }).on('blur', function() {
            currentSkillRating = parseInt($(this).val());
            if (isNaN(currentSkillRating)) {
                currentSkillRating = -1;
            }
            updateTop500();
        }).on('change', function() {
            currentSkillRating = parseInt($(this).val());
            if (isNaN(currentSkillRating)) {
                currentSkillRating = -1;
            }
            updateTop500();
        });

        $('#top500-games').val(top500Games);

        $('#top500-games-minus').mousedown(function() {
            top500GamesDecrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    top500GamesDecrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });
        $('#top500-games-plus').mousedown(function() {
            top500GamesIncrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    top500GamesIncrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });

        $('#top500-games-slider').ionRangeSlider({
            min: 0,
            max: 50,
            hide_min_max: true,
            from: top500Games,
            prettify_enabled: false,
            grid: true,
            grid_num: 5,
            onChange: function (data) {
                top500Games = data.from;
                $('#top500-games').val(top500Games);
                updateTop500();
            },
            disable: true
        });
        top500GamesSlider = $('#top500-games-slider').data("ionRangeSlider");


        $('#top500-current-skill-rating').focus().tooltip('show');
        updateTop500();

    } else if ($('.order-create').length) {
        skillRatingPrices = $('#tab-skill-rating').attr('data-prices').split(',');
        generateSkillRatingPricesDetailed();
        skillRatingMax = parseInt($('#tab-skill-rating').attr('data-skill-rating-max'));
        streamPercent = parseInt($('#tab-skill-rating').attr('data-stream-percent'));
        gamesScale = parseInt($('#tab-games').attr('data-games-scale'));
        updateDiscount();

        $('#current-skill-rating, #desired-skill-rating, #games-current-skill-rating').mask('0000');

        $('#current-skill-rating').on('input', function() {
            currentSkillRating = parseInt($(this).val());
            if (isNaN(currentSkillRating)) {
                currentSkillRating = -1;
            }
            if ((currentSkillRating < 0) || (currentSkillRating >= 1000)) {
                currentSkillRatingChanged();
            }
            if (desiredSkillRating >= 0) {
                updateSkillRatingOrderSummaryAndPrice();
            }
            updateSkillRatingTooltips();
        }).on('change', currentSkillRatingChanged);

        $('#desired-skill-rating-minus').mousedown(function() {
            desiredSkillRatingDecrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    desiredSkillRatingDecrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });
        $('#desired-skill-rating-plus').mousedown(function() {
            desiredSkillRatingIncrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    desiredSkillRatingIncrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });

        $('#desired-skill-rating').on('input', function() {
            showDesiredSkillRatingTooltip = false;
            if ($(this).val().length >= 4) {
                desiredSkillRatingChanged();
                $('#desired-skill-rating').blur();
            } else {
                updateSkillRatingTooltips();
            }
        }).on('change', desiredSkillRatingChanged);
        $('#desired-skill-rating-slider').ionRangeSlider({
            min: 0,
            max: skillRatingMax,
            hide_min_max: true,
            from: 0,
            step: 50,
            prettify_enabled: false,
            grid: true,
            grid_num: 1,
            disable: true,
            onChange: function (data) {
                $('#desired-skill-rating').val(data.from);
                showDesiredSkillRatingTooltip = false;
                desiredSkillRatingChanged(false);
            }
        });
        desiredSkillRatingSlider = $('#desired-skill-rating-slider').data("ionRangeSlider");

        if ($('#current-skill-rating').val().length > 0) {
            showDesiredSkillRatingTooltip = false;
            currentSkillRating = parseInt($('#current-skill-rating').val());
            desiredSkillRating = parseInt($('#desired-skill-rating').val());
            currentSkillRatingChanged();
            desiredSkillRatingChanged();
        } else {
            updateSkillRatingTooltips();
        }

        $('#duo-skill-rating-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.prop('checked')) {
                $('#stream-skill-rating').prop('checked', false);
            }
            if (desiredSkillRating > 0) {
                desiredSkillRatingChanged();
            }
        });

        if (window.location.hash) {
            var hash = window.location.hash.substring(1);
            if (hash == 'duo') {
                $('#duo-skill-rating-group input').prop('checked', true);
            }
        }

        $('#stream-skill-rating-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.is(':checked')) {
                $('#duo-skill-rating').prop('checked', false);
            }
            if (desiredSkillRating > 0) {
                desiredSkillRatingChanged();
            }
        });

        // Duo Games

        gamesPrices = $('#tab-games').attr('data-prices').split(',');

        $('#games-current-skill-rating').on('input', function() {
            var newCurrentSkillRating = parseInt($(this).val());
            if (isNaN(newCurrentSkillRating)) {
                $(this).tooltip('show');
                currentSkillRating = -1;
                updateGames();
            } else {
                $(this).tooltip('hide');
                if (newCurrentSkillRating > 5000) {
                    newCurrentSkillRating = 5000;
                    $('#games-current-skill-rating').val(newCurrentSkillRating);
                }
                if ((currentSkillRating >= 0) || (newCurrentSkillRating > 1000)) {
                    currentSkillRating = newCurrentSkillRating;
                    updateGames();
                }
            }
            updateGames();
        }).on('blur', function() {
            currentSkillRating = parseInt($(this).val());
            if (isNaN(currentSkillRating)) {
                currentSkillRating = -1;
            }
            updateGames();
        }).on('change', function() {
            currentSkillRating = parseInt($(this).val());
            if (isNaN(currentSkillRating)) {
                currentSkillRating = -1;
            }
            updateGames();
        });

        $('#games-minus').mousedown(function() {
            gamesDecrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    gamesDecrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });
        $('#games-plus').mousedown(function() {
            gamesIncrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    gamesIncrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });

        $('#games-slider').ionRangeSlider({
            min: 1,
            max: 20,
            hide_min_max: true,
            from: games,
            prettify_enabled: false,
            grid: true,
            grid_num: 19,
            onChange: function (data) {
                games = data.from;
                $('#games').val(games);
                updateGames();
            }
        });
        gamesSlider = $('#games-slider').data("ionRangeSlider");

        $('#duo-games-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.prop('checked')) {
                $('#stream-games').prop('checked', false);
            }
            updateGames();
        });

        $('#stream-games-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.is(':checked')) {
                $('#duo-games').prop('checked', false);
            }
            updateGames();
        });

        // Placement

        placementPrices = $('#tab-placement').attr('data-prices').split(',');

        $('#placement-rank').find('a').on('click', function() {
            $('#placement-rank').find('button span').html($(this).html());
            placementRank = parseInt($(this).attr('data-value'));
            updatePlacement();
        });

        $('#placement-games-minus').mousedown(function() {
            placementGamesDecrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    placementGamesDecrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });
        $('#placement-games-plus').mousedown(function() {
            placementGamesIncrease();
            plusMinusTimeout = setTimeout(function() {
                plusMinusInterval = setInterval(function() {
                    placementGamesIncrease();
                }, 125)
            }, 300);
        }).mouseup(function() {
            clearTimeout(plusMinusTimeout);
            clearInterval(plusMinusInterval);
        });

        $('#placement-games-slider').ionRangeSlider({
            min: 1,
            max: 10,
            hide_min_max: true,
            from: placementGames,
            prettify_enabled: false,
            grid: true,
            grid_num: 9,
            onChange: function (data) {
                placementGames = data.from;
                $('#placement-games').val(placementGames);
                updatePlacement();
            }
        });
        placementGamesSlider = $('#placement-games-slider').data("ionRangeSlider");

        $('#duo-placement-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.prop('checked')) {
                $('#stream-placement').prop('checked', false);
            }
            updatePlacement();
        });

        $('#stream-placement-group').click(function(event) {
            event.preventDefault();
            var checkbox = $(this).find('input');
            checkbox.prop('checked', !checkbox.prop('checked'));
            if (checkbox.is(':checked')) {
                $('#duo-placement').prop('checked', false);
            }
            updatePlacement();
        });

        // On switch

        $('#tab-skill-rating-link').on('show.bs.tab', function() {
            var currentSkillRatingInput = $('#current-skill-rating');
            if (currentSkillRating >= 0) {
                if (currentSkillRating >= skillRatingMax) {
                    currentSkillRating = skillRatingMax - 1;
                }
                currentSkillRatingInput.val(currentSkillRating);
            } else {
                currentSkillRatingInput.val('');
            }
        }).on('shown.bs.tab', function() {
            currentSkillRatingChanged();
            updateSkillRatingTooltips();
        });

        $('#tab-skill-rating-link-from-duo').click(function() {
            $('#tab-skill-rating-link').click();
            $('#duo-skill-rating-group input').prop('checked', true);
        });

        $('#tab-games-link').on('show.bs.tab', function() {
            var duoCurrentSkillRatingInput = $('#games-current-skill-rating');
            if (currentSkillRating >= 0) {
                duoCurrentSkillRatingInput.val(currentSkillRating);
            } else {
                duoCurrentSkillRatingInput.val('');
            }
            $('#games').val(games);
        }).on('shown.bs.tab', function() {
            if (currentSkillRating < 0) {
                $('#games-current-skill-rating').focus();
            } else {
                $('#games-current-skill-rating').tooltip('hide');
            }
            updateGames();
        });

        $('#tab-placement-link').on('show.bs.tab', function() {
            $('#placement-games').val(placementGames);
        }).on('shown.bs.tab', function() {
            updatePlacement();
        });
        $('#placement-games').val(placementGames);
        updatePlacement();

    }

    // PayPal

    // $('#form-skill-rating,#form-games,#form-placement,#form-top500').find('button').click(function(event) {
    //     event.preventDefault();
    //     var form = $(this).parent('form');
    //     var ladda = require('ladda');
    //     var l = ladda.create(this);
    //     l.start();
    //     form.submit();
    // });

    var ladda = require('ladda');
    $('#form-skill-rating,#form-games,#form-placement').find('button.paypal').click(function(event) {
        event.preventDefault();
        var form = $(this).parents('form');
        form.find('.hidden-payment-method').val('paypal');
        form.find('.order-purchase').prop('disabled', true);
        paypal.checkout.initXO();
        var l = ladda.create(this);
        l.start();
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function(data) {
            paypal.checkout.startFlow(data);
        }).fail(function() {
            paypal.checkout.closeFlow();
            window.location = '/overwatch/boost';
        }.bind(this));
    });

    // Stripe

    var stripeHandler;
    var stripeExecuteUrl;
    $('#form-skill-rating,#form-games,#form-placement').find('button.stripe').click(function(event) {
        event.preventDefault();
        var form = $(this).parents('form');
        form.find('.hidden-payment-method').val('stripe');
        form.find('.order-purchase').prop('disabled', true);
        var reEnableButtons = true;
        if (stripeHandler === undefined) {
            var key = 'pk_live_jYaIKrvl064hNRIHySF3sJUq';
            if (window.location.host.substring(0, 9) === 'localhost') {
                key = 'pk_test_zsAvn8eIfazVtnjHqS9Ff4pC';
            }
            stripeHandler = StripeCheckout.configure({
                key: key,
                image: '/img/logo-stripe.png',
                locale: 'auto',
                token: function(token) {
                    reEnableButtons = false;
                    window.location = stripeExecuteUrl + '?id=' + token.id + '&email=' + token.email;
                }
            });
        }
        var l = ladda.create(this);
        l.start();
        $.ajax({
            type: 'POST',
            async: false,
            url: form.attr('action'),
            data: form.serialize()
        }).done(function(data) {
            stripeExecuteUrl = data.url;
            data.stripe.closed = function() {
                if (reEnableButtons) {
                    l.stop();
                    form.find('.order-purchase').prop('disabled', false);
                }
            };
            stripeHandler.open(data.stripe);
        }).fail(function() {
            window.location = '/overwatch/boost';
        }.bind(this));
    });

    // News

    var owlCarouselNews = $('#owl-carousel-news');

    if (owlCarouselNews.length) {
        owlCarouselNews.owlCarousel({
            items : 3,
            itemsDesktop: false,
            itemsDesktopSmall: [1199, 2],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1]
        });
        $('#owl-carousel-news-container').find('.next').click(function(){
            owlCarouselNews.trigger('owl.next');
            return false;
        });

        $('#owl-carousel-news-container').find('.prev').click(function(){
            owlCarouselNews.trigger('owl.prev');
            return false;
        });
    }

    var owlCarouselReviews = $('#owl-carousel-reviews');

    if (owlCarouselReviews.length) {
        if (owlCarouselReviews.attr('data-autoplay') == 'true') {
            owlCarouselReviews.owlCarousel({
                items : 2,
                itemsDesktop: false,
                itemsDesktopSmall: [1199, 1],
                itemsTablet: [768, 1],
                itemsMobile: [479, 1],
                autoPlay: true,
                stopOnHover: true
            });
        } else {
            owlCarouselReviews.owlCarousel({
                items : 2,
                itemsDesktop: false,
                itemsDesktopSmall: [1199, 1],
                itemsTablet: [768, 1],
                itemsMobile: [479, 1]
            });
        }

        $('#owl-carousel-reviews-container').find('.next').click(function(){
            owlCarouselReviews.trigger('owl.next');
            return false;
        });

        $('#owl-carousel-reviews-container').find('.prev').click(function(){
            owlCarouselReviews.trigger('owl.prev');
            return false;
        });
    }

    // Jobs

    if ($('#form-jobs').length) {
        $('#platform-pc').change(function() {
            jobsPlatformPcChanged();
            jobsUpdateCheckboxesValidation();
        });
        $('#platform-psn').change(function() {
            jobsPlatformPsnChanged();
            jobsUpdateCheckboxesValidation();
        });
        $('#platform-xbl').change(function() {
            jobsPlatformXblChanged();
            jobsUpdateCheckboxesValidation();
        });
        jobsPlatformPcChanged();
        jobsPlatformPsnChanged();
        jobsPlatformXblChanged();
    }

    function jobsPlatformPcChanged() {
        if ($('#platform-pc').prop('checked')) {
            $('#battletag').attr('data-validation-regex', '/^.{3,12}#\\d{4,5}$/');
            $('#battletag_field').removeClass('hidden');
        } else {
            $('#battletag').val('');
            $('#battletag').removeAttr('data-validation-regex');
            $('#battletag_field').addClass('hidden');
        }
    }

    function jobsPlatformPsnChanged() {
        if ($('#platform-psn').prop('checked')) {
            $('#psn-online-id').attr('data-validation', '[L>=3, L<=16]');
            $('#psn-online-id_field').removeClass('hidden');
            $('#psn-online-id_field').find('.hidden').removeClass('hidden');
        } else {
            $('#psn-online-id').val('');
            $('#psn-online-id').removeAttr('data-validation');
            $('#psn-online-id_field').addClass('hidden');
        }
    }

    function jobsPlatformXblChanged() {
        if ($('#platform-xbl').prop('checked')) {
            $('#xbox-gamertag').attr('data-validation', '[L>=1, L<=15]');
            $('#xbox-gamertag_field').removeClass('hidden');
            $('#xbox-gamertag_field').find('.hidden').removeClass('hidden');
        } else {
            $('#xbox-gamertag').val('');
            $('#xbox-gamertag').removeAttr('data-validation');
            $('#xbox-gamertag_field').addClass('hidden');
        }
    }

    function jobsUpdateCheckboxesValidation() {
        $('#platform_field').find('.form-tooltip-error').remove();
        if (!$('#platform-pc').prop('checked') && !$('#platform-psn').prop('checked') && !$('#platform-xbl').prop('checked')) {
            $('#platform').attr('data-validation', '[L>0]');
        } else {
            $('#platform').removeAttr('data-validation');
        }
    }

    // Order redirect countdown

    if ($('#order-redirect-countdown').length) {
        var redirectCountdown = $('#order-redirect-countdown');
        function redirectCountdownTick() {
            redirectCountdown.text(parseInt(redirectCountdown.text()) - 1);
            if (parseInt(redirectCountdown.text()) <= 0) {
                clearInterval(redirectCountdownInterval);
                location.href = $('#order-redirect-countdown-link').attr('href');
            }
        }
        var redirectCountdownInterval = setInterval(redirectCountdownTick, 1000);
    }

    // Validation

    var forms = $('form.validate');
    if (forms.length > 0) {
        forms.validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorClass: 'has-error',
                    errorListClass: 'form-tooltip-error'
                }
            }
        });
    }

});

