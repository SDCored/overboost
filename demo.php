    <?php $page = "demo"; $sRoot = $_SERVER['DOCUMENT_ROOT']; require($sRoot . '/include/navbar.php'); ?>

      <!-- MAIN CONTENT -->
        <div class="section" id="demo">
            <div class="container">
                <div class="info-strip">
                    <p>This is just a demo of our order page. It showcases all the functions and features that you will have access to after you make an order with us.
                    All the buttons on this page are only for show, so it's normal that nothing happens when you click them!</p>
                </div>

                <div class="row order-row">

                    <div class="col-12 margin-top-row small-piece">
                    <div class="row">
                        <div class="col-12 col-lg-7">
                            <div class="block">
                                <div class="block-head">
                                    Skill Rating Boost (In Progress)
                                </div>
                                <div class="block-body no-pad">
                                    <div class="rank-list">
                                        <div class="rank">
                                            <p class="head">Start</p>
                                            <img src="static/img/demo/ranks/1.png" class="img-responsive">  
                                            <p class="value">2474</p>
                                        </div>
                                        <div class="rank">
                                            <p class="head">Current</p>
                                            <img src="static/img/demo/ranks/2.png" class="img-responsive">  
                                            <p class="value">2537</p>
                                        </div>
                                        <div class="rank">
                                            <p class="head">Desired</p>
                                            <img src="static/img/demo/ranks/3.png" class="img-responsive">  
                                            <p class="value">3000</p>
                                        </div>
                                    </div>

                                    <div class="order-progress">
                                        <p class="head">Progress</p>
                                        <div class="order-progress-slider" data-value="40">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5 right-col">

                            <div class="block">
                                <div class="block-head">
                                    Like our Services?
                                </div>
                                <div class="block-body text-center">
                                    <form method="post">
                                        <div class="form-group">
                                          
                                          <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" value="52">
                                            <span class="input-group-addon inv">.00</span>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-order">Leave a tip</button>
                                        </div>
                                    </form>
                                </div>  
                            </div>
                            <div class="block">
                                <div class="block-body">
                                    
                                    <div class="booster">
                                        <span class="booster-head">Booster</span>
                                        <span class="booster-name">Julian</span>
                                    </div>

                                    <div class="streaming">
                                        <a href="#" class="btn btn-order">Live Stream</a>
                                        <a href="#" class="btn btn-order">Pause Order</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    </div>

                    <div class="col-12 margin-top-row big-piece">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="block">
                                    <div class="block-head">Options</div>
                                    <div class="block-body">
                                        <ul class="nav nav-pills nav-fill nav-options">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#gameAccountTab" data-toggle="tab"><i class="fa fa-gamepad"></i> Game Account</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#herosTab" data-toggle="tab"><i class="fa fa-bolt"></i> Heroes</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#notesTab" data-toggle="tab"><i class="fa fa-pencil"></i> Notes</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#boosterTab" data-toggle="tab"><i class="fa fa-space-shuttle"></i> Booster</a>
                                          </li>
                                        </ul>

                                        <div class="tab-content options-tab-content">
                                            <div class="tab-pane active show shorten-center fade" id="gameAccountTab" role="tabpanel">
                                                <p class="info">It is not possible to edit your account details after we've started working on your order.</p>

                                                <div class="platform-selection" id="options-platform-selector">
                                                    <div class="platform active"><i class="fa fa-windows"></i> Windows</div>
                                                    <div class="platform"><img src="static/img/demo/playstation-icon.png"> PS4
                                                    </div>
                                                    <div class="platform"><img src="static/img/demo/xbox-icon.png"> XBOX
                                                    </div>
                                                </div>

                                                <div class="form-elements">
                                                    <form method="post">
                                                        <div class="form-group">
                                                            <label class="control-label">Region</label>
                                                            <select class="form-control">
                                                                <option>Europe</option>
                                                                <option>America</option>
                                                                <option>Asia</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Account</label>
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                            <input type="text" class="form-control" placeholder="Your Email" value="demo@demo.com">
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                            <input type="text" class="form-control" placeholder="Your Password" value="demo">
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="submit" class="btn btn-order" value="Submit">
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>

                                            <div class="tab-pane shorten-center fade" id="herosTab" role="tabpanel">
                                                <p class="info">
                                                    Please select at least 6 heroes. If you won't select any then our booster will choose heroes to play by himself.
                                                </p>
                                                <div class="hero-selection" id="options-hero-selector">
                                                    <div class="hero-row">
                                                        <div class="hero-class">
                                                            <img src="static/img/demo/heros/offense.png">
                                                        </div>
                                                        <div class="heros">
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/genji.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/mccree.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/pharah.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/reaper.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/soldier76.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/sombra.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/tracer.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hero-row">
                                                        <div class="hero-class">
                                                            <img src="static/img/demo/heros/defense.png">
                                                        </div>
                                                        <div class="heros">
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/bastion.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/hanzo.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/junkrat.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/mei.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/torbjorn.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/widowmaker.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hero-row">
                                                        <div class="hero-class">
                                                            <img src="static/img/demo/heros/tank.png">
                                                        </div>
                                                        <div class="heros">
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/dva.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/orisa.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/reinhardt.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/roadhog.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/winston.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/zarya.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hero-row">
                                                        <div class="hero-class">
                                                            <img src="static/img/demo/heros/support.png">
                                                        </div>
                                                        <div class="heros">
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/ana.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/lucio.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/mercy.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/symmetra.png">
                                                            </div>
                                                            <div class="hero">
                                                                <img src="static/img/demo/heros/zenyatta.png">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <button type="submit" class="btn btn-order" id="heroSubmitBtn">Submit</button>

                                                </div>
                                            </div>

                                            <div class="tab-pane shorten-center fade" id="notesTab" role="tabpanel">
                                                <p class="info">You can leave additional requests here.</p>

                                                <div class="form-elements">
                                                    <div class="form-group">
                                                        <textarea class="form-control" rows="7" placeholder="You can leave messages or instructions here."></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="btn btn-order" type="submit" value="Submit">
                                                    </div>  
                                                </div>

                                            </div>   

                                            <div class="tab-pane shorten-center fade" id="boosterTab" role="tabpanel">
                                                <p class="info">You can assign booster from the list of boosters who worked on your orders before. Remember, assigning booster will usually extend boosting time.</p>

                                                <div class="form-elements">
                                                    <div class="form-group">
                                                        <label class="control-label">Select Booster (optional)</label>
                                                        <select class="form-control">
                                                            <option>Someone</option>
                                                            <option>Something</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>   

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                    <div class="block live-chat-block">
                                        <div class="block-head">Live Chat</div>
                                        <div class="block-body no-pad">
                                            <div class="live-chat-container">

                                                <div class="chat-input">
                                                    <input type="text" class="form-control" placeholder="Type a message here (disabled for demo)">
                                                    <i class="fa fa-send"></i>
                                                </div>

                                                <div class="chat-container">
                                                    <div class="message-box">
                                                        <p class="intro"><b>Booster</b> <span>11:19</span></p>
                                                        <div class="message">
                                                            Hey, am about to start your thing. Can I go?
                                                        </div>
                                                    </div>
                                                    <div class="message-box inverse">
                                                        <p class="intro"><b>You</b> <span>11:19</span></p>
                                                        <div class="message">
                                                            Sure!
                                                        </div>
                                                    </div>
                                                    <div class="message-box">
                                                        <p class="intro"><b>Booster</b> <span>11:21</span></p>
                                                        <div class="message">
                                                            You're cool!
                                                        </div>
                                                    </div>
                                                    <div class="message-box inverse">
                                                        <p class="intro"><b>You</b> <span>11:22</span></p>
                                                        <div class="message">
                                                            NO! YOU ARE MORE COOL SHUT UP DIE 
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 margin-top-row">
                        <div class="block game-history">
                            <div class="block-head">Games History</div>
                            <div class="block-body no-pad">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>When</th>
                                            <th>Booster</th>
                                            <th>Result</th>
                                            <th>Skill Rating</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2</td>
                                            <td>30 minutes ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge victory">Victory</span></td>
                                            <td>+23</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>1 hour ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge defeat">Defeat</span></td>
                                            <td>+10</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>30 minutes ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge victory">Victory</span></td>
                                            <td>+23</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>1 hour ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge defeat">Defeat</span></td>
                                            <td>+10</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>30 minutes ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge victory">Victory</span></td>
                                            <td>+23</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>1 hour ago</td>
                                            <td>Someone</td>
                                            <td><span class="badge defeat">Defeat</span></td>
                                            <td>+10</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div>

                </div>

            </div>
        </div>
      <!-- ============ -->

      <?php require($sRoot.'/include/footer.php'); ?>